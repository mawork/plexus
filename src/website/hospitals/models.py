from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import (AutoSlugField, CreationDateTimeField, ModificationDateTimeField)


class Sector(models.Model):
    title = models.CharField(max_length=255, blank=True, null=True)
    slug = AutoSlugField(populate_from=('title'), unique=True)

    def __unicode__(self):
        return unicode(self.title) or u''

    class Meta:
        verbose_name = _('Sector')
        verbose_name_plural = _('Sector')


class Parent(models.Model):
    code = models.CharField(max_length=255, blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    slug = AutoSlugField(populate_from=('code'), unique=True)

    def __unicode__(self):
        return unicode(self.title) or u''

    class Meta:
        verbose_name = _('Parent organisation')
        verbose_name_plural = _('Parent organisation')


class Organisation(models.Model):
    organisation_id = models.IntegerField(blank=True, null=True)
    organisation_code = models.CharField(max_length=10, blank=True, null=True)
    sector = models.ForeignKey(Sector, blank=True, null=True)
    status = models.BooleanField(default=True)
    organisation_name = models.CharField(max_length=255, blank=True, null=True)
    address_one = models.CharField(max_length=255, blank=True, null=True)
    address_two = models.CharField(max_length=255, blank=True, null=True)
    address_three = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    county = models.CharField(max_length=255, blank=True, null=True)
    postcode = models.CharField(max_length=10, blank=True, null=True)
    Latitude = models.CharField(max_length=255, blank=True, null=True)
    longitude = models.CharField(max_length=255, blank=True, null=True)
    phone = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    website = models.CharField(max_length=255, blank=True, null=True)
    parent = models.ForeignKey(Parent, blank=True, null=True)

    position_latitude = models.FloatField(null=True, blank=True)
    position_longitude = models.FloatField(null=True, blank=True)

    def __unicode__(self):
        return unicode(self.organisation_name) or u''

    def convertPos(self):
        if self.Latitude is not None:
            self.position_latitude = float(self.Latitude)
        if self.longitude is not None:
            self.position_longitude = float(self.longitude)

    def get_distance(self, points):
        from math import radians, cos, sin, asin, sqrt
        lon1, lat1, lon2, lat2 = map(radians, [float(points[1]), float(points[0]), self.position_longitude, self.position_latitude])
        # haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
        c = 2 * asin(sqrt(a))
        km = 6367 * c
        return km

    class Meta:
        verbose_name = _('Organisation')
        verbose_name_plural = _('Organisation')

    def get_services_api(self):
        duct = []
        for item in self.services_set.all():
            dicts = {}
            dicts['metricname'] = item.metricname
            dicts['value'] = item.value
            dicts['text'] = item.text
            duct.append(dicts)
        return duct


class Services(models.Model):
    organisation = models.ForeignKey(Organisation, blank=True, null=True)
    metricname = models.TextField(blank=True, null=True)
    value = models.CharField(max_length=255, blank=True, null=True)
    text = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return "{0}, {1}".format(unicode(self.organisation.organisation_code) or u"", self.metricname)

    class Meta:
        verbose_name = _('Services')
        verbose_name_plural = _('Services')
