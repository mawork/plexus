__author__ = 'sam'
from django.core.management.base import BaseCommand, CommandError
from datetime import datetime
from dateutil import relativedelta
from website.hospitals.models import *

class Command(BaseCommand):
    help = 'This will strip out any unliked char from the names'

    def handle(self, **options):
        for item in Organisation.objects.all():
            if any(i in item.organisation_name for i in '-'):
                item.organisation_name = item.organisation_name.replace('-',' ')
                item.save()
            else:
                pass
