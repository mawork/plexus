# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hospitals', '0002_auto_20150821_1215'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='organisation',
            options={'verbose_name': 'Organisation', 'verbose_name_plural': 'Organisation'},
        ),
        migrations.AlterModelOptions(
            name='parent',
            options={'verbose_name': 'Parent organisation', 'verbose_name_plural': 'Parent organisation'},
        ),
        migrations.AlterModelOptions(
            name='sector',
            options={'verbose_name': 'Sector', 'verbose_name_plural': 'Sector'},
        ),
        migrations.AlterModelOptions(
            name='services',
            options={'verbose_name': 'Services', 'verbose_name_plural': 'Services'},
        ),
        migrations.AddField(
            model_name='organisation',
            name='position_latitude',
            field=models.FloatField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='organisation',
            name='position_longitude',
            field=models.FloatField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
