# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Organisation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('organisation_id', models.IntegerField()),
                ('organisation_code', models.CharField(max_length=10)),
                ('status', models.BooleanField(default=True)),
                ('organisation_name', models.CharField(max_length=255)),
                ('address_one', models.CharField(max_length=255, null=True, blank=True)),
                ('address_two', models.CharField(max_length=255, null=True, blank=True)),
                ('address_three', models.CharField(max_length=255, null=True, blank=True)),
                ('city', models.CharField(max_length=255, null=True, blank=True)),
                ('county', models.CharField(max_length=255, null=True, blank=True)),
                ('postcode', models.CharField(max_length=10, null=True, blank=True)),
                ('Latitude', models.CharField(max_length=255, null=True, blank=True)),
                ('longitude', models.CharField(max_length=255, null=True, blank=True)),
                ('phone', models.CharField(max_length=255, null=True, blank=True)),
                ('email', models.CharField(max_length=255, null=True, blank=True)),
                ('website', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Parent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=255)),
                ('title', models.CharField(max_length=255)),
                ('slug', django_extensions.db.fields.AutoSlugField(editable=False, populate_from=b'title', blank=True, unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sector',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('slug', django_extensions.db.fields.AutoSlugField(editable=False, populate_from=b'title', blank=True, unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Services',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('metricname', models.TextField(null=True, blank=True)),
                ('value', models.CharField(max_length=255, null=True, blank=True)),
                ('text', models.TextField(null=True, blank=True)),
                ('organisation', models.ForeignKey(blank=True, to='hospitals.Organisation', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='organisation',
            name='parent',
            field=models.ForeignKey(blank=True, to='hospitals.Parent', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='organisation',
            name='sector',
            field=models.ForeignKey(blank=True, to='hospitals.Sector', null=True),
            preserve_default=True,
        ),
    ]
