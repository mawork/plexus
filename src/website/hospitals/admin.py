from django.contrib import admin
from .models import *

class Hospital(admin.ModelAdmin):
    list_display=('organisation_code','organisation_name','Latitude','longitude')
    search_fields=['organisation_name','organisation_code']

admin.site.register(Sector)
admin.site.register(Parent)
admin.site.register(Organisation, Hospital)
admin.site.register(Services)
