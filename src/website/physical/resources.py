__author__ = 'sam'
from .models import *
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

class PhysicalSubItemReousrces(ModelResource):
    url = fields.CharField(attribute='url')
    class Meta:
        queryset = PhysicalSubItem.objects.all()
        resource_name ='subitem'
        allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'sort_value':ALL,
            'body':ALL,
            'slug':ALL,
            'section':ALL,
            'partof':ALL,
            'id':ALL
        }

class PhysicalItemResources(ModelResource):
    #section = fields.ToOneField(SectionResources, 'section', full=True)
    data = fields.ToManyField(PhysicalSubItemReousrces, 'physicalsubitem_set', full=True, null=True)
    url = fields.CharField(attribute='url')

    class Meta:
        queryset = PhysicalItems.objects.all()
        resource_name ='item'
        allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'sort_value':ALL,
            'body':ALL,
            'slug':ALL,
            'section':ALL_WITH_RELATIONS,
            'physicalsubitem_set':ALL_WITH_RELATIONS,
            'id':ALL
        }

class PhysicalSectionResources(ModelResource):
    subsection = fields.ToManyField(PhysicalItemResources, 'physicalitems_set', full=True, null=True)
    url = fields.CharField(attribute='url')
    class Meta:
        queryset = PhysicalSection.objects.all()
        resource_name = 'section'
        detail_allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'sort_value':ALL,
            'body':ALL,
            'slug':ALL,
            'id':ALL
        }
