from flatblocks.models import FlatBlock
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

class FlatBlockResources(ModelResource):
    class Meta:
        queryset = FlatBlock.objects.all()
        resource_name = 'flatblock'
        allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'id':ALL,
            'slug':ALL,
            'header':ALL,
            'content':ALL,
        }
