# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aroundyou', '0010_jobseeking_last'),
    ]

    operations = [
        migrations.AddField(
            model_name='jobseeking',
            name='five_over',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobseeking',
            name='one_to_two',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobseeking',
            name='six_to_one',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobseeking',
            name='two_to_five',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobseeking',
            name='unkown',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobseeking',
            name='up_six',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
