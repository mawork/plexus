# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aroundyou', '0003_region_regioninactive_regioninemployment_regionpopulation_regionunemployment'),
    ]

    operations = [
        migrations.AddField(
            model_name='population',
            name='effected',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
