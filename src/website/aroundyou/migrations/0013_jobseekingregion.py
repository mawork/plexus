# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aroundyou', '0012_auto_20151028_1236'),
    ]

    operations = [
        migrations.CreateModel(
            name='JobSeekingRegion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('value', models.FloatField(null=True, blank=True)),
                ('last', models.CharField(max_length=10, null=True, blank=True)),
                ('up_six', models.IntegerField(null=True, blank=True)),
                ('six_to_one', models.IntegerField(null=True, blank=True)),
                ('one_to_two', models.IntegerField(null=True, blank=True)),
                ('two_to_five', models.IntegerField(null=True, blank=True)),
                ('five_over', models.IntegerField(null=True, blank=True)),
                ('unkown', models.IntegerField(null=True, blank=True)),
                ('region', models.ForeignKey(to='aroundyou.Region')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
