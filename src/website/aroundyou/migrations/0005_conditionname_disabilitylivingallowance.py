# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aroundyou', '0004_population_effected'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConditionName',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DisabilityLivingAllowance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('value', models.IntegerField()),
                ('condition', models.ForeignKey(to='aroundyou.ConditionName')),
                ('local', models.ForeignKey(to='aroundyou.LocalAuthority')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
