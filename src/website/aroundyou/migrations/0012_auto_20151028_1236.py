# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aroundyou', '0011_auto_20151028_1221'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jobseeking',
            name='value',
            field=models.FloatField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
