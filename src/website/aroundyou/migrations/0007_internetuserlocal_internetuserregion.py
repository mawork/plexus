# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aroundyou', '0006_disabilitylivingallowanceregion'),
    ]

    operations = [
        migrations.CreateModel(
            name='InternetUserLocal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('last_three_per', models.FloatField(null=True, blank=True)),
                ('more_three_per', models.FloatField(null=True, blank=True)),
                ('never_per', models.FloatField(null=True, blank=True)),
                ('local', models.ForeignKey(to='aroundyou.LocalAuthority')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='InternetUserRegion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('last_three_per', models.FloatField(null=True, blank=True)),
                ('more_three_per', models.FloatField(null=True, blank=True)),
                ('never_per', models.FloatField(null=True, blank=True)),
                ('region', models.ForeignKey(to='aroundyou.Region')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
