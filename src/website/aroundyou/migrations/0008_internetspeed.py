# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aroundyou', '0007_internetuserlocal_internetuserregion'),
    ]

    operations = [
        migrations.CreateModel(
            name='InternetSpeed',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('avg_speed', models.FloatField(help_text=b'Average sync speed (Mbit/s)', null=True, blank=True)),
                ('avg_score', models.FloatField(help_text=b'Average sync speed score', null=True, blank=True)),
                ('perc_not', models.FloatField(help_text=b'Percentage not receiving 2Mbit/s', null=True, blank=True)),
                ('perc_not_score', models.FloatField(help_text=b'Percentage not receiving 2Mbit/s score', null=True, blank=True)),
                ('superfast', models.FloatField(help_text=b'Superfast broadband availability', null=True, blank=True)),
                ('superfast_score', models.FloatField(help_text=b'Superfast broadband availability score', null=True, blank=True)),
                ('takeup', models.FloatField(help_text=b'Take-up (including superfast broadband)', null=True, blank=True)),
                ('takeup_score', models.FloatField(help_text=b'Take-up score', null=True, blank=True)),
                ('superfast_takeup', models.FloatField(help_text=b'Superfast takeup', null=True, blank=True)),
                ('overall_score', models.FloatField(help_text=b'Overall Total of Scores', null=True, blank=True)),
                ('quality_score', models.FloatField(help_text=b'Quality Score', null=True, blank=True)),
                ('data_throughtput', models.FloatField(help_text=b'Data Throughput', null=True, blank=True)),
                ('local', models.ForeignKey(to='aroundyou.LocalAuthority')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
