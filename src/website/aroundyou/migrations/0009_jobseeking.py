# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aroundyou', '0008_internetspeed'),
    ]

    operations = [
        migrations.CreateModel(
            name='JobSeeking',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('value', models.FloatField()),
                ('local', models.ForeignKey(to='aroundyou.LocalAuthority')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
