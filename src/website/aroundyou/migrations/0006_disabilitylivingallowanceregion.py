# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aroundyou', '0005_conditionname_disabilitylivingallowance'),
    ]

    operations = [
        migrations.CreateModel(
            name='DisabilityLivingAllowanceRegion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('value', models.IntegerField()),
                ('condition', models.ForeignKey(to='aroundyou.ConditionName')),
                ('region', models.ForeignKey(to='aroundyou.Region')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
