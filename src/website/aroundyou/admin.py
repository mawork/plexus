from django.contrib import admin
from .models import *


class DataAdmin(admin.ModelAdmin):
    list_display = ('local', 'start', 'end', 'value', 'conf')
    list_filter = ('local',)


class PopAdmin(admin.ModelAdmin):
    list_display = ('local', 'year', 'value')
    list_filter = ('local',)


class LocalAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_filter = ('name',)
    search_fields = ['name',]

admin.site.register(LocalAuthority, LocalAdmin)
admin.site.register(InEmployment, DataAdmin)
admin.site.register(UnEmployment, DataAdmin)
admin.site.register(InActive, DataAdmin)
admin.site.register(Population, PopAdmin)


class RegionDataAdmin(admin.ModelAdmin):
    list_display = ('region', 'start', 'end', 'value', 'conf')


class RegionPopAdmin(admin.ModelAdmin):
    list_display = ('region', 'year', 'value')

admin.site.register(Region)
admin.site.register(RegionInEmployment, RegionDataAdmin)
admin.site.register(RegionUnEmployment, RegionDataAdmin)
admin.site.register(RegionInActive, RegionDataAdmin)
admin.site.register(RegionPopulation, RegionPopAdmin)


class DLAAdmin(admin.ModelAdmin):
    list_display = ('local', 'condition')
    list_filter = ('local',)


class DLARAdmin(admin.ModelAdmin):
    list_display = ('region', 'condition')
    list_filter = ('region',)

admin.site.register(ConditionName)
admin.site.register(DisabilityLivingAllowance, DLAAdmin)
admin.site.register(DisabilityLivingAllowanceRegion, DLARAdmin)


class InternetAdmin(admin.ModelAdmin):
    list_display = ('local','date')
    list_filter = ('local',)


class RegioInternetAdmin(admin.ModelAdmin):
    list_display = ('region','date')
    list_filter = ('region',)


admin.site.register(InternetUserLocal, InternetAdmin)
admin.site.register(InternetUserRegion, RegioInternetAdmin)


class InternetSpeedAdmin(admin.ModelAdmin):
    list_display = ('local','date')
    list_filter = ('local',)


class RegionJobsee(admin.ModelAdmin):
    list_display = ('region','date')
    list_filter = ('region',)

admin.site.register(InternetSpeed, InternetSpeedAdmin)
admin.site.register(JobSeeking, InternetSpeedAdmin)
admin.site.register(JobSeekingRegion, RegionJobsee)
