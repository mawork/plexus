__author__ = 'sam'

from django.db import models
from django_extensions.db.fields import (AutoSlugField, CreationDateTimeField, ModificationDateTimeField)
from django.db.models import Sum
import itertools


class LocalAuthority(models.Model):
    name = models.CharField(max_length=255, verbose_name="Local Authority")
    slug = AutoSlugField(populate_from=('name',), unique=True)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return self.name

    def totalpeople_api(self, year):
        total = 0
        total += self.inemployment_set.filter(end__year=year).order_by('-end')[0].value
        total += self.unemployment_set.filter(end__year=year).order_by('-end')[0].value
        total += self.inactive_set.filter(end__year=year).order_by('-end')[0].value
        return total

    def jobseeking_api(self):
        duct = []
        for item in self.jobseeking_set.all():
            dt= {}
            dt['date'] = item.date
            dt['value'] = item.value

            pop = 0
            try:
                pop = Population.objects.get(local=self, year=item.date.year).value
            except:
                pop = Population.objects.get(local=self, year=item.date.year -1).value
            try:
                perc = (float(item.value) / float(pop)) * float(100)
            except:
                perc = 0

            dt['percentage']="{0:.2f}".format(perc)
            dt['change'] = item.last
            dt['up_six'] = item.up_six
            dt['six_to_one'] = item.six_to_one
            dt['one_to_two'] = item.one_to_two
            dt['two_to_five'] = item.two_to_five
            dt['five_over'] = item.five_over
            dt['unkown'] = item.unkown
            duct.append(dt)
        return duct

    def disabilityliving_api(self):
        duct = {}
        for item in self.disabilitylivingallowance_set.all():
            dt = {}
            value = self.disabilitylivingallowance_set.filter(date=item.date).aggregate(Sum('value'))['value__sum']
            pop = 0
            try:
                pop = Population.objects.get(local=self, year=item.date.year).value
            except:
                pop = Population.objects.get(local=self, year=item.date.year -1).value
            try:
                perc = (float(item.value)/ float(pop)) * float(100)
            except:
                perc = 0.0

            dt['date'] = item.date
            dt['value'] = item.value
            dt['percetange'] ="{0:.2f}".format(perc)

            if  item.condition.name in duct:
                duct[item.condition.name].append(dt)
            else:
                duct[item.condition.name] = []
                duct[item.condition.name].append(dt)
        dl = self.disabilitylivingallowance_set.all().values_list('date').distinct()
        dates = list(itertools.chain(*dl))
        duct['total'] = []
        for item in dates:
            value = self.disabilitylivingallowance_set.filter(date=item).aggregate(Sum('value'))['value__sum']
            pop = 0
            try:
                pop = Population.objects.get(local=self, year=item.year).value
            except:
                pop = Population.objects.get(local=self, year=item.year -1).value
            try:
                perc = (float(value)/ float(pop)) * float(100)
            except:
                perc = 0

            dt = {}
            dt['date'] = item
            dt['value'] = value
            dt['percetange'] ="{0:.2f}".format(perc)
            duct['total'].append(dt)
        return duct

    def inempl_api(self):
        duct =[]
        for item in self.inemployment_set.all():
            pop = 0
            try:
                pop = Population.objects.get(local=self, year=item.end.year).value
            except:
                pop = Population.objects.get(local=self, year=item.end.year -1).value
            try:
                perc = (float(item.value)/ float(pop)) * float(100)
            except:
                perc = 0

            total = InEmployment.objects.filter(end=item.end).aggregate(Sum('value'))['value__sum']
            toperc = (float(item.value)/ float(total)) * float(100)
            lceffer = self.totalpeople_api(item.end.year) or 0.0
            dt = {}
            dt['year'] = item.end.year
            dt['value'] = item.value
            dt['url'] = '/api/v1/inemployment/'+str(item.id)+'/'
            dt['percentage'] = "{0:.2f}".format(perc)
            dt['everyone'] = total
            dt['percentagetotal'] = "{0:.2f}".format(toperc)
            dt['localpopulation'] = pop
            dt['localaurtheffect'] = lceffer
            duct.append(dt)
        return duct

    def unempl_api(self):
        duct =[]
        for item in self.unemployment_set.all():
            pop = 0
            try:
                pop = Population.objects.get(local=self, year=item.end.year).value
            except:
                pop = Population.objects.get(local=self, year=item.end.year -1).value

            try:
                perc = (float(item.value)/ float(pop)) * float(100)
            except:
                perc = 0
            total = UnEmployment.objects.filter(end=item.end).aggregate(Sum('value'))['value__sum']
            toperc = (float(item.value)/ float(total)) * float(100)
            lceffer = self.totalpeople_api(item.end.year) or 0.0
            dt = {}
            dt['year'] = item.end.year
            dt['value'] = item.value
            dt['url'] = '/api/v1/unemployment/'+str(item.id)+'/'
            dt['percentage'] = "{0:.2f}".format(perc)
            dt['everyone'] = total
            dt['percentagetotal'] = "{0:.2f}".format(toperc)
            dt['localpopulation'] = pop
            dt['localaurtheffect'] = lceffer
            duct.append(dt)
        return duct

    def inact_api(self):
        duct =[]
        for item in self.inactive_set.all():
            pop = 0
            try:
                pop = Population.objects.get(local=self, year=item.end.year).value
            except:
                pop = Population.objects.get(local=self, year=item.end.year -1).value

            try:
                perc = (float(item.value)/ float(pop)) * float(100)
            except:
                perc = 0
            total = InActive.objects.filter(end=item.end).aggregate(Sum('value'))['value__sum']
            toperc = (float(item.value)/ float(total)) * float(100)
            lceffer = self.totalpeople_api(item.end.year) or 0.0
            dt = {}
            dt['year'] = item.end.year
            dt['value'] = item.value
            dt['url'] = '/api/v1/inactive/'+str(item.id)+'/'
            dt['percentage'] = "{0:.2f}".format(perc)
            dt['everyone'] = total
            dt['percentagetotal'] = "{0:.2f}".format(toperc)
            dt['localpopulation'] = pop
            dt['localaurtheffect'] = lceffer
            duct.append(dt)
        return duct

    def populat_api(self):
        duct =[]
        for item in self.population_set.all():
            dt ={}
            dt['year'] = item.year | 0
            dt['value'] = item.value | 0
            dt['url'] = '/api/v1/population/'+str(item.id)+'/'
        return duct


class InEmployment(models.Model):
    start = models.DateField(blank=True, null=True)
    end = models.DateField(blank=True, null=True)
    local = models.ForeignKey(LocalAuthority)
    value = models.FloatField(blank=True, null=True)
    conf = models.FloatField(blank=True, null=True)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return "{0} in {1} value {2}".format(self.local.name, self.end, self.value)


class UnEmployment(models.Model):
    start = models.DateField(blank=True, null=True)
    end = models.DateField(blank=True, null=True)
    local = models.ForeignKey(LocalAuthority)
    value = models.FloatField(blank=True, null=True)
    conf = models.FloatField(blank=True, null=True)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return "{0} in {1} value {2}".format(self.local.name, self.end, self.value)


class InActive(models.Model):
    start = models.DateField(blank=True, null=True)
    end = models.DateField(blank=True, null=True)
    local = models.ForeignKey(LocalAuthority)
    value = models.FloatField(blank=True, null=True)
    conf = models.FloatField(blank=True, null=True)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return "{0} in {1} value {2}".format(self.local.name, self.end, self.value)


class Population(models.Model):
    local = models.ForeignKey(LocalAuthority)
    year = models.IntegerField()
    value = models.IntegerField()
    effected = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return "{0} in {1} value {2}".format(self.local.name, self.year, self.value)


class Region(models.Model):
    name = models.CharField(max_length=255, verbose_name="Region")
    slug = AutoSlugField(populate_from=('name',), unique=True)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return self.name

    def unemp_first(self):
        duct = self.unempl_api()
        if len(duct) > 1:
            return duct[-1]
        else:
            return duct

    def unemp_sec(self):
        duct = self.unempl_api()
        if len(duct) > 2:
            return duct[-2]
        else:
            return duct


    def inempl_api(self):
        duct = []
        for item in self.regioninemployment_set.all():
            pop = 0
            try:
                pop = RegionPopulation.objects.get(region=self, year=item.end.year).value
            except:
                pop = RegionPopulation.objects.get(region=self, year=item.end.year -1).value

            try:
                perc = (float(item.value)/ float(pop)) * float(100)
            except:
                perc = 0
            total = RegionInEmployment.objects.filter(end=item.end).aggregate(Sum('value'))['value__sum']
            toperc = (float(item.value)/ float(total)) * float(100)
            dt = {}
            dt['year'] = item.end.year
            dt['value'] = item.value
            dt['url'] = '/api/v1/region_inemployment/'+str(item.id)+'/'
            dt['percentageoffullpop'] = "{0:.2f}".format(perc)
            dt['everyone'] = total
            dt['percentagetotal'] = "{0:.2f}".format(toperc)
            dt['percentagenum'] = (toperc * 300) / 100
            dt['regionpopulation'] = pop
            duct.append(dt)
        return duct

    def unempl_api(self):
        duct =[]
        for item in self.regionunemployment_set.all():
            pop = 0
            try:
                pop = RegionPopulation.objects.get(region=self, year=item.end.year).value
            except:
                pop = RegionPopulation.objects.get(region=self, year=item.end.year -1).value

            try:
                perc = (float(item.value)/ float(pop)) * float(100)
            except:
                perc = 0
            total = RegionUnEmployment.objects.filter(end=item.end).aggregate(Sum('value'))['value__sum']
            toperc = (float(item.value)/ float(total)) * float(100)
            dt = {}
            dt['year'] = item.end.year
            dt['value'] = item.value
            dt['url'] = '/api/v1/region_unemployment/'+str(item.id)+'/'
            dt['percentage'] = "{0:.2f}".format(perc)
            dt['everyone'] = total
            dt['percentagetotal'] = "{0:.2f}".format(toperc)
            dt['percentagenum'] = (toperc * 300) / 100
            dt['regionpopulation'] = pop
            duct.append(dt)
        return duct

    def inact_api(self):
        duct =[]
        for item in self.regioninactive_set.all():
            pop = 0
            try:
                pop = RegionPopulation.objects.get(region=self, year=item.end.year).value
            except:
                pop = RegionPopulation.objects.get(region=self, year=item.end.year -1).value

            try:
                perc = (float(item.value)/ float(pop)) * float(100)
            except:
                perc = 0
            total = RegionInActive.objects.filter(end=item.end).aggregate(Sum('value'))['value__sum']
            toperc = (float(item.value)/ float(total)) * float(100)
            dt = {}
            dt['year'] = item.end.year
            dt['value'] = item.value
            dt['url'] = '/api/v1/region_inactive/'+str(item.id)+'/'
            dt['percentage'] = "{0:.2f}".format(perc)
            dt['everyone'] = total
            dt['percentagetotal'] = "{0:.2f}".format(toperc)
            dt['regionpopulation'] = pop
            duct.append(dt)
        return duct


    def populat_api(self):
        duct =[]
        for item in self.regionpopulation_set.all():
            dt ={}
            dt['year'] = item.year | 0
            dt['value'] = item.value | 0
            dt['url'] = '/api/v1/region_population/'+str(item.id)+'/'
        return duct


class RegionInEmployment(models.Model):
    start = models.DateField(blank=True, null=True)
    end = models.DateField(blank=True, null=True)
    region = models.ForeignKey(Region)
    value = models.FloatField(blank=True, null=True)
    conf = models.FloatField(blank=True, null=True)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return "{0} in {1} value {2}".format(self.region.name, self.end, self.value)


class RegionUnEmployment(models.Model):
    start = models.DateField(blank=True, null=True)
    end = models.DateField(blank=True, null=True)
    region = models.ForeignKey(Region)
    value = models.FloatField(blank=True, null=True)
    conf = models.FloatField(blank=True, null=True)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return "{0} in {1} value {2}".format(self.region.name, self.end, self.value)


class RegionInActive(models.Model):
    start = models.DateField(blank=True, null=True)
    end = models.DateField(blank=True, null=True)
    region = models.ForeignKey(Region)
    value = models.FloatField(blank=True, null=True)
    conf = models.FloatField(blank=True, null=True)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return "{0} in {1} value {2}".format(self.region.name, self.end, self.value)


class RegionPopulation(models.Model):
    region = models.ForeignKey(Region)
    year = models.IntegerField()
    value = models.IntegerField()

    def __unicode__(self):
        return "{0} in {1} value {2}".format(self.region.name, self.year, self.value)


class ConditionName(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name


class DisabilityLivingAllowance(models.Model):
    date = models.DateField()
    local = models.ForeignKey(LocalAuthority)
    condition = models.ForeignKey(ConditionName)
    value = models.IntegerField()

    def __unicode__(self):
        return "{0}, {1}, {2}".format(self.date, self.local, self.condition)


class DisabilityLivingAllowanceRegion(models.Model):
    date = models.DateField()
    region = models.ForeignKey(Region)
    condition = models.ForeignKey(ConditionName)
    value = models.IntegerField()

    def __unicode__(self):
        return "{0}, {1}, {2}".format(self.date, self.region, self.condition)


class InternetUserLocal(models.Model):
    date = models.DateField()
    local = models.ForeignKey(LocalAuthority)
    last_three_per = models.FloatField(blank=True, null=True)
    more_three_per = models.FloatField(blank=True, null=True)
    never_per = models.FloatField(blank=True, null=True)

    def __unicode__(self):
        return "{0} , {1}".format(self.date, self.local)


class InternetUserRegion(models.Model):
    date = models.DateField()
    region = models.ForeignKey(Region)
    last_three_per = models.FloatField(blank=True, null=True)
    more_three_per = models.FloatField(blank=True, null=True)
    never_per = models.FloatField(blank=True, null=True)

    def __unicode__(self):
        return "{0} , {1}".format(self.date, self.region)


class InternetSpeed(models.Model):
    date = models.DateField()
    local = models.ForeignKey(LocalAuthority)
    avg_speed = models.FloatField(blank=True, null=True, help_text='Average sync speed (Mbit/s)')
    avg_score = models.FloatField(blank=True, null=True, help_text='Average sync speed score')
    perc_not = models.FloatField(blank=True, null=True, help_text='Percentage not receiving 2Mbit/s')
    perc_not_score = models.FloatField(blank=True, null=True, help_text='Percentage not receiving 2Mbit/s score')
    superfast = models.FloatField(blank=True, null=True, help_text='Superfast broadband availability')
    superfast_score = models.FloatField(blank=True, null=True, help_text='Superfast broadband availability score')
    takeup = models.FloatField(blank=True, null=True, help_text='Take-up (including superfast broadband)')
    takeup_score = models.FloatField(blank=True, null=True, help_text='Take-up score')
    superfast_takeup = models.FloatField(blank=True, null=True, help_text='Superfast takeup')
    overall_score = models.FloatField(blank=True, null=True, help_text='Overall Total of Scores')
    quality_score = models.FloatField(blank=True, null=True, help_text='Quality Score')
    data_throughtput = models.FloatField(blank=True, null=True, help_text='Data Throughput')

    def __unicode__(self):
        return "{0} , {1}".format(self.date, self.local)


class JobSeeking(models.Model):
    date = models.DateField()
    local = models.ForeignKey(LocalAuthority)
    value = models.FloatField(blank=True, null=True)
    last = models.CharField(blank=True, null=True, max_length=10)
    up_six = models.IntegerField(blank=True, null=True)
    six_to_one = models.IntegerField(blank=True, null=True)
    one_to_two = models.IntegerField(blank=True, null=True)
    two_to_five = models.IntegerField(blank=True, null=True)
    five_over = models.IntegerField(blank=True, null=True)
    unkown = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return "{0} , {1} - {2}".format(self.date, self.local, self.value)


class JobSeekingRegion(models.Model):
    date = models.DateField()
    region = models.ForeignKey(Region)
    value = models.FloatField(blank=True, null=True)
    last = models.CharField(blank=True, null=True, max_length=10)
    up_six = models.IntegerField(blank=True, null=True)
    six_to_one = models.IntegerField(blank=True, null=True)
    one_to_two = models.IntegerField(blank=True, null=True)
    two_to_five = models.IntegerField(blank=True, null=True)
    five_over = models.IntegerField(blank=True, null=True)
    unkown = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return "{0} , {1} - {2}".format(self.date, self.region, self.value)
