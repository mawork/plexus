from .models import *
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.paginator import Paginator


class LocalArthorityRes(ModelResource):

    def dehydrate(self, bundle):
        bundle.data['inemployment'] = bundle.obj.inempl_api()
        bundle.data['unemployment'] = bundle.obj.unempl_api()
        bundle.data['inactive'] = bundle.obj.inact_api()
        bundle.data['population'] = bundle.obj.populat_api()
        bundle.data['disabilityliving'] = bundle.obj.disabilityliving_api()
        bundle.data['jobseeking'] = bundle.obj.jobseeking_api()

        return bundle

    class Meta:
        queryset = LocalAuthority.objects.all()
        resource_name = 'localathority'
        detail_allowed_methods = ['get',]
        filtering = {
            'name':ALL,
            'slug':ALL,
            'created':ALL,
            'modified':ALL,
            'id':ALL
        }
        paginator_class = Paginator
        limit = 0
        max_limit = 0


class LocalArthorityName(ModelResource):
    class Meta:
        queryset = LocalAuthority.objects.all()
        resource_name = 'localathority_name'
        detail_allowed_methods = ['get',]
        filtering = {
            'name':ALL,
            'slug':ALL,
            'created':ALL,
            'modified':ALL,
            'id':ALL
        }
        paginator_class = Paginator
        limit = 0
        max_limit = 0

class InEmRes(ModelResource):
    class Meta:
        queryset = InEmployment.objects.all()
        resource_name = 'inemployment'
        detail_allowed_methods = ['get',]
        filtering = {
            'start':ALL,
            'end':ALL,
            'local':ALL,
            'value':ALL,
            'conf':ALL,
            'created':ALL,
            'modified':ALL,
            'id':ALL
        }
        paginator_class = Paginator
        limit = 0
        max_limit = 0


class UnEmRes(ModelResource):
    class Meta:
        queryset = UnEmployment.objects.all()
        resource_name = 'unemployment'
        detail_allowed_methods = ['get',]
        filtering = {
            'start':ALL,
            'end':ALL,
            'local':ALL,
            'value':ALL,
            'conf':ALL,
            'created':ALL,
            'modified':ALL,
            'id':ALL
        }
        paginator_class = Paginator
        limit = 0
        max_limit = 0


class InActRes(ModelResource):
    class Meta:
        queryset = InActive.objects.all()
        resource_name = 'inactive'
        detail_allowed_methods = ['get',]
        filtering = {
            'start':ALL,
            'end':ALL,
            'local':ALL,
            'value':ALL,
            'conf':ALL,
            'created':ALL,
            'modified':ALL,
            'id':ALL
        }
        paginator_class = Paginator
        limit = 0
        max_limit = 0


class LocPopRes(ModelResource):
    class Meta:
        queryset = Population.objects.all()
        resource_name = 'population'
        detail_allowed_methods = ['get',]
        limit = 0
        max_limit = 0


class RegionRes(ModelResource):
    def dehydrate(self, bundle):
        bundle.data['inemployment'] = bundle.obj.inempl_api()
        bundle.data['unemployment'] = bundle.obj.unempl_api()
        bundle.data['inactive'] = bundle.obj.inact_api()
        bundle.data['population'] = bundle.obj.populat_api()
        bundle.data['first'] = bundle.obj.unemp_first()
        bundle.data['second'] = bundle.obj.unemp_sec()
        return bundle


    class Meta:
        queryset = Region.objects.all()
        resource_name = 'region'
        detail_allowed_methods = ['get',]
        filtering = {
            'name':ALL,
            'slug':ALL,
            'created':ALL,
            'modified':ALL,
            'id':ALL
        }
        paginator_class = Paginator
        limit = 0
        max_limit = 0


class RegInEmRes(ModelResource):
    class Meta:
        queryset = RegionInEmployment.objects.all()
        resource_name = 'region_inemployment'
        detail_allowed_methods = ['get',]
        filtering = {
            'start':ALL,
            'end':ALL,
            'local':ALL,
            'value':ALL,
            'conf':ALL,
            'created':ALL,
            'modified':ALL,
            'id':ALL
        }
        paginator_class = Paginator
        limit = 0
        max_limit = 0


class RegUnEmRes(ModelResource):
    class Meta:
        queryset = RegionUnEmployment.objects.all()
        resource_name = 'region_unemployment'
        detail_allowed_methods = ['get',]
        filtering = {
            'start':ALL,
            'end':ALL,
            'local':ALL,
            'value':ALL,
            'conf':ALL,
            'created':ALL,
            'modified':ALL,
            'id':ALL
        }
        paginator_class = Paginator
        limit = 0
        max_limit = 0


class RegInActRes(ModelResource):
    class Meta:
        queryset = RegionInActive.objects.all()
        resource_name = 'region_inactive'
        detail_allowed_methods = ['get',]
        filtering = {
            'start':ALL,
            'end':ALL,
            'local':ALL,
            'value':ALL,
            'conf':ALL,
            'created':ALL,
            'modified':ALL,
            'id':ALL
        }
        paginator_class = Paginator
        limit = 0
        max_limit = 0


class RegPopRes(ModelResource):
    class Meta:
        queryset = RegionPopulation.objects.all()
        resource_name = 'region_population'
        detail_allowed_methods = ['get',]
        filtering = {
            'local':ALL,
            'value':ALL,
            'created':ALL,
            'modified':ALL,
            'id':ALL
        }
        paginator_class = Paginator
        limit = 0
        max_limit = 0


class ConditionRes(ModelResource):
    class Meta:
        queryset = ConditionName.objects.all()
        resource_name = 'conditions'
        detail_allowed_methods = ['get',]
        filtering = {
            'name':ALL
        }
        paginator_class = Paginator
        limit = 0
        max_limit = 0


class DisabilityRes(ModelResource):
    local = fields.ToOneField(LocalArthorityRes, 'local', full=True)
    condition = fields.ToOneField(ConditionRes, 'condition', full=True)

    class Meta:
        queryset = DisabilityLivingAllowance.objects.all()
        resource_name = 'disability_living_allowance'
        detail_allowed_methods = ['get',]
        filtering = {
            'date':ALL,
            'value':ALL,
            'local':ALL_WITH_RELATIONS,
            'condition':ALL_WITH_RELATIONS
        }
        paginator_class = Paginator
        limit = 0
        max_limit = 0
