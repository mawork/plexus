__author__ = 'sam'

from .models import *
from datetime import datetime
import pandas as pd

'''
Commented it out as it is only used ones to create the dataset from sourced data, on local site. Wouldn't load onto server so commented it out as not needed.
need to install pandas == 0.16.2
'''

def setthedata():
    dt = pd.read_csv('/home/sam/Desktop/aroundyou-data/dataDWP.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        la, created = LocalAuthority.objects.get_or_create(name=dt[saved_column[0]][i])
        ie, iecre = InEmployment.objects.get_or_create(local=la, start=dt[saved_column[1]][i], end=dt[saved_column[2]][i], value=dt[saved_column[3]][i], conf=dt[saved_column[4]][i])
        ue, uecre = UnEmployment.objects.get_or_create(local=la, start=dt[saved_column[1]][i], end=dt[saved_column[2]][i], value=dt[saved_column[5]][i], conf=dt[saved_column[6]][i])
        ia, iacre = InActive.objects.get_or_create(local=la, start=dt[saved_column[1]][i], end=dt[saved_column[2]][i], value=dt[saved_column[7]][i], conf=dt[saved_column[8]][i])
        la.save()
        ie.save()
        ue.save()
        ia.save()


def setPop():
    dt = pd.read_csv('/home/sam/Desktop/aroundyou-data/population.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        la, created = LocalAuthority.objects.get_or_create(name=dt[saved_column[0]][i])
        pop, pcret = Population.objects.get_or_create(local=la, year=int(saved_column[1]), value=dt[saved_column[1]][i])
        popone, pcretone = Population.objects.get_or_create(local=la, year=int(saved_column[2]), value=dt[saved_column[2]][i])
        la.save()
        pop.save()
        popone.save()


def setRegionthedata():
    dt = pd.read_csv('/home/sam/Desktop/aroundyou-data/regionUnemployemnt.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        la, created = Region.objects.get_or_create(name=dt[saved_column[0]][i])
        ie, iecre = RegionInEmployment.objects.get_or_create(region=la, start=dt[saved_column[1]][i], end=dt[saved_column[2]][i], value=dt[saved_column[3]][i], conf=dt[saved_column[4]][i])
        ue, uecre = RegionUnEmployment.objects.get_or_create(region=la, start=dt[saved_column[1]][i], end=dt[saved_column[2]][i], value=dt[saved_column[5]][i], conf=dt[saved_column[6]][i])
        ia, iacre = RegionInActive.objects.get_or_create(region=la, start=dt[saved_column[1]][i], end=dt[saved_column[2]][i], value=dt[saved_column[7]][i], conf=dt[saved_column[8]][i])
        la.save()
        ie.save()
        ue.save()
        ia.save()


def setRegionPop():
    dt = pd.read_csv('/home/sam/Desktop/aroundyou-data/regionpopulation.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        la, created = Region.objects.get_or_create(name=dt[saved_column[0]][i])
        pop, pcret = RegionPopulation.objects.get_or_create(region=la, year=int(saved_column[1]), value=dt[saved_column[1]][i])
        popone, pcretone = RegionPopulation.objects.get_or_create(region=la, year=int(saved_column[2]), value=dt[saved_column[2]][i])
        la.save()
        pop.save()
        popone.save()


def getDisableAllowed():
    dt = pd.read_csv('/home/sam/Desktop/disableallow.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        con, created = ConditionName.objects.get_or_create(name=dt[saved_column[2]][i])
        con.save()
        la, created = LocalAuthority.objects.get_or_create(name=dt[saved_column[1]][i])
        la.save()
        da, created = DisabilityLivingAllowance.objects.get_or_create(date=datetime.strptime(dt[saved_column[0]][i], '%d/%m/%Y'), local=la, condition=con, value=dt[saved_column[3]][i])
        da.save()


def getRegionDisableAllowed():
    dt = pd.read_csv('/home/sam/Desktop/disableallowRegion.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        con, created = ConditionName.objects.get_or_create(name=dt[saved_column[2]][i])
        con.save()
        la, created = Region.objects.get_or_create(name=dt[saved_column[1]][i])
        la.save()
        da, created = DisabilityLivingAllowanceRegion.objects.get_or_create(date=datetime.strptime(dt[saved_column[0]][i], '%d/%m/%Y'), region=la, condition=con, value=dt[saved_column[3]][i])
        da.save()


def getRegionalInternet():
    dt = pd.read_csv('/home/sam/Desktop/RegionInternet.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        la, created = Region.objects.get_or_create(name=dt[saved_column[0]][i])
        la.save()
        dates =['01/11/2013', '01/02/2014', '01/05/2014', '01/08/2014', '01/11/2014', '01/02/2015']
        count = 0
        for item in dates:
            ir, created = InternetUserRegion.objects.get_or_create(date=datetime.strptime(item, '%d/%m/%Y'),
                                                                   region= la,
                                                                   last_three_per=dt[saved_column[1 + count]][i],
                                                                   more_three_per=dt[saved_column[7 + count]][i],
                                                                   never_per=dt[saved_column[13 + count]][i]
                                                                  )
            ir.save()
            count += 1


def getLocalInternet():
    dt = pd.read_csv('/home/sam/Desktop/InternetUserLowLevelPerc.csv').fillna(0)
    saved_column = dt.columns

    for i in range(0, len(dt)):
        try:
            la = LocalAuthority.objects.get(name=dt[saved_column[0]][i])
            dates =['01/11/2013', '01/02/2014', '01/05/2014', '01/08/2014', '01/11/2014', '01/02/2015']
            count = 0
            for item in dates:
                ir, created = InternetUserLocal.objects.get_or_create(date=datetime.strptime(item, '%d/%m/%Y'),
                                                                      local= la,
                                                                      last_three_per=dt[saved_column[1 + count]][i],
                                                                      more_three_per=dt[saved_column[7 + count]][i]
                                                                      )
                ir.save()
                count += 1

        except LocalAuthority.DoesNotExist:
            pass


def localcheck():
    dt = pd.read_csv('/home/sam/Desktop/ofcome.csv').fillna(0)
    saved_column = dt.columns
    fail = []
    for i in range(0, len(dt)):
        try:
            la = LocalAuthority.objects.get(name=dt[saved_column[0]][i])
            print la
        except LocalAuthority.DoesNotExist:
            fail.extend(dt[saved_column[0]][i])
    print 'Fail'
    print fail


def getLocalInternet():
    dt = pd.read_csv('/home/sam/Desktop/ofcome.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        try:
            la = LocalAuthority.objects.get(name=dt[saved_column[0]][i])
            ir, created = InternetSpeed.objects.get_or_create(date=datetime.strptime('01/01/2013', '%d/%m/%Y'),
                                                            local= la,
                                                            avg_speed=dt[saved_column[1]][i],
                                                            avg_score=dt[saved_column[2]][i],
                                                            perc_not=dt[saved_column[3]][i],
                                                            perc_not_score=dt[saved_column[4]][i],
                                                            superfast=dt[saved_column[5]][i],
                                                            superfast_score=dt[saved_column[6]][i],
                                                            takeup=dt[saved_column[7]][i],
                                                            takeup_score=dt[saved_column[8]][i],
                                                            superfast_takeup=dt[saved_column[9]][i],
                                                            overall_score=dt[saved_column[10]][i],
                                                            quality_score=dt[saved_column[11]][i],
                                                            data_throughtput=dt[saved_column[12]][i]
                                                            )
            ir.save()


        except LocalAuthority.DoesNotExist:
            pass


def getJobSeeking():
    dt = pd.read_csv('/home/sam/Desktop/jobseeking.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        la, created = LocalAuthority.objects.get_or_create(name=dt[saved_column[1]][i])
        la.save()
        da, created = JobSeeking.objects.get_or_create(date=datetime.strptime(dt[saved_column[0]][i], '%d/%m/%Y'), local=la, value=dt[saved_column[2]][i])
        da.save()


def getChange():
    la = LocalAuthority.objects.all()
    for l in la:
        jb = JobSeeking.objects.filter(local=l).order_by('date')
        count = 0
        last = 0
        for j in jb:
            if count == 0:
                j.last = 'start'
                j.save()
            else:
                if last >= j.value:
                    j.last = 'down'
                else:
                    j.last = 'up'
                j.save()
            last = j.value
            count += 1

def JobSeekingDuration():
    dt = pd.read_csv('/home/sam/Desktop/justJobSeekDuration.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        la, created = LocalAuthority.objects.get_or_create(name=dt[saved_column[1]][i])
        la.save()
        da, created = JobSeeking.objects.get_or_create(date=datetime.strptime(dt[saved_column[0]][i], '%Y-%m'), local=la)
        if dt[saved_column[5]][i] == 'up to 6 months':
            da.up_six = dt[saved_column[6]][i]
        elif dt[saved_column[5]][i] == '6 months up to 1 year':
            da.six_to_one = dt[saved_column[6]][i]
        elif dt[saved_column[5]][i] == '1 year and up to 2 years':
            da.one_to_two = dt[saved_column[6]][i]
        elif dt[saved_column[5]][i] == '2 years and up to 5 years':
            da.two_to_five = dt[saved_column[6]][i]
        elif dt[saved_column[5]][i] == '5 years and over':
            da.five_over = dt[saved_column[6]][i]
        elif dt[saved_column[5]][i] == 'unknown duration':
            da.unkown = dt[saved_column[6]][i]
        da.save()

def JobSeekignRegion():
    dt = pd.read_csv('/home/sam/Desktop/regionjobseeking.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        la, created = Region.objects.get_or_create(name=dt[saved_column[1]][i])
        la.save()
        da, created = JobSeekingRegion.objects.get_or_create(date=datetime.strptime(dt[saved_column[0]][i], '%Y-%m'), region=la)
        if dt[saved_column[2]][i] == 'up to 6 months':
            da.up_six = dt[saved_column[3]][i]
        elif dt[saved_column[2]][i] == '6 months up to 1 year':
            da.six_to_one = dt[saved_column[3]][i]
        elif dt[saved_column[2]][i] == '1 year and up to 2 years':
            da.one_to_two = dt[saved_column[3]][i]
        elif dt[saved_column[2]][i] == '2 years and up to 5 years':
            da.two_to_five = dt[saved_column[3]][i]
        elif dt[saved_column[2]][i] == '5 years and over':
            da.five_over = dt[saved_column[3]][i]
        elif dt[saved_column[2]][i] == 'unknown duration':
            da.unkown = dt[saved_column[3]][i]

        if da.value is None:
            if dt[saved_column[2]][i] == 'all durations':
                da.value = dt[saved_column[3]][i]
        da.save()


def printlocalJobseeking():
    import csv
    with open('/home/sam/Desktop/ary/jobseeking_localauthority.csv', 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='"')
        writer.writerow(['Local Authority', 'Date', 'value', 'percentage','Up to Six months', 'Six months to 1 year', '1 - 2 year','2 - 5 year','over five year', 'unkown' ])
        for l in LocalAuthority.objects.all():
            for u in l.jobseeking_set.all():
                pop = 0
                try:
                    pop = Population.objects.get(local=l, year=u.date.year).value
                except:
                    pop = Population.objects.get(local=l, year=u.date.year -1).value
                else:
                    pop = 0
                try:
                    perc = (float(l.value) / float(pop)) * float(100)
                except:
                    perc = 0
                writer.writerow([l.name, u.date, u.value, "{0:.2f}".format(perc), u.up_six, u.six_to_one, u.one_to_two, u.two_to_five, u.five_over, u.unkown])

def printlocalHealth():
    import csv
    with open('/home/sam/Desktop/ary/health_localauthority.csv', 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='"')
        writer.writerow(['Local Authority', 'Date', 'value', 'percentage'])
        for l in LocalAuthority.objects.all():
            for u in l.disabilitylivingallowance_set.all():
                value = l.disabilitylivingallowance_set.filter(date=u.date).aggregate(Sum('value'))['value__sum']
                pop = 0
                try:
                    pop = Population.objects.get(local=l, year=u.date.year).value
                    perc = (float(u.value)/ float(pop)) * float(100)
                except:
                    pop = Population.objects.get(local=l, year=u.date.year -1).value
                    perc = (float(u.value)/ float(pop)) * float(100)
                else:
                    perc = 0.0

                writer.writerow([l.name, u.date, u.value, "{0:.2f}".format(perc)])


def printregionJobseeking():
    import csv
    with open('/home/sam/Desktop/ary/jobseeking_region.csv', 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='"')
        writer.writerow(['Region', 'Date', 'value','Up to Six months', 'Six months to 1 year', '1 - 2 year','2 - 5 year','over five year', 'unkown' ])
        for l in Region.objects.all():
            for u in l.jobseekingregion_set.all():
                writer.writerow([l.name, u.date, u.value,  u.up_six, u.six_to_one, u.one_to_two, u.two_to_five, u.five_over, u.unkown])


def printregionhealth():
    import csv
    with open('/home/sam/Desktop/ary/health_region.csv', 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='"')
        writer.writerow(['Region', 'Date', 'value'])
        for l in Region.objects.all():
            for u in l.disabilitylivingallowanceregion_set.all():
                writer.writerow([l.name, u.date, u.value])


def printregionemp():
    import csv
    with open('/home/sam/Desktop/ary/empl_local.csv', 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='"')
        writer.writerow(['local', 'start', 'end', 'value'])
        for l in InEmployment.objects.all():
            writer.writerow([l.local, l.start, l.end, l.value])
    with open('/home/sam/Desktop/ary/unempl_local.csv', 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='"')
        writer.writerow(['local', 'start', 'end', 'value'])
        for l in UnEmployment.objects.all():
            writer.writerow([l.local, l.start, l.end, l.value])
    with open('/home/sam/Desktop/ary/inactive_local.csv', 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='"')
        writer.writerow(['local', 'start', 'end', 'value'])
        for l in InActive.objects.all():
            writer.writerow([l.local, l.start, l.end, l.value])
    with open('/home/sam/Desktop/ary/population_local.csv', 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='"')
        writer.writerow(['local', 'year', 'value'])
        for l in Population.objects.all():
            writer.writerow([l.local, l.year, l.value])


