from django.contrib import admin
from .models import *
from tinymce.widgets import TinyMCE

class LegalAdmin(admin.ModelAdmin):
    list_display=('title','slug', 'sort_value', 'is_public')
    list_editable=('sort_value', 'is_public')
    formfield_overrides = {
        models.TextField: {'widget': TinyMCE(attrs={'cols': 100, 'rows': 50})},
    }

admin.site.register(LegalNotice, LegalAdmin)
