# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('legal', '0002_auto_20150903_1309'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='legalnotice',
            options={'ordering': ['sort_value'], 'verbose_name': 'Legal Notice', 'verbose_name_plural': 'Legal Notice'},
        ),
    ]
