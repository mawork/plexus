from .models import *
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

class LegalResources(ModelResource):
    url = fields.CharField(attribute='url')
    class Meta:
        queryset = LegalNotice.objects.filter(is_public=True)
        resource_name = 'legal'
        allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'sort_value':ALL,
            'body':ALL
        }
