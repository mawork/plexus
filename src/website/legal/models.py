from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import (AutoSlugField, CreationDateTimeField, ModificationDateTimeField)

class LegalNotice(models.Model):
    title = models.CharField(max_length=255, verbose_name="Page title")
    sort_value = models.IntegerField(blank=True, null=True)
    is_public = models.BooleanField(default=False)
    body = models.TextField(blank=True, null=True)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()
    slug = AutoSlugField(populate_from=('title',), unique=True)

    def __unicode__(self):
        return self.title

    @property
    def url(self):
        return '/legal/{0}'.format(self.slug)

    class Meta:
        ordering =['sort_value',]
        verbose_name = _('Legal Notice')
        verbose_name_plural = _('Legal Notice')
