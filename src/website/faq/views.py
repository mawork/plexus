from .models import *
from django.views.generic import DetailView, ListView
from django.shortcuts import render_to_response, get_object_or_404

class FaqList(ListView):
    template_name='faq/faq.html'
    model=Faq
    context_object_name = "faq"

faq_list = FaqList.as_view()
