from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import (AutoSlugField, CreationDateTimeField, ModificationDateTimeField)
from mediastore.fields import MediaField, MultipleMediaField


def faq_count():
    try:
        return Faq.objects.count()
    except:
        return 0


class Category(models.Model):
    title = models.CharField(_('title'), max_length=255)
    slug = AutoSlugField(populate_from='title', unique=True)
    sort_value = models.IntegerField(_('sort value'), default=0, db_index=True)
    is_public = models.BooleanField(_('Public'), default=True)

    def __unicode__(self):
        return self.title

    @property
    def url(self):
        return "/faq/{0}".format(self.slug)

    class Meta:
        ordering = ['sort_value']
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')


class Faq(models.Model):
    question = models.TextField(_('Question'))
    answer = models.TextField(_('Answer'))
    date = models.DateField(_('Date'))
    is_public = models.BooleanField(_('Public'), default=False)
    sort_order = models.IntegerField(_('Sort Order'), default=faq_count)
    category = models.ManyToManyField(Category, blank=True, null=True)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return self.question

    @property
    def url(self):
        return "/faq/{0}/{1}".format(self.slug, self.category.first().slug)

    class Meta:
        ordering = ['sort_order']