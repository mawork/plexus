from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import (AutoSlugField, CreationDateTimeField, ModificationDateTimeField)
from mediastore.fields import MediaField, MultipleMediaField


class Quote(models.Model):
    source = models.CharField(_('Source'), max_length=255)
    logo = MediaField(
        related_name='quote_logo',
        limit_choices_to={'content_type__model': 'image'},
        null=True, blank=True)
    quote = models.TextField(_('Quote'))
    is_public = models.BooleanField(default=False)
    link = models.URLField(_('Link'), blank=True, null=True)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return self.source


class Release(models.Model):
    title = models.CharField(_('Title'), max_length=255)
    date = models.DateField(_('Date'))
    is_public = models.BooleanField(default=False)
    link = models.URLField(_('Link'), blank=True, null=True)
    file = MediaField(
        related_name='release_document',
        limit_choices_to={'content_type__model': ('pdf', 'downloads')},
        null=True, blank=True)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return self.title
