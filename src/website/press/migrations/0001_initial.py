# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_extensions.db.fields
import mediastore.fields.related
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('mediastore', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Quote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('source', models.CharField(max_length=255, verbose_name='Source')),
                ('quote', models.TextField(verbose_name='Quote')),
                ('is_public', models.BooleanField(default=False)),
                ('link', models.URLField(null=True, verbose_name='Link', blank=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
                ('logo', mediastore.fields.related.MediaField(related_name='quote_logo', blank=True, to='mediastore.Media', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Release',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('date', models.DateField(verbose_name='Date')),
                ('is_public', models.BooleanField(default=False)),
                ('link', models.URLField(null=True, verbose_name='Link', blank=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
                ('file', mediastore.fields.related.MediaField(related_name='release_document', blank=True, to='mediastore.Media', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
