from django.contrib import admin
from .models import *
from mediastore.admin import ModelAdmin

class QuoteAdmin(ModelAdmin):
    pass


admin.site.register(Quote, QuoteAdmin)
admin.site.register(Release, QuoteAdmin)

