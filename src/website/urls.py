"""website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url, patterns
from django.contrib import admin
from tastypie.api import Api
from django.conf import settings
from django.views.generic import TemplateView

from website.hospitals.resources import *
from .views import *
from website.doit.resources import *
from website.legal.resources import *
from website.ourdata.resources import *
from website.backtowork.resources import *
from website.mydetails.resources import *
from website.frontpages.resources import *
from website.aroundyou.resources import *
from website.nhs.resources import *

from website.shelter.resources import TypeReosurce, \
                                    ServiceResources,\
                                    PostCodeResources, \
                                    SubItemReousrces as Sheltersub, \
                                    ItemResources as ShelterItem, \
                                    SectionResources as ShelterSection

from website.physical.resources import *
from website.finance.resources import *

from .resource import *

v1_api = Api(api_name='v1')

v1_api.register(OppResources())         #doit
v1_api.register(JobResources())         #doit
v1_api.register(LegalResources())       #legal
v1_api.register(OurdataResources())     #our data
v1_api.register(SectionResources())     #back to work
v1_api.register(OldSectionResources())  #back to work
v1_api.register(SubItemReousrces())     #back to work
v1_api.register(ItemResources())        #back to work
v1_api.register(OldItemResources())        #back to work
v1_api.register(AcheivementResources()) #my details
v1_api.register(DownloadResources())    #our data
v1_api.register(ChangeResource())       #our data
v1_api.register(mailListResource())     #our data
v1_api.register(FrontPageResources())   #front pages
v1_api.register(FlatBlockResources())   #Flat block elements
v1_api.register(LocalArthorityRes())    #LocalAuth
v1_api.register(InEmRes())              #In employed data
v1_api.register(UnEmRes())              #Un employed data
v1_api.register(InActRes())             #in active data
v1_api.register(LocPopRes())            #population data
v1_api.register(RegionRes())            #LocalAuth
v1_api.register(RegInEmRes())           #In employed data
v1_api.register(RegUnEmRes())           #Un employed data
v1_api.register(RegInActRes())          #in active data
v1_api.register(RegPopRes())            #population data
v1_api.register(LocalArthorityName())   #Local auth number

v1_api.register(TrustResources())       #TrustResources
v1_api.register(MetricResources())          #MetricResources
v1_api.register(TrustValuesResources())
v1_api.register(HospitalResources())
v1_api.register(HospitalValuesResources())
v1_api.register(TrustAddressResources())
v1_api.register(ServiceProvResources())
v1_api.register(TrustService())
v1_api.register(HospitalService())

v2_api = Api(api_name='old')
v2_api.register(OrgResources())         #hospital
v2_api.register(OrgServResources())     #hospital
v2_api.register(SectorResources())      #hospital

v3_api = Api(api_name='shelter')
v3_api.register(PostCodeResources())    #shelter
v3_api.register(ServiceResources())     #shelter
v3_api.register(TypeReosurce())         #shelter
v3_api.register(Sheltersub())           #shelter
v3_api.register(ShelterItem())          #shelter
v3_api.register(ShelterSection())       #shelter

v4_api = Api(api_name='physical')
v4_api.register(PhysicalSubItemReousrces())       #shelter
v4_api.register(PhysicalItemResources())          #shelter
v4_api.register(PhysicalSectionResources())       #shelter

v5_api = Api(api_name='finance')
v5_api.register(FinanceSubItemReousrces())       #finance
v5_api.register(FinanceItemsResources())         #finance
v5_api.register(FinanceSectionResources())       #finance

urlpatterns = patterns('',
    #Admin
    url(r'^admin/', include(admin.site.urls)),

    #Mobile API
    url(r'^api/', include(v1_api.urls)),
    url(r'^api/', include(v2_api.urls)),
    url(r'^api/', include(v3_api.urls)),
    url(r'^api/', include(v4_api.urls)),
    url(r'^api/', include(v5_api.urls)),

    url(r'^user/$', 'website.mydetails.views.getdata', name='user_data'),
    url(r'^user/update/$', 'website.mydetails.views.update', name='user_update'),
    url(r'^user/registration/$', 'website.mydetails.views.registration', name='user_registration'),
    url(r'^user/login/$', 'website.mydetails.views.login', name='user_login'),
    url(r'^user/facebooklogin/$', 'website.mydetails.views.facebooklogin', name='user_facebooklogin'),
    url(r'^user/googlelogin/$', 'website.mydetails.views.googlelogin', name='user_googlelogin'),
    url(r'^user/image/$', 'website.mydetails.views.image', name='user_image'),
    url(r'^user/maillist/$', 'website.ourdata.views.maillist', name='maillist_add'),
    url(r'^data/(.*)$', 'website.views.views.getdata', name='data'),

    #Desktop access
    url(r'^home/$','website.views.views.desktop', name='desktop'),
    url(r'^home/about/$','website.views.views.about', name='about'),
    url(r'^home/contact/$','website.views.views.contact', name='contact'),
    url(r'^home/press/$','website.views.views.press', name='press'),
    url(r'^home/blog/$','website.blog.views.blog_list', name='blog'),
    url(r'^home/blog/(?P<slug>[^/]+)/$','website.blog.views.blog_post', name='blog_post'),
    url(r'^home/faq/$','website.faq.views.faq_list', name='faq'),

    url(r'service-and-support', 'website.views.views.appload', name='appload'),
    url(r'working-opportunities', 'website.views.views.appload', name='appload'),
    url(r'preparing-for-work', 'website.views.views.appload', name='appload'),
    url(r'legal-rights', 'website.views.views.appload', name='appload'),
    url(r'money-management', 'website.views.views.appload', name='appload'),
    url(r'housing', 'website.views.views.appload', name='appload'),
    url(r'physical-activities', 'website.views.views.appload', name='appload'),

    #Decider - Angular start
    url(r'^$', 'website.views.views.index', name='home'),
)

if settings.DEBUG:
    urlpatterns += patterns('',
                            url(r'^media/(.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
                            )
