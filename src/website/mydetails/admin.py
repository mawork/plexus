from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from mediastore.admin import ModelAdmin
from tinymce.widgets import TinyMCE
from .models import *
from .forms import CustomUserChangeForm, CustomUserCreationForm

class UserAchAdmin(admin.StackedInline):
    model = UserAchievements
    formfield_overrides = {
        models.TextField: {'widget': TinyMCE(attrs={'cols': 100, 'rows': 50})},
    }


class AchevementAdmin(ModelAdmin):
    list_display=('title',)
    extra = 1
    formfield_overrides = {
        models.TextField: {'widget': TinyMCE(attrs={'cols': 100, 'rows': 50})},
    }

class CustomUserAdmin(UserAdmin):
    # The forms to add and change user instances

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference the removed 'username' field
    fieldsets = (
        (None, {'fields': ('email','username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name','gender','dob')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        (_('Other Information'), {'fields':('phone','bio', 'photo')}),
        (_('Location'), {'fields':('postcode','latitude', 'longitude')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
         ),
    )
    form = CustomUserChangeForm
    add_form = CustomUserCreationForm
    list_display = ('email', 'first_name', 'last_name', 'is_staff')
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)
    formfield_overrides = {
        models.TextField: {'widget': TinyMCE(attrs={'cols': 100, 'rows': 50})},
    }
    inlines = [UserAchAdmin,]





admin.site.register(User, CustomUserAdmin)
admin.site.register(Achievements, AchevementAdmin)