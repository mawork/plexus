# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mydetails', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='achievements',
            options={'verbose_name': 'Achievements', 'verbose_name_plural': 'Achievements'},
        ),
        migrations.AlterModelOptions(
            name='user',
            options={'verbose_name': 'User', 'verbose_name_plural': 'Users'},
        ),
        migrations.AddField(
            model_name='user',
            name='access_token',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='user',
            name='facebooklog',
            field=models.BooleanField(default=False, verbose_name='Log in using Facebook'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='user',
            name='googlelog',
            field=models.BooleanField(default=False, verbose_name='Log in using Google'),
            preserve_default=True,
        ),
    ]
