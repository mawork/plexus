# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mydetails', '0004_token'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='gender',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Gender', choices=[(b'male', 'Male'), (b'female', 'Female')]),
            preserve_default=True,
        ),
    ]
