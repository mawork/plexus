from django.db import models
from django.utils import timezone
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _
from django.core.mail import send_mail
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.auth.models import BaseUserManager
from django_extensions.db.fields import (AutoSlugField, CreationDateTimeField, ModificationDateTimeField)
from mediastore.fields import MediaField
import binascii
import os
from django.utils.encoding import python_2_unicode_compatible
from time import strftime


class CustomUserManager(BaseUserManager):

    def _create_user(self, email, password, username,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, username=username,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, username, password=None, **extra_fields):
        return self._create_user(email, password, username, False, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
                                 **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """
    A fully featured User model with admin-compliant permissions that uses
    a full-length email field as the username.

    Email and password are required. Other fields are optional.
    """
    MALE = 'male'
    FEMALE = 'female'
    GENDER_CHOICE = (
        (MALE, _('Male'),),
        (FEMALE, _('Female'),),
    )
    username = models.CharField(_('username'), max_length=255, blank=True, null=True)
    email = models.EmailField(_('email address'), max_length=254, unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin '
                                               'site.'))
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    phone = models.CharField(_('Phone Number'), max_length=20, null=True, blank=True)
    postcode = models.CharField(_('Post Code'), max_length=10, null=True, blank=True)
    latitude = models.CharField(max_length=255, blank=True, null=True)
    longitude = models.CharField(max_length=255, blank=True, null=True)
    gender = models.CharField(_('Gender'), max_length=10, choices=GENDER_CHOICE, null=True, blank=True)
    dob = models.DateField(_('Date of Birth'), blank=True, null=True)
    bio = models.TextField(_('Bio'), blank=True, null=True)
    photo = models.ImageField(_('Display Photo'), upload_to='users/avatars/', null=True, blank=True, help_text=_('Maximum size of 500kb. Only JPG, PNG accepted'))
    achievements = models.ManyToManyField('Achievements', verbose_name='User Achievements', through='UserAchievements')

    googlelog = models.BooleanField(_('Log in using Google'), default=False)
    facebooklog = models.BooleanField(_('Log in using Facebook'), default=False)
    access_token = models.TextField(blank=True, null=True)


    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def __unicode__(self):
        return self.get_full_name()

    def get_absolute_url(self):
        return "/users/%s/" % urlquote(self.email)

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])

    def getbirth(self):
        if self.dob:
            return self.dob.strftime('%d/%m/%Y')
        else:
            return None

    def getphoto(self):
        if self.photo:
            return self.photo.url
        else:
            return '/static/img/buttons/addimage.svg'

    def getdata(self):
        data = {'name': self.get_full_name(),
                'firstname': self.first_name,
                'surname': self.last_name,
                'username': self.username,
                'email': self.email,
                'phone': self.phone,
                'postcode': self.postcode,
                'latitude': self.latitude,
                'longitude': self.longitude,
                'gender':self.gender,
                'dob':self.getbirth(),
                'bio':self.bio,
                'image':self.getphoto(),
                'archeivements':self.getarcheivements()}
        return data

    def getarcheivements(self):
        arch = self.userachivements.all()
        data = []
        for i in arch:
            item = {'title':i.achievements.title, 'slug':i.achievements.slug, 'body':i.achievements.body, 'image':i.achievements.badge.image.file.url}
            data.append(item)
        return data

class Achievements(models.Model):
    title = models.CharField(_('Title'), max_length=50)
    slug = AutoSlugField(populate_from=('title',), unique=True)
    body = models.TextField(blank=True, null=True)
    badge = MediaField(
        related_name='achievement_badge_icon',
        limit_choices_to={'content_type__model':'image'}, null=True, blank=True
    )

    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Achievements')
        verbose_name_plural = _('Achievements')

class UserAchievements(models.Model):
    user = models.ForeignKey('User', related_name='userachivements')
    achievements = models.ForeignKey('Achievements')
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()


@python_2_unicode_compatible
class Token(models.Model):
    key = models.CharField(max_length=40, primary_key=True)
    user = models.OneToOneField(User, related_name='token')
    created = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(Token, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.key
