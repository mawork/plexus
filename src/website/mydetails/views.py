
from django.http import HttpResponseRedirect
from django.views.generic import View
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import *
from .forms import UploadFileForm
import json
import dateutil.parser
import urllib2
from django.conf import settings


@csrf_exempt
def update(request):
    if request.method == 'GET':
        return HttpResponse(status=405)
    elif request.method == 'POST':
        item = json.loads(request.body)
        try:
            us = User.objects.get(token=item['token'])
            if item['firstname']:
                us.first_name = str(item['firstname'])
            if item['surname']:
                us.last_name = str(item['surname'])
            if item['phone']:
                us.phone = str(item['phone'])
            if item['postcode']:
                us.postcode = str(item['postcode'])
            if item['latitude']:
                us.latitude = str(item['latitude'])
            if item['longitude']:
                us.longitude = str(item['longitude'])
            if item['bio']:
                us.bio = str(item['bio'])
            if item['dob']:
                us.dob = dateutil.parser.parse(str(item['dob']))
            us.save()
            return HttpResponse(status=202)
        except User.DoesNotExist:
            return HttpResponse(status=403)


@csrf_exempt
def registration(request):
    if request.method == 'GET':
        return HttpResponse(status=405)
    elif request.method == 'POST':
        item = json.loads(request.body)
        if item[u'password1'] in item[u'password2']:
            us = User.objects.create_user(item[u'email'], item[u'username'], item[u'password1'])
            tk = Token(user=us)
            tk.save()
            data = {"token":str(tk)}
            return HttpResponse(json.dumps(data), content_type='application/json')
        else:
            return HttpResponse(status=409)
    return HttpResponse(status=403)

@csrf_exempt
def login(request):
    if request.method == 'GET':
        return HttpResponse(status=405)
    elif request.method == 'POST':
        item = json.loads(request.body)
        try:
            us = User.objects.get(email=item[u'email'])
            if us.check_password(item[u'password']):
                data = {'token':str(us.token), 'username':str(us.username)}
                return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                return HttpResponse(status=403)
        except User.DoesNotExist:
            return HttpResponse(status=401)


@csrf_exempt
def getdata(request):
    if request.method == 'GET':
        return HttpResponse(status=405)
    elif request.method == 'POST':
        item = json.loads(request.body)
        try:
            us = User.objects.get(token=item['token'])
            return HttpResponse(json.dumps(us.getdata()), content_type='application/json')
        except User.DoesNotExist:
            return HttpResponse(status=401)

@csrf_exempt
def image(request):
    if request.method == 'GET':
        return HttpResponse(status=405)
    elif request.method == 'POST':
        upload_file = UploadFileForm(request.POST, request.FILES)
        if upload_file.is_valid():
            print 'valid'
            token = upload_file.cleaned_data['session']
            us = User.objects.get(token=token)
            us.photo = request.FILES['file']
            us.save()
            return HttpResponse(json.dumps('works'), status=200)
        else:
            return HttpResponse(json.dumps(upload_file.errors), status=403)
        return HttpResponse(status=401)


@csrf_exempt
def facebooklogin(request):
    fbappid = '1795428174017119'
    fbappsecret = '23d941c65a544e616a1a6212e16c1ed8'
    if request.method == 'GET':
        return HttpResponse(status=405)
    elif request.method == 'POST':
        fbdata = json.loads(request.body)
        reponse = urllib2.urlopen('https://graph.facebook.com/oauth/access_token?client_id={0}&client_secret={1}&grant_type=client_credentials'.format(fbappid, fbappsecret))
        html = reponse.read()
        reult = urllib2.urlopen('https://graph.facebook.com/debug_token?input_token={0}&{1}'.format(fbdata['statuscode'], html))
        fbresult = json.loads(reult.read())
        if fbresult['data']['is_valid'] is True:
            if int(fbresult['data']['user_id']) == int(fbdata['id']):
                try:
                    us = User.objects.get(email=fbdata['email'])
                    try:
                        data = {'token':str(us.token), 'username':str(us.username)}
                    except:
                        tk = Token(user=us)
                        tk.save()
                        data = {'token':str(tk), 'username':str(us.username)}
                    return HttpResponse(json.dumps(data), content_type='application/json')
                except User.DoesNotExist:
                    uname = fbdata['first_name']+fbdata['last_name']
                    us = User.objects.create_user(fbdata['email'],uname,fbdata['id'])
                    tk = Token(user=us)
                    tk.save()
                    data = {'token':str(tk), 'username':str(uname)}
                    return HttpResponse(json.dumps(data), content_type='application/json')

        return HttpResponse(status=403)


@csrf_exempt
def googlelogin(request):
    if request.method == 'GET':
        return HttpResponse(status=405)
    elif request.method == 'POST':
        rtdata = json.loads(request.body)
        response = urllib2.urlopen('https://www.googleapis.com/oauth2/v1/tokeninfo?access_token={0}'.format(rtdata['statuscode']))
        checkdata = json.loads(response.read())
        if rtdata['id'] == checkdata['user_id']:
            lastuser = None
            for item in rtdata['emails']:
                print item['value']
                try:
                    us = User.objects.get(email=item['value'])
                    try:
                        data = {'token':str(us.token), 'username':str(us.username)}
                    except:
                        tk = Token(user=us)
                        tk.save()
                        data = {'token':str(tk), 'username':str(us.username)}
                    return HttpResponse(json.dumps(data), content_type='application/json')
                except User.DoesNotExist:
                    lastuser = item['value']

            if lastuser is not None:
                uname = rtdata['name']['givenName']+rtdata['name']['familyName']
                us = User.objects.create_user(lastuser,uname,rtdata['id'])
                tk = Token(user=us)
                tk.save()
                us.first_name = rtdata['name']['givenName']
                us.last_name = rtdata['name']['familyName']
                us.save()
                data = {'token':str(tk), 'username':str(uname)}
                return HttpResponse(json.dumps(data), content_type='application/json')

        return HttpResponse(status=403)
