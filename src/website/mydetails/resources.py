from .models import *
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS


class AcheivementResources(ModelResource):
    class Meta:
        queryset = Achievements.objects.all()
        resource_name = 'achievements'
        allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'body':ALL
        }
