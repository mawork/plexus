from django.template.response import TemplateResponse
import json
import urllib2
from django.http import HttpResponse

def index(request):
    return TemplateResponse(request, 'base.html', {})


def getdata(request, kwargs):
    url = 'https://openhub.io/api/v1/'
    urls = url + str(request.get_full_path()[6:])
    f = urllib2.urlopen(urls)
    response = f.read()
    return HttpResponse(response, content_type="application/json")

def desktop(request):
    return TemplateResponse(request, 'desktop.html', {})

def press(request):
    return TemplateResponse(request, 'press.html', {})

def about(request):
    return TemplateResponse(request, 'about.html', {})

def contact(request):
    return TemplateResponse(request, 'contact.html', {})

def appload(request):
    return TemplateResponse(request, 'plexus_redirect.html', {})