from .models import *
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

class FrontPageResources(ModelResource):
    class Meta:
        queryset = FrontPage.objects.filter(is_public=True)
        resource_name = 'frontslide'
        detail_allowed_methods = ['get',]
