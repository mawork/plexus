from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import (AutoSlugField, CreationDateTimeField, ModificationDateTimeField)

class FrontPage(models.Model):
    title = models.CharField(max_length=255)
    slug = AutoSlugField(populate_from=('title',), unique=True)
    sort_value = models.IntegerField(blank=True, null=True)
    is_public = models.BooleanField(default=False)
    body = models.TextField(blank=True, null=True)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return self.title

    class Meta:
        ordering =['sort_value',]
        verbose_name = _('Front Slides')
        verbose_name_plural = _('Front Slides')
