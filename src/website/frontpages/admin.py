__author__ = 'sam'
from django.contrib import admin
from .models import *
from tinymce.widgets import TinyMCE

class SlideAdmin(admin.ModelAdmin):
    list_display = ('title','sort_value','is_public')
    list_editable = ('sort_value','is_public')
    formfield_overrides = {
        models.TextField: {'widget': TinyMCE(attrs={'cols': 100, 'rows': 50})},
    }
    save_on_top = True

admin.site.register(FrontPage, SlideAdmin)
