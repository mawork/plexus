__author__ = 'sam'
from django.conf import settings
import os
import urllib2
import json
from django.core.exceptions import ObjectDoesNotExist
from django.utils.text import slugify


try:
    import urlparse
    from urllib import urlencode
except: # For Python 3
    import urllib.parse as urlparse
    from urllib.parse import urlencode



def get_new_code(code):
    from website.shelter.models import *

    apikey = settings.IMPORT_API_KEY
    url = 'https://api.import.io/store/connector/28c6cd45-fac0-4e2d-af65-4496730fd185/_query'
    params = {
        '_apikey':apikey,
        'input':"postcode:{0}".format(code)
    }
    url_parts = list(urlparse.urlparse(url))
    query = dict(urlparse.parse_qsl(url_parts[4]))
    query.update(params)

    url_parts[4] = urlencode(query)
    post_url = urlparse.urlunparse(url_parts)
    response = urllib2.urlopen(post_url).read()
    fl = json.loads(response)

    pc,created = PostCode.objects.get_or_create(code=code)

    for item in fl['results']:
        if u'service' in item.keys():
            if isinstance(item['service'], list):
                type = None
                if u'type' in item.keys():
                    raw_type = item['type'].lower().replace('show +','')
                    type,created = Type.objects.get_or_create(name=raw_type)

                for count in xrange(0,len(item['service'])):
                    service = Service.objects.create(
                        service = item['service'][count] or None,
                        contact = item['contact'][count] or None,
                        link = item['link'][count] or None,
                        text = item['link/_text'][count] or None,
                        location = item['location'][count] or None,
                        place = item['place'][count] or None,
                        postcode = pc,
                    )
                    if type:
                        service.type = type
                    service.save()

            else:
                type = None
                if u'type' in item.keys():
                    raw_type = item['type'].lower().replace('show +','')
                    type,created = Type.objects.get_or_create(name=raw_type)

                service = Service.objects.create(
                    service = item['service'] or None,
                    contact = item['contact'] or None,
                    link = item['link'] or None,
                    text = item['link/_text'] or None,
                    location = item['location'] or None,
                    place = item['place'] or None,
                    postcode = pc
                )
                if type:
                    service.type = type
                    service.save()


