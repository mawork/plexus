__author__ = 'sam'
from django.contrib import admin
from .models import *
from tinymce.widgets import TinyMCE

class SubItemInline(admin.StackedInline):
    model = SubItem
    extra = 1
    fieldsets = (
        ('Content',{
            'classes':('wide',),
            'fields':(
                'title',
                'sort_value',
                'is_public',
                'body'
            )
        }),
    )
    formfield_overrides = {
        models.TextField: {'widget': TinyMCE(attrs={'cols': 100, 'rows': 50})},
    }

class SectionAdmin(admin.ModelAdmin):
    list_display = ('title','sort_value','is_public')
    list_editable = ('sort_value','is_public')
    formfield_overrides = {
        models.TextField: {'widget': TinyMCE(attrs={'cols': 100, 'rows': 50})},
    }
    save_on_top = True


class ItemAdmin(admin.ModelAdmin):
    list_display = ('title','section','sort_value','is_public')
    list_editable = ('sort_value','is_public')
    inlines = [SubItemInline,]
    formfield_overrides = {
        models.TextField: {'widget': TinyMCE(attrs={'cols': 100, 'rows': 50})},
    }
    save_on_top = True
    list_filter = ('section', 'is_public', )

admin.site.register(Section, SectionAdmin)
admin.site.register(Items, ItemAdmin)

admin.site.register(PostCode)
admin.site.register(Type)
admin.site.register(Service)