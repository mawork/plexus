from .models import *
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from website.views.cors import CORSResource
from django.db.models import Q
import math
import time


class TypeReosurce(ModelResource, CORSResource):
    class Meta:
        queryset = Type.objects.all()
        resource_name = 'service_type'
        detail_allowed_methods = ['get',]
        limit = 0
        max_limit = 0
        always_return_data = True
        filtering = {
            'name': ALL,
            'id':ALL,
        }


class ServiceResources(ModelResource, CORSResource):
    type = fields.ForeignKey(TypeReosurce, 'type',full=True, full_detail=True, full_list=False, null=True)

    class Meta:
        queryset = Service.objects.all()
        resource_name = 'services'
        detail_allowed_methods = ['get',]
        limit = 0
        max_limit = 0
        always_return_data = True
        filtering = {
            'service': ALL,
            'type':ALL_WITH_RELATIONS,
            'id':ALL,
            'contact':ALL,
            'link':ALL,
            'text':ALL,
            'location':ALL,
            'place':ALL,
        }

class PostCodeResources(ModelResource, CORSResource):
    service = fields.ToManyField(ServiceResources, 'service_set',full=True, full_detail=True, full_list=True, null=True)


    def build_filters(self, filters=None):
        if filters is None:
            filters = {}
        orm_filters = super(PostCodeResources, self).build_filters(filters)

        if 'input' in filters:
            try:
                input = filters['input']
                try:
                    check = PostCode.objects.find(code=input)
                    if check:
                        # this is to put a delay until the other site dater has been checked
                        time.sleep(3)
                except Exception as ex:
                    pass
                qset = (
                    Q(code=input)
                )
            except Exception as ex:
                print ex
                pass
            orm_filters.update({'custome':qset})

        return orm_filters

    def apply_filters(self, request, applicable_filters):
        if 'custome' in applicable_filters:
            custom = applicable_filters.pop('custome')
        else:
            custom = None
        semi_filtered = super(PostCodeResources, self).apply_filters(request, applicable_filters).distinct()
        return semi_filtered.filter(custom) if custom else semi_filtered

    class Meta:
        queryset = PostCode.objects.all()
        resource_name = 'postcode'
        detail_allowed_methods = ['get',]
        limit = 0
        max_limit = 0
        always_return_data = False
        filtering = {
            'code': ALL,
            'service_set':ALL_WITH_RELATIONS,
            'id':ALL,
        }


class SubItemReousrces(ModelResource):
    url = fields.CharField(attribute='url')
    class Meta:
        queryset = SubItem.objects.all()
        resource_name ='shelter_subitem'
        allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'sort_value':ALL,
            'body':ALL,
            'slug':ALL,
            'section':ALL,
            'partof':ALL,
            'id':ALL
        }

class ItemResources(ModelResource):
    #section = fields.ToOneField(SectionResources, 'section', full=True)
    data = fields.ToManyField(SubItemReousrces, 'subitem_set', full=True)
    url = fields.CharField(attribute='url')

    class Meta:
        queryset = Items.objects.all()
        resource_name ='shelter_item'
        allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'sort_value':ALL,
            'body':ALL,
            'slug':ALL,
            'section':ALL_WITH_RELATIONS,
            'subitem_set':ALL_WITH_RELATIONS,
            'id':ALL
        }

class SectionResources(ModelResource):
    subsection = fields.ToManyField(ItemResources, 'items_set', full=True)
    url = fields.CharField(attribute='url')

    class Meta:
        queryset = Section.objects.all()
        resource_name = 'shelter_resource'
        detail_allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'sort_value':ALL,
            'body':ALL,
            'slug':ALL,
            'id':ALL
        }