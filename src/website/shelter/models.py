__author__ = 'sam'
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import (AutoSlugField, CreationDateTimeField, ModificationDateTimeField)
from datetime import timedelta, datetime
from .cruller import get_new_code
import pytz

class PostCodeManager(models.Manager):
    def find(self, *args, **kwargs):
        code = kwargs.get('code','')
        check_new = True

        try:
            pc = PostCode.objects.get(code=code)
            tehn = datetime.now() + timedelta(days= -7)
            tehn = tehn.replace(tzinfo=pytz.UTC)
            if pc.modified < tehn:
                check_new = False
            else:
                pc.service_set.all().delete()
        except PostCode.DoesNotExist:
            pass

        if check_new:
            get_new_code(code)
        return check_new

class PostCode(models.Model):
    code = models.CharField(max_length=10, verbose_name="Post Code")
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()
    objects = PostCodeManager()


    def __unicode__(self):
        return self.code

    class Meta:
        verbose_name = _('Post code')
        verbose_name_plural = _('Post code')


class Type(models.Model):
    name = models.CharField(max_length=100, verbose_name="Name")
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Type')
        verbose_name_plural = _('Type')

class Service(models.Model):
    service = models.TextField(blank=True, null=True)
    contact = models.TextField(blank=True, null=True)
    link = models.TextField(blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    location = models.TextField(blank=True, null=True)
    place = models.TextField(blank=True, null=True)
    type = models.ForeignKey(Type, blank=True, null=True)
    postcode = models.ForeignKey(PostCode, blank=True, null=True)

    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return self.service

    class Meta:
        verbose_name = _('Service')
        verbose_name_plural = _('Services')


class Section(models.Model):
    title = models.CharField(max_length=255, verbose_name="Section")
    slug = AutoSlugField(populate_from=('title',), unique=True)
    sort_value = models.IntegerField(blank=True, null=True)
    is_public = models.BooleanField(default=False)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return self.title

    @property
    def url(self):
        return "/housing/{0}".format(self.slug)

    class Meta:
        ordering =['sort_value',]
        verbose_name = _('Section')
        verbose_name_plural = _('Section')


class Items(models.Model):
    title = models.CharField(max_length=255, verbose_name="Title")
    slug = AutoSlugField(populate_from=('title',), unique=True)
    sort_value = models.IntegerField(blank=True, null=True)
    is_public = models.BooleanField(default=False)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()
    body = models.TextField(blank=True, null=True)
    section = models.ForeignKey(Section, blank=True, null=True)

    def __unicode__(self):
        return self.title

    @property
    def url(self):
        return "/housing/{0}/{1}".format(self.section.slug, self.slug)

    class Meta:
        ordering =['sort_value',]
        verbose_name = _('Item')
        verbose_name_plural = _('Items')


class SubItem(models.Model):
    title = models.CharField(max_length=255, verbose_name="Title")
    slug = AutoSlugField(populate_from=('title',), unique=True)
    sort_value = models.IntegerField(blank=True, null=True)
    is_public = models.BooleanField(default=False)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()
    body = models.TextField(blank=True, null=True)
    partof = models.ForeignKey(Items, blank=True, null=True)

    def __unicode__(self):
        return self.title

    @property
    def url(self):
        return "/housing/{0}/{1}/{2}".format(self.partof.section.slug,self.partof.slug, self.slug)

    class Meta:
        ordering =['sort_value',]
        verbose_name = _('SubItem')
        verbose_name_plural = _('SubItem')