# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Items',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'Section')),
                ('slug', django_extensions.db.fields.AutoSlugField(editable=False, populate_from=(b'title',), blank=True, unique=True)),
                ('sort_value', models.IntegerField(null=True, blank=True)),
                ('is_public', models.BooleanField(default=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
                ('body', models.TextField(null=True, blank=True)),
            ],
            options={
                'ordering': ['sort_value'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'Section')),
                ('slug', django_extensions.db.fields.AutoSlugField(editable=False, populate_from=(b'title',), blank=True, unique=True)),
                ('sort_value', models.IntegerField(null=True, blank=True)),
                ('is_public', models.BooleanField(default=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
            ],
            options={
                'ordering': ['sort_value'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SubItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'Section')),
                ('slug', django_extensions.db.fields.AutoSlugField(editable=False, populate_from=(b'title',), blank=True, unique=True)),
                ('sort_value', models.IntegerField(null=True, blank=True)),
                ('is_public', models.BooleanField(default=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
                ('body', models.TextField(null=True, blank=True)),
                ('partof', models.ForeignKey(blank=True, to='backtowork.Items', null=True)),
            ],
            options={
                'ordering': ['sort_value'],
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='items',
            name='section',
            field=models.ForeignKey(blank=True, to='backtowork.Section', null=True),
            preserve_default=True,
        ),
    ]
