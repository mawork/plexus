# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backtowork', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='items',
            options={'ordering': ['sort_value'], 'verbose_name': 'Item', 'verbose_name_plural': 'Items'},
        ),
        migrations.AlterModelOptions(
            name='section',
            options={'ordering': ['sort_value'], 'verbose_name': 'Section', 'verbose_name_plural': 'Section'},
        ),
        migrations.AlterModelOptions(
            name='subitem',
            options={'ordering': ['sort_value'], 'verbose_name': 'SubItem', 'verbose_name_plural': 'SubItem'},
        ),
        migrations.AlterField(
            model_name='items',
            name='title',
            field=models.CharField(max_length=255, verbose_name=b'Title'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='subitem',
            name='title',
            field=models.CharField(max_length=255, verbose_name=b'Title'),
            preserve_default=True,
        ),
    ]
