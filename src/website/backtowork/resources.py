from .models import *
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS


class OldSectionResources(ModelResource):
    class Meta:
        queryset = Section.objects.all()
        resource_name = 'old_section'
        detail_allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'sort_value':ALL,
            'body':ALL,
            'slug':ALL,
            'id':ALL
            }

class SubItemReousrces(ModelResource):
    url = fields.CharField(attribute='url')
    class Meta:
        queryset = SubItem.objects.all()
        resource_name ='btwsub'
        allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'sort_value':ALL,
            'body':ALL,
            'slug':ALL,
            'section':ALL,
            'partof':ALL,
            'id':ALL
        }

class ItemResources(ModelResource):
    #section = fields.ToOneField(SectionResources, 'section', full=True)
    data = fields.ToManyField(SubItemReousrces, 'subitem_set', full=True)
    url = fields.CharField(attribute='url')

    class Meta:
        queryset = Items.objects.all()
        resource_name ='btwitem'
        allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'sort_value':ALL,
            'body':ALL,
            'slug':ALL,
            'section':ALL_WITH_RELATIONS,
            'subitem_set':ALL_WITH_RELATIONS,
            'id':ALL
        }

class SectionResources(ModelResource):
    subsection = fields.ToManyField(ItemResources, 'items_set', full=True)
    url = fields.CharField(attribute='url')

    class Meta:
        queryset = Section.objects.all()
        resource_name = 'section'
        detail_allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'sort_value':ALL,
            'body':ALL,
            'slug':ALL,
            'id':ALL
        }

class OldItemResources(ModelResource):
    section = fields.ToOneField(OldSectionResources, 'section', full=True)
    data = fields.ToManyField(SubItemReousrces, 'subitem_set', full=True)
    url = fields.CharField(attribute='url')

    class Meta:
        queryset = Items.objects.all()
        resource_name ='old_btwitem'
        allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'sort_value':ALL,
            'body':ALL,
            'slug':ALL,
            'section':ALL_WITH_RELATIONS,
            'subitem_set':ALL_WITH_RELATIONS,
            'id':ALL
        }