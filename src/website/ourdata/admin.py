from django.contrib import admin
from .models import *
from tinymce.widgets import TinyMCE

class OurDataAdmin(admin.ModelAdmin):
    list_display=('title','slug', 'sort_value', 'has_form','is_public')
    list_editable=('sort_value', 'is_public')
    formfield_overrides = {
        models.TextField: {'widget': TinyMCE(attrs={'cols': 100, 'rows': 50})},
    }

class ChangeAdmin(admin.ModelAdmin):
    readonly_fields=('created','modified','content_type','object_id','content_object','url','type')

admin.site.register(OurData, OurDataAdmin)
admin.site.register(DownloadBackup)
admin.site.register(ChangeLog,ChangeAdmin )
admin.site.register(mailList)
