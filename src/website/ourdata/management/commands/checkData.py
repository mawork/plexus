from django.core.management.base import NoArgsCommand, CommandError
from datetime import datetime, timedelta
from dateutil import relativedelta
from website.ourdata.models import *
from website.ourdata.downloads import *

class Command(NoArgsCommand):
    help = "This is to check the change logs every day and if there is changes in the last week then it will create the backup and send out the email"

    def handle_noargs(self, **options):
        '''
        This checks to see if there is any changes in the last 7 days, then checks to see if the download backup is older than 7 days.
        If it is then it sends out the change email and creats a new download of the data
        :param options:
        :return:
        '''
        now = datetime.now()
        last = now - timedelta(days = 7)
        if ChangeLog.objects.filter(created__gte=last).count() > 0:
            if relativedelta.relativedelta(now, DownloadBackup.objects.getLastCreated()).days >= 7:
                createDownload()
