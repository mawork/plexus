from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import mailList
import json

@csrf_exempt
def maillist(request):
    if request.method == 'GET':
        return HttpResponse(status=405)
    elif request.method == 'POST':
        item = json.loads(request.body)
        try:
            mail = mailList.objects.get(email=item['EMAIL'])
            return HttpResponse(status=202)
        except mailList.DoesNotExist:
            if len(item['FNAME']) < 10 and len(item['LNAME']) < 10:
                nm = item['FNAME'] + ' ' + item['LNAME']
                mail = mailList.objects.create(email=item['EMAIL'], name=nm)
                mail.save()
                return HttpResponse(status=202)
            else:
                return HttpResponse(status=402)
