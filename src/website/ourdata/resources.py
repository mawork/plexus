from .models import *
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS

class OurdataResources(ModelResource):
    class Meta:
        queryset = OurData.objects.filter(is_public=True)
        resource_name = 'data'
        allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'sort_value':ALL,
            'body':ALL
        }

class DownloadResources(ModelResource):
    class Meta:
        queryset = DownloadBackup.objects.all()
        resource_name = 'download'
        allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'slug':ALL,
            'created':ALL
        }

class ChangeResource(ModelResource):
    class Meta:
        queryset = ChangeLog.objects.all()
        resource_name = 'changes'
        allowed_methods = ['get',]
        always_return_data = True


class mailListResource(ModelResource):
    class Meta:
        queryset = mailList.objects.all()
        resource_name = 'maillist'
        authorization = Authorization()
        allowed_methods = ['post',]
