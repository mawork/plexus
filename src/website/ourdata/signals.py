import signal
from django.db.models.signals import pre_save, post_save, pre_delete
from django.dispatch import receiver
from django.contrib.contenttypes.models import ContentType

from website.backtowork.models import *
from website.doit.models import *
from website.hospitals.models import *
from website.legal.models import *
from website.mydetails.models import *
from .models import *

def getsignal(sender, instance, created, **kwargs):
    if created:
        cre = 'added'
    else:
        cre = 'change'
    ct = ContentType.objects.get_for_model(instance)
    moditem = ''
    if ct is u'legal notice':
        moditem = 'legal'
    elif ct is u'section':
        moditem = 'section'
    elif ct is u'items':
        moditem = 'btwitem'
    elif ct is u'sub item':
        moditem = 'btwsub'
    elif ct is u'opportunities':
        moditem = 'opportunities'
    elif ct is u'jobs':
        moditem = 'jobs'
    elif ct is u'sector':
        moditem = 'sector'
    elif ct is u'organisation':
        moditem = 'organisation'
    elif ct is u'services':
        moditem = 'services'
    elif ct is u'our data':
        moditem = 'data'

    ct = ContentType.objects.get_for_model(instance)
    c = ChangeLog(content_type=ct, object_id=instance.pk, url="/api/v1/{0}/{1}/?format=json".format(moditem, instance.pk), type=cre)
    c.save()

def getdelete(sender, instance, **kwargs):
    ct = ContentType.objects.get_for_model(instance)
    moditem = ''
    if ct is u'legal notice':
        moditem = 'legal'
    elif ct is u'section':
        moditem = 'section'
    elif ct is u'items':
        moditem = 'btwitem'
    elif ct is u'sub item':
        moditem = 'btwsub'
    elif ct is u'opportunities':
        moditem = 'opportunities'
    elif ct is u'jobs':
        moditem = 'jobs'
    elif ct is u'sector':
        moditem = 'sector'
    elif ct is u'organisation':
        moditem = 'organisation'
    elif ct is u'services':
        moditem = 'services'
    elif ct is u'our data':
        moditem = 'data'

    ct = ContentType.objects.get_for_model(instance)
    c = ChangeLog(content_type=ct, object_id=instance.pk, url="/api/v1/{0}/{1}/?format=json".format(moditem, instance.pk), type='delete')
    c.save()


post_save.connect(getsignal, sender=Section)
post_save.connect(getsignal, sender=Items)
post_save.connect(getsignal, sender=SubItem)
post_save.connect(getsignal, sender=Opportunities)
post_save.connect(getsignal, sender=Jobs)
post_save.connect(getsignal, sender=Sector)
post_save.connect(getsignal, sender=Organisation)
post_save.connect(getsignal, sender=Services)
post_save.connect(getsignal, sender=Services)
post_save.connect(getsignal, sender=LegalNotice)
post_save.connect(getsignal, sender=Achievements)
post_save.connect(getsignal, sender=OurData)

pre_delete.connect(getdelete, sender=Section)
pre_delete.connect(getdelete, sender=Items)
pre_delete.connect(getdelete, sender=SubItem)
pre_delete.connect(getdelete, sender=Opportunities)
pre_delete.connect(getdelete, sender=Jobs)
pre_delete.connect(getdelete, sender=Sector)
pre_delete.connect(getdelete, sender=Organisation)
pre_delete.connect(getdelete, sender=Services)
pre_delete.connect(getdelete, sender=Services)
pre_delete.connect(getdelete, sender=LegalNotice)
pre_delete.connect(getdelete, sender=Achievements)
pre_delete.connect(getdelete, sender=OurData)
