# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('ourdata', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='ourdata',
            options={'ordering': ['sort_value']},
        ),
        migrations.AddField(
            model_name='ourdata',
            name='slug',
            field=django_extensions.db.fields.AutoSlugField(editable=False, populate_from=(b'title',), blank=True, unique=True),
            preserve_default=True,
        ),
    ]
