# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ourdata', '0004_ourdata_has_form'),
    ]

    operations = [
        migrations.AlterField(
            model_name='changelog',
            name='type',
            field=models.CharField(max_length=10, choices=[('added', 'Added'), ('change', 'Changed'), ('delete', 'Delete')]),
            preserve_default=True,
        ),
    ]
