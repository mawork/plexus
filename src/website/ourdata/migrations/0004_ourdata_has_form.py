# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ourdata', '0003_auto_20150907_1122'),
    ]

    operations = [
        migrations.AddField(
            model_name='ourdata',
            name='has_form',
            field=models.BooleanField(default=False, verbose_name='Has user form'),
            preserve_default=True,
        ),
    ]
