__author__ = 'sam'
from .resources import *
from .models import *
from datetime import datetime
import os
import shutil
import zipfile
import cStringIO, codecs
import json
from django.conf import settings

def filecheck(path, num):
    if os.path.exists(path + '.zip'):
        num += 1
        path += str(num)
        filecheck(path, num)
    return path

def resource_to_json(resource):
    r_list = resource.get_object_list(None)
    r_to_serialize = [resource.full_dehydrate(resource.build_bundle(obj=obj)) for obj in r_list]
    dh = '{ ' + str(resource._meta.resource_name) + ':'
    df = '}'
    data = resource.serialize(None, r_to_serialize, 'application/json')
    data = dh + data + df
    return data


def createDownload():
    file_name = "DataDownload_{0}".format(datetime.now().strftime("%Y%m"))
    patha = os.path.join(settings.MEDIA_ROOT,
                         'dataapi',
                         datetime.now().strftime("%Y"),
                         datetime.now().strftime("%m"))
    path = os.path.join(patha,file_name)
    if not os.path.exists(patha):
        os.makedirs(patha)
    loc = filecheck(path, 0) + '.zip'
    resorc = [
        OrgResources(),
        OrgServResources(),
        SectorResources(),
        OppResources(),
        JobResources(),
        LegalResources(),
        OurdataResources(),
        SectionResources(),
        ItemResources(),
        AcheivementResources(),
        DownloadResources(),
        ChangeResource(),
        mailListResource()
    ]
    json_buffer = cStringIO.StringIO()
    codecinfo = codecs.lookup("utf8")
    wrapper = codecs.StreamReaderWriter(json_buffer,codecinfo.streamreader, codecinfo.streamwriter)
    for item in resorc:
        wrapper.writelines(resource_to_json(item))

    archivezip = zipfile.ZipFile(loc, "w")
    archivezip.writestr("alldata.json", json_buffer.getvalue())
    json_buffer.close()
    archivezip.close()

    db = DownloadBackup(title=file_name, location=loc)
    db.save()
