from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import (AutoSlugField, CreationDateTimeField, ModificationDateTimeField)
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

class OurData(models.Model):
    title = models.CharField(max_length=255, verbose_name="Page title")
    sort_value = models.IntegerField(blank=True, null=True)
    is_public = models.BooleanField(default=False)
    body = models.TextField(blank=True, null=True)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()
    slug = AutoSlugField(populate_from=('title',), unique=True)
    has_form = models.BooleanField(_('Has user form'), default=False)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering =['sort_value',]
        verbose_name = _('Our Data')
        verbose_name_plural = _('Our Data')


class mailList(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    email = models.CharField(_('Email'), max_length=255)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return self.email

    class Meta:
        ordering =['created',]
        verbose_name = _('Mailing list')
        verbose_name_plural = _('Mailing list')


class DownloadBackupManager(models.Manager):
    def getLastModified(self):
        item = DownloadBackup.objects.all().order_by('-created')[0]
        return item.created

    def getLastCreated(self):
        item = DownloadBackup.objects.all().order_by('-created')[0]
        return item.created

class DownloadBackup(models.Model):
    created = CreationDateTimeField()
    title = models.CharField(max_length=255)
    location = models.TextField()
    slug = AutoSlugField(unique=True, populate_from=('title', 'created'))
    objects = DownloadBackupManager()

    def __unicode__(self):
        return "{0}".format(self.title)

class ChangeLogManager(models.Manager):
    def getLastModified(self):
        item = ChangeLog.objects.all().order_by('-modified')[0]
        return item.modified

    def getLastCreated(self):
        item = ChangeLog.objects.all().order_by('-created')[0]
        return item.created

class ChangeLog(models.Model):
    TYPE_CHOICE = (
        (u'added', u"Added",),
        (u'change', u"Changed",),
        (u'delete', u"Delete",),
    )
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')
    url = models.CharField(max_length=255)
    type = models.CharField(max_length=10, choices=TYPE_CHOICE)
    objects = ChangeLogManager()

    def __unicode__(self):
        return "{0} @ {1}".format(self.content_object, self.created)
