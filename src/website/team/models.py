from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import (AutoSlugField, CreationDateTimeField, ModificationDateTimeField)
from mediastore.fields import MediaField, MultipleMediaField


class Team(models.Model):
    name = models.CharField(_('name'), max_length=255)
    image = MediaField(
        related_name='team_image',
        limit_choices_to={'content_type__model': 'image'},
        null=True, blank=True)
    role = models.CharField(_('Role'), max_length=255, blank=True, null=True)
    bio = models.TextField(_('Bio'), blank=True, null=True)

    def __unicode__(self):
        return self.name
