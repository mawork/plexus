# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mediastore.fields.related


class Migration(migrations.Migration):

    dependencies = [
        ('mediastore', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='name')),
                ('role', models.CharField(max_length=255, null=True, verbose_name='Role', blank=True)),
                ('bio', models.TextField(null=True, verbose_name='Bio', blank=True)),
                ('image', mediastore.fields.related.MediaField(related_name='team_image', blank=True, to='mediastore.Media', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
