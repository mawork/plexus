from django.contrib import admin
from .models import *
from mediastore.admin import ModelAdmin

class TeamAdmin(ModelAdmin):
    pass

admin.site.register(Team, TeamAdmin)
