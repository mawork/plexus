# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FinanceItems',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'Title')),
                ('slug', django_extensions.db.fields.AutoSlugField(editable=False, populate_from=(b'title',), blank=True, unique=True)),
                ('sort_value', models.IntegerField(null=True, blank=True)),
                ('is_public', models.BooleanField(default=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
                ('body', models.TextField(null=True, blank=True)),
            ],
            options={
                'ordering': ['sort_value'],
                'verbose_name': 'Item',
                'verbose_name_plural': 'Items',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FinanceSection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'Section')),
                ('slug', django_extensions.db.fields.AutoSlugField(editable=False, populate_from=(b'title',), blank=True, unique=True)),
                ('sort_value', models.IntegerField(null=True, blank=True)),
                ('is_public', models.BooleanField(default=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
            ],
            options={
                'ordering': ['sort_value'],
                'verbose_name': 'Section',
                'verbose_name_plural': 'Section',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FinanceSubItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'Title')),
                ('slug', django_extensions.db.fields.AutoSlugField(editable=False, populate_from=(b'title',), blank=True, unique=True)),
                ('sort_value', models.IntegerField(null=True, blank=True)),
                ('is_public', models.BooleanField(default=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
                ('body', models.TextField(null=True, blank=True)),
                ('partof', models.ForeignKey(blank=True, to='finance.FinanceItems', null=True)),
            ],
            options={
                'ordering': ['sort_value'],
                'verbose_name': 'SubItem',
                'verbose_name_plural': 'SubItem',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='financeitems',
            name='section',
            field=models.ForeignKey(blank=True, to='finance.FinanceSection', null=True),
            preserve_default=True,
        ),
    ]
