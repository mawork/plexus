__author__ = 'sam'
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import (AutoSlugField, CreationDateTimeField, ModificationDateTimeField)

class FinanceSection(models.Model):
    title = models.CharField(max_length=255, verbose_name="Section")
    slug = AutoSlugField(populate_from=('title',), unique=True)
    sort_value = models.IntegerField(blank=True, null=True)
    is_public = models.BooleanField(default=False)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return self.title

    @property
    def url(self):
        return "/finance/{0}".format(self.slug)

    class Meta:
        ordering =['sort_value',]
        verbose_name = _('Section')
        verbose_name_plural = _('Section')


class FinanceItems(models.Model):
    title = models.CharField(max_length=255, verbose_name="Title")
    slug = AutoSlugField(populate_from=('title',), unique=True)
    sort_value = models.IntegerField(blank=True, null=True)
    is_public = models.BooleanField(default=False)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()
    body = models.TextField(blank=True, null=True)
    section = models.ForeignKey(FinanceSection, blank=True, null=True)

    def __unicode__(self):
        return self.title

    @property
    def url(self):
        return "/finance/{0}/{1}".format(self.section.slug, self.slug)

    class Meta:
        ordering =['sort_value',]
        verbose_name = _('Item')
        verbose_name_plural = _('Items')


class FinanceSubItem(models.Model):
    title = models.CharField(max_length=255, verbose_name="Title")
    slug = AutoSlugField(populate_from=('title',), unique=True)
    sort_value = models.IntegerField(blank=True, null=True)
    is_public = models.BooleanField(default=False)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()
    body = models.TextField(blank=True, null=True)
    partof = models.ForeignKey(FinanceItems, blank=True, null=True)

    def __unicode__(self):
        return self.title

    @property
    def url(self):
        return "/finance/{0}/{1}/{2}".format(self.partof.section.slug,self.partof.slug, self.slug)

    class Meta:
        ordering =['sort_value',]
        verbose_name = _('SubItem')
        verbose_name_plural = _('SubItem')
