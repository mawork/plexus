__author__ = 'sam'
from .models import *
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS


class FinanceSubItemReousrces(ModelResource):
    url = fields.CharField(attribute='url')
    class Meta:
        queryset = FinanceSubItem.objects.all()
        resource_name ='subitem'
        allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'sort_value':ALL,
            'body':ALL,
            'slug':ALL,
            'section':ALL,
            'partof':ALL,
            'id':ALL
        }

class FinanceItemsResources(ModelResource):
    #section = fields.ToOneField(SectionResources, 'section', full=True)
    data = fields.ToManyField(FinanceSubItemReousrces, 'financesubitem_set', full=True, null=True)
    url = fields.CharField(attribute='url')

    class Meta:
        queryset = FinanceItems.objects.all()
        resource_name ='item'
        allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'sort_value':ALL,
            'body':ALL,
            'slug':ALL,
            'section':ALL_WITH_RELATIONS,
            'physicalsubitem_set':ALL_WITH_RELATIONS,
            'id':ALL
        }

class FinanceSectionResources(ModelResource):
    subsection = fields.ToManyField(FinanceItemsResources, 'financeitems_set', full=True, null=True)
    url = fields.CharField(attribute='url')

    class Meta:
        queryset = FinanceSection.objects.all()
        resource_name = 'section'
        detail_allowed_methods = ['get',]
        always_return_data = True
        filtering = {
            'title':ALL,
            'sort_value':ALL,
            'body':ALL,
            'slug':ALL,
            'id':ALL
        }
