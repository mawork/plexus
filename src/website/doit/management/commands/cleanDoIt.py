__author__ = 'sam'
from django.core.management.base import BaseCommand, CommandError
from datetime import datetime
from dateutil import relativedelta
from website.doit.models import Opportunities, Jobs

class Command(BaseCommand):
    help = "This will on a nightly bases download the doit jobs and the opportunites"

    def handle(self, **options):
        self.stdout.write('Start removing oppertunities {0}'.format(datetime.now()))
        Opportunities.objects.all().delete()
        self.stdout.write('Finish removing oppertunities {0}'.format(datetime.now()))
        self.stdout.write('Start removing jobs {0}'.format(datetime.now()))
        Jobs.objects.all().delete()
        self.stdout.write('Finish removing jobs {0}'.format(datetime.now()))