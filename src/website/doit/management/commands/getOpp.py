__author__ = 'sam'
from django.core.management.base import BaseCommand, CommandError
from datetime import datetime
from dateutil import relativedelta
from website.doit.get import *

class Command(BaseCommand):
    help = "This will on a nightly bases download the doit jobs and the opportunites"

    def handle(self, **options):
        try:
            load_opp()
            self.stdout.write('load_opp finished {0}'.format(datetime.now()))
        except Exception as e:
            self.stdout.write('load_opp error {0} - {1}'.format(datetime.now(), e))


