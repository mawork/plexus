from .models import *
import os
import urllib2
import json
from django.core.exceptions import ObjectDoesNotExist
from django.utils.text import slugify

def load_opp():
    url = "https://api.do-it.org/v1/opportunities?page="
    response = urllib2.urlopen(url).read()
    fl = json.loads(response)
    tot = fl['meta']['total_pages']+1
    for item in range(1,tot):
        rest = urllib2.urlopen(url + str(item)).read()
        fly = json.loads(rest)
        for data in fly['data']['items']:
            try:
                Opportunities.objects.get(opid=data['id'])
                pass
            except ObjectDoesNotExist:
                op = Opportunities.objects.create(opid=data['id'])
                op.title = data['title']
                op.slug = slugify(data['title'])
                op.city = data['city']
                op.postcode = data['postcode']
                op.start_date = data['specific_start_date']
                op.end_date = data['specific_end_date']
                op.updated = data['updated']
                op.email_address = data['for_recruiter']['email']
                op.contact_name = data['for_recruiter']['name']
                op.name = data['owner_recruiter']['name']
                if len(data['owner_recruiter']['logo']) > 0:
                    op.logo = data['owner_recruiter']['logo'][0]['absolute_url']
                if data['postcode'] is not None:
                    try:
                        pc = urllib2.urlopen('http://api.postcodes.io/postcodes/' + str(data['postcode'])).read()
                        pcd = json.loads(pc)
                        op.latitude = pcd['result']['latitude']
                        op.longitude = pcd['result']['longitude']
                    except urllib2.HTTPError:
                        try:
                            pc = urllib2.urlopen('http://api.postcodes.io/outcodes/' + str(data['postcode'])).read()
                            pcd = json.loads(pc)
                            op.latitude = pcd['result']['latitude']
                            op.longitude = pcd['result']['longitude']
                        except urllib2.HTTPError:
                            pass
                op.save()

def load_jobs():
    url = "https://api.do-it.org/v1/jobs?page="
    response = urllib2.urlopen(url).read()
    fl = json.loads(response)
    tot = fl['meta']['total_pages']+1
    for item in range(1,tot):
        rest = urllib2.urlopen(url + str(item)).read()
        fly = json.loads(rest)
        for data in fly['data']['items']:
            try:
                Jobs.objects.get(jdid=data['id'])
            except ObjectDoesNotExist:
                jd = Jobs.objects.create(jdid=data['id'])
                jd.title = data['title']
                jd.slug = data['slug']
                jd.city = data['city']
                jd.postcode = data['postcode']
                jd.created = data['created']
                jd.apply_url = data['apply_url']
                jd.name = data['manager']['name']
                jd.email_address = data['manager']['email']
                jd.contact_name = data['manager']['contact_name']
                if len(data['manager']['logo']) > 0:
                    jd.logo = data['manager']['logo'][0]['absolute_url']
                if data['postcode'] is not None:
                    try:
                        pc = urllib2.urlopen('http://api.postcodes.io/postcodes/' + str(data['postcode'])).read()
                        pcd = json.loads(pc)
                        jd.latitude = pcd['result']['latitude']
                        jd.longitude = pcd['result']['longitude']
                    except urllib2.HTTPError:
                        try:
                            pc = urllib2.urlopen('http://api.postcodes.io/outcodes/' + str(data['postcode'])).read()
                            pcd = json.loads(pc)
                            jd.latitude = pcd['result']['latitude']
                            jd.longitude = pcd['result']['longitude']
                        except urllib2.HTTPError:
                            pass
                elif data['location_name'] is not None:
                    jd.location_name = data['location_name']
                    if data['postcode'] is None:
                        try:
                            pc = urllib2.urlopen('http://api.opencagedata.com/geocode/v1/json?q='+str(data['location_name'].replace('&','and'))+'&key=22053dc69dd9e5582358c71e9d5f685c').read()
                            pcd = json.loads(pc)
                            fill = 0
                            for item in pcd['results']:
                                if fill is 0:
                                    if -10.0 < item['geometry']['lng'] < 3.0:
                                        if 49.0 < item['geometry']['lat'] < 61.0:
                                            jd.latitude = item['geometry']['lat']
                                            jd.longitude = item['geometry']['lng']
                                            fill = 1
                        except urllib2.HTTPError:
                            jd.latitude = 51.5073219
                            jd.longitude = -0.1276474
                jd.save()
