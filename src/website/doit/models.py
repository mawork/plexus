__author__ = 'sam'
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import (AutoSlugField, CreationDateTimeField, ModificationDateTimeField)

class Opportunities(models.Model):
    logo = models.TextField(blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    slug = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    postcode = models.CharField(max_length=255, blank=True, null=True)
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    opid = models.CharField(max_length=255)
    updated = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    apply_url = models.TextField(blank=True, null=True)
    email_address = models.TextField(blank=True, null=True)
    contact_name = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Opportunities')
        verbose_name_plural = _('Opportunities')

    def get_distance(self, points):
        from math import radians, cos, sin, asin, sqrt
        lon1, lat1, lon2, lat2 = map(radians, [float(points[1]), float(points[0]), self.longitude, self.latitude])
        # haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
        c = 2 * asin(sqrt(a))
        km = 6367 * c
        return km


class Jobs(models.Model):
    jdid = models.CharField(max_length=255)
    logo = models.TextField(blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    slug = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    postcode = models.CharField(max_length=255, blank=True, null=True)
    created = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    apply_url = models.TextField(blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    email_address = models.TextField(blank=True, null=True)
    contact_name = models.TextField(blank=True, null=True)
    location_name = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Jobs')
        verbose_name_plural = _('Jobs')

    def get_distance(self, points):
        from math import radians, cos, sin, asin, sqrt
        lon1, lat1, lon2, lat2 = map(radians, [float(points[1]), float(points[0]), self.longitude, self.latitude])
        # haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
        c = 2 * asin(sqrt(a))
        km = 6367 * c
        return km
