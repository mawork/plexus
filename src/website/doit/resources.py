__author__ = 'sam'
from .models import *
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from datetime import datetime
from django.db.models import Q
import math

class OppResources(ModelResource):
    points = None

    def dehydrate(self, bundle):
        if self.points is not None:
            bundle.data['distance'] = self.getDistance(bundle)
        return bundle

    def getDistance(self, bundle):
        if self.points is not None:
            from math import radians, cos, sin, asin, sqrt
            lon1, lat1, lon2, lat2 = map(radians, [float(self.points[1]), float(self.points[0]), bundle.obj.longitude, bundle.obj.latitude])
            # haversine formula
            dlon = lon2 - lon1
            dlat = lat2 - lat1
            a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
            c = 2 * asin(sqrt(a))
            km = 6367 * c
            return km
        else:
            return '0'

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}
        orm_filters = super(OppResources, self).build_filters(filters)

        if 'points' in filters and 'distance' in filters:
            loc = filters['points'].split(',')
            self.points = loc
            try:
                lng = float(loc[1])
                lat = float(loc[0])
                R = 6371
                dist = float(filters['distance'])

                x1 = lng - math.degrees(dist/R/math.cos(math.radians(lat)))
                x2 = lng + math.degrees(dist/R/math.cos(math.radians(lat)))
                y1 = lat - math.degrees(dist/R)
                y2 = lat + math.degrees(dist/R)
                qset = (
                    Q(latitude__gte=y1) & Q(latitude__lt=y2) & Q(longitude__gt=x1) & Q(longitude__lt=x2)
                )
            except:
                pass

            orm_filters.update({'custome':qset})


        return orm_filters

    def apply_filters(self, request, applicable_filters):
        if 'custome' in applicable_filters:
            custom = applicable_filters.pop('custome')
        else:
            custom = None

        semi_filtered = super(OppResources, self).apply_filters(request, applicable_filters).distinct()

        return semi_filtered.filter(custom) if custom else semi_filtered

    class Meta:
        queryset = Opportunities.objects.filter(end_date__gte=datetime.now())
        resource_name = 'opportunities'
        allowed_methods = ['get',]
        filtering = {
            'latitude': ['exact', 'lt', 'lte', 'gte', 'gt'],
            'longitude': ['exact', 'lt', 'lte', 'gte', 'gt'],
            'postcode': ['exact', 'startswith','iexact','contains','icontains'],
            'name':ALL,
            'start_date': ALL,
            'end_data': ALL,
            'opid':ALL,
        }
        always_return_data = True


class JobResources(ModelResource):

    points = None

    def dehydrate(self, bundle):
        if self.points is not None:
            bundle.data['distance'] = self.getDistance(bundle)
        return bundle

    def getDistance(self, bundle):
        if self.points is not None:
            from math import radians, cos, sin, asin, sqrt
            lon1, lat1, lon2, lat2 = map(radians, [float(self.points[1]), float(self.points[0]), bundle.obj.longitude, bundle.obj.latitude])
            # haversine formula
            dlon = lon2 - lon1
            dlat = lat2 - lat1
            a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
            c = 2 * asin(sqrt(a))
            km = 6367 * c
            return km
        else:
            return '0'

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}
        orm_filters = super(JobResources, self).build_filters(filters)

        if 'points' in filters and 'distance' in filters:
            loc = filters['points'].split(',')
            try:
                lng = float(loc[1])
                lat = float(loc[0])
                R = 6371
                dist = float(filters['distance'])

                x1 = lng - math.degrees(dist/R/math.cos(math.radians(lat)))
                x2 = lng + math.degrees(dist/R/math.cos(math.radians(lat)))
                y1 = lat - math.degrees(dist/R)
                y2 = lat + math.degrees(dist/R)
                qset = (
                    Q(latitude__gte=y1) & Q(latitude__lt=y2) & Q(longitude__gt=x1) & Q(longitude__lt=x2)
                )
            except:
                pass

            orm_filters.update({'custome':qset})


        return orm_filters

    def apply_filters(self, request, applicable_filters):
        if 'custome' in applicable_filters:
            custom = applicable_filters.pop('custome')
        else:
            custom = None

        semi_filtered = super(JobResources, self).apply_filters(request, applicable_filters).distinct()

        return semi_filtered.filter(custom) if custom else semi_filtered

    class Meta:
        queryset = Jobs.objects.all()
        resource_name = 'jobs'
        allowed_methods = ['get',]
        filtering = {
            'latitude': ['exact', 'lt', 'lte', 'gte', 'gt'],
            'longitude': ['exact', 'lt', 'lte', 'gte', 'gt'],
            'postcode': ['exact', 'startswith','iexact','contains','icontains'],
            'name':ALL,
            'created': ALL,
            'slug': ALL,
            'jdid':ALL,

        }
        always_return_data = True
