from django.contrib import admin
from .models import *
from tinymce.widgets import TinyMCE

class listsAdmin(admin.ModelAdmin):
    list_display = ('title','postcode','latitude','longitude','city')

class JobAdmin(admin.ModelAdmin):
    list_display = ('title','postcode','latitude','longitude','city','location_name')
admin.site.register(Opportunities,listsAdmin )
admin.site.register(Jobs,JobAdmin)