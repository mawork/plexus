# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doit', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='jobs',
            options={'verbose_name': 'Jobs', 'verbose_name_plural': 'Jobs'},
        ),
        migrations.AlterModelOptions(
            name='opportunities',
            options={'verbose_name': 'Opportunities', 'verbose_name_plural': 'Opportunities'},
        ),
        migrations.AddField(
            model_name='opportunities',
            name='apply_url',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
