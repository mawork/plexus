# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doit', '0003_auto_20150918_1000'),
    ]

    operations = [
        migrations.AddField(
            model_name='jobs',
            name='location_name',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
