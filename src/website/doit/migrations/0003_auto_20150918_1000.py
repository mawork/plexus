# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doit', '0002_auto_20150914_1152'),
    ]

    operations = [
        migrations.AddField(
            model_name='jobs',
            name='contact_name',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='jobs',
            name='email_address',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='opportunities',
            name='contact_name',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='opportunities',
            name='email_address',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
