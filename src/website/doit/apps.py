from django.apps import AppConfig

class DoitConfig(AppConfig):
    name = 'website.doit'
    verbose_name = 'Working'