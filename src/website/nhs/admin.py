from django.contrib import admin
from .models import *


class HospitalData(admin.ModelAdmin):
    list_display = ('code', 'name')
    search_fields = ('code', 'name')

class SubHospitalData(admin.ModelAdmin):
    list_display = ('code', 'name', 'postcode', 'telephone','email','website')
    list_editable = ('telephone','email','website')
    search_fields = ('code', 'name')

class ValueData(admin.ModelAdmin):
    list_display = ('hospital', 'metric', 'value')
    search_fields = ('hospital', 'metric')
    list_filter = ('hospital', 'metric')


class SubValueData(admin.ModelAdmin):
    list_display = ('subhospital', 'metric', 'value')
    search_fields = ('subhospital', 'metric')
    list_filter = ('subhospital', 'metric')


class AddressAdmin(admin.ModelAdmin):
    list_display = ('hospital', 'postcode', 'telephone','email','website')
    list_editable = ('telephone','email','website')


class MetricAdmin(admin.ModelAdmin):
    list_display = ('title', 'resulttype')
    list_editable = ('resulttype',)


class SubHosServ(admin.ModelAdmin):
    list_display = ('hospital', 'provider')
    search_fields = ('hospital', 'provider')
    list_filter = ('provider',)

admin.site.register(Hospital, HospitalData)
admin.site.register(SubHospital, SubHospitalData)
admin.site.register(Metric, MetricAdmin)
admin.site.register(Values, ValueData)
admin.site.register(SubValues, SubValueData)
admin.site.register(Address, AddressAdmin)
admin.site.register(ServiceProvideer)
admin.site.register(Service)
admin.site.register(HospitalService, SubHosServ)
