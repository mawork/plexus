from .models import *

def clearaddress():
    pass


'''
# Not needed for the server and the import is no install on server (pandas)
import pandas as pd


def getmental():
    dt = pd.read_csv('/home/sam/Desktop/MentalHealthTrusts.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        la, created = Hospital.objects.get_or_create(code=dt[saved_column[1]][i], name=dt[saved_column[2]][i])
        la.save()
        ma, created = Metric.objects.get_or_create(title=dt[saved_column[3]][i])
        ma.save()
        vl,created = Values.objects.get_or_create(hospital=la, metric=ma, value=dt[saved_column[4]][i], text=dt[saved_column[5]][i])
        vl.save()


def getaddress():
    dt = pd.read_csv('/home/sam/Desktop/etr.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        try:
            la = Hospital.objects.get(code=dt[saved_column[0]][i])
            adds, created = Address.objects.get_or_create(hospital=la,
                                                         higherhealthauthority=dt[saved_column[3]][i],
                                                         address_one=dt[saved_column[4]][i],
                                                         address_two=dt[saved_column[5]][i],
                                                         address_three=dt[saved_column[6]][i],
                                                         address_four=dt[saved_column[7]][i],
                                                         address_five=dt[saved_column[8]][i],
                                                         postcode=dt[saved_column[9]][i],
                                                         telephone=dt[saved_column[17]][i],
                                                         )
            adds.save()
        except Hospital.DoesNotExist:
            pass


def getHospitalPerfo():
    dt = pd.read_csv('/home/sam/Desktop/HospitalsPerformance.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        try:
            la = Hospital.objects.get(code=dt[saved_column[1]][i][:3])
            ma, created = Metric.objects.get_or_create(title=dt[saved_column[3]][i])
            ma.save()
            vl,created = Values.objects.get_or_create(hospital=la, metric=ma, value=dt[saved_column[4]][i], text=dt[saved_column[5]][i])
            vl.save()
        except Hospital.DoesNotExist:
            pass


def getAPTPerf():
    dt = pd.read_csv('/home/sam/Desktop/APTservicesPerformance.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        try:
            la = Hospital.objects.get(code=dt[saved_column[1]][i][:3])
            ma, created = Metric.objects.get_or_create(title=dt[saved_column[3]][i])
            ma.save()
            vl,created = Values.objects.get_or_create(hospital=la, metric=ma, value=dt[saved_column[4]][i], text=dt[saved_column[5]][i])
            vl.save()
        except Hospital.DoesNotExist:
            pass


def nhsstats():
    dt = pd.read_csv('/home/sam/Desktop/nhsmentastats.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        try:
            la = Hospital.objects.get(code=dt[saved_column[0]][i][:3])
            for c in range(2,9):
                ma, created = Metric.objects.get_or_create(title=dt[saved_column[c]])
                ma.save()
                vl,created = Values.objects.get_or_create(hospital=la, metric=ma, value=dt[saved_column[c]][i])

        except Hospital.DoesNotExist:
            pass


def cleardup():
    las = Hospital.objects.all()
    metrs = Metric.objects.all()
    for la in las:
        for met in metrs:
            val = Values.objects.filter(hospital=la, metric=met)
            if len(val) > 1:
                id = val[0].id
                Values.objects.filter(hospital=la, metric=met).exclude(pk=id).delete()


def getsubhosp():
    dt = pd.read_csv('/home/sam/Desktop/Hospital.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        try:
            la = Hospital.objects.get(code=dt[saved_column[16]][i])
            sub, created = SubHospital.objects.get_or_create(hospital=la,
                                                             code=dt[saved_column[1]][i],
                                                             name=dt[saved_column[7]][i],
                                                             address_one=dt[saved_column[8]][i],
                                                             address_two=dt[saved_column[9]][i],
                                                             address_three=dt[saved_column[10]][i],
                                                             address_four=dt[saved_column[11]][i],
                                                             address_five=dt[saved_column[12]][i],
                                                             postcode=dt[saved_column[13]][i],
                                                             telephone=dt[saved_column[18]][i],
                                                             email=dt[saved_column[19]][i],
                                                             website=dt[saved_column[20]][i],
                                                             fax=dt[saved_column[21]][i],
                                                             latitude=dt[saved_column[14]][i],
                                                             longitude=dt[saved_column[15]][i])
            sub.save()
        except Hospital.DoesNotExist:
            pass



def getsubAPTPerf():
    dt = pd.read_csv('/home/sam/Desktop/APTservicesPerformance.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        try:
            la = SubHospital.objects.get(code=dt[saved_column[1]][i])
            ma, created = Metric.objects.get_or_create(title=dt[saved_column[3]][i])
            ma.save()
            vl, created = SubValues.objects.get_or_create(subhospital=la, metric=ma, value=dt[saved_column[4]][i], text=dt[saved_column[5]][i])
            vl.save()
        except SubHospital.DoesNotExist:
            pass


def getSubHospitalPerfo():
    dt = pd.read_csv('/home/sam/Desktop/HospitalsPerformance.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        try:
            la = SubHospital.objects.get(code=dt[saved_column[1]][i])
            ma, created = Metric.objects.get_or_create(title=dt[saved_column[3]][i])
            ma.save()
            vl, created = SubValues.objects.get_or_create(subhospital=la, metric=ma, value=dt[saved_column[4]][i], text=dt[saved_column[5]][i])
            vl.save()
        except SubHospital.DoesNotExist:
            pass


def getHSCA():
    dt = pd.read_csv('/home/sam/Desktop/HSCAActiveLocationsMin.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        try:
            la = SubHospital.objects.get(code=dt[saved_column[0]][i])
            for c in range(1,61):
                if dt[saved_column[c]][i] is 'Y':
                    sp, created = ServiceProvideer.objects.get_or_create(title=saved_column[c])
                    sp.save()
                    hp, created = HospitalService.objects.get_or_create(hospital=la, provider=sp)
                    hp.save()
        except SubHospital.DoesNotExist:
            pass


def getLatestRating():
    dt = pd.read_csv('/home/sam/Desktop/LatestRatings.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        for c in range(1,4):
            try:
                la = SubHospital.objects.get(code=dt[saved_column[0]][i])
                ma, created = Metric.objects.get_or_create(title=saved_column[c])
                ma.save()
                vl, created = SubValues.objects.get_or_create(subhospital=la, metric=ma, value=dt[saved_column[c]][i])
                vl.save()
            except SubHospital.DoesNotExist:
                pass


def gethospServe():
    dt = pd.read_csv('/home/sam/Desktop/hospitalservices.csv').fillna(0)
    saved_column = dt.columns
    for i in range(0, len(dt)):
        try:
            la = SubHospital.objects.get(code=dt[saved_column[0]][i])
            sl = dt[saved_column[1]][i].split(',')
            for ser in sl:
                sp, created = ServiceProvideer.objects.get_or_create(title=ser)
                sp.save()
                hp, created = HospitalService.objects.get_or_create(hospital=la, provider=sp)
                hp.save()
        except SubHospital.DoesNotExist:
            pass
'''