# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nhs', '0002_hospitalservice'),
    ]

    operations = [
        migrations.AddField(
            model_name='metric',
            name='resulttype',
            field=models.CharField(blank=True, max_length=20, null=True, choices=[(b'rate', b'Rating'), (b'percentage', b'Percentage'), (b'text', b'text')]),
            preserve_default=True,
        ),
    ]
