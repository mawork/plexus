# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nhs', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='HospitalService',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('hospital', models.ForeignKey(to='nhs.SubHospital')),
                ('provider', models.ForeignKey(to='nhs.ServiceProvideer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
