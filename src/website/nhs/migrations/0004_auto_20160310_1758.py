# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nhs', '0003_metric_resulttype'),
    ]

    operations = [
        migrations.AlterField(
            model_name='metric',
            name='resulttype',
            field=models.CharField(blank=True, max_length=20, null=True, choices=[(b'rate', b'Rating'), (b'percentage', b'Percentage'), (b'text', b'text'), (b'number', b'number'), (b'scale', b'scale'), (b'price', b'price')]),
            preserve_default=True,
        ),
    ]
