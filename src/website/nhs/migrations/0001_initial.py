# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('higherhealthauthority', models.CharField(max_length=255, null=True, blank=True)),
                ('address_one', models.CharField(max_length=255, null=True, blank=True)),
                ('address_two', models.CharField(max_length=255, null=True, blank=True)),
                ('address_three', models.CharField(max_length=255, null=True, blank=True)),
                ('address_four', models.CharField(max_length=255, null=True, blank=True)),
                ('address_five', models.CharField(max_length=255, null=True, blank=True)),
                ('postcode', models.CharField(max_length=255, null=True, blank=True)),
                ('telephone', models.CharField(max_length=255, null=True, blank=True)),
                ('latitude', models.FloatField(null=True, blank=True)),
                ('longitude', models.FloatField(null=True, blank=True)),
                ('email', models.CharField(max_length=255, null=True, blank=True)),
                ('website', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Hospital',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=5)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Metric',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('hospital', models.ForeignKey(to='nhs.Hospital')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ServiceProvideer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SubHospital',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=5)),
                ('name', models.CharField(max_length=255)),
                ('address_one', models.CharField(max_length=255, null=True, blank=True)),
                ('address_two', models.CharField(max_length=255, null=True, blank=True)),
                ('address_three', models.CharField(max_length=255, null=True, blank=True)),
                ('address_four', models.CharField(max_length=255, null=True, blank=True)),
                ('address_five', models.CharField(max_length=255, null=True, blank=True)),
                ('postcode', models.CharField(max_length=255, null=True, blank=True)),
                ('telephone', models.CharField(max_length=255, null=True, blank=True)),
                ('email', models.CharField(max_length=255, null=True, blank=True)),
                ('website', models.CharField(max_length=255, null=True, blank=True)),
                ('fax', models.CharField(max_length=255, null=True, blank=True)),
                ('latitude', models.FloatField(null=True, blank=True)),
                ('longitude', models.FloatField(null=True, blank=True)),
                ('hospital', models.ForeignKey(to='nhs.Hospital')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SubValues',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=255, null=True, blank=True)),
                ('text', models.CharField(max_length=255, null=True, blank=True)),
                ('metric', models.ForeignKey(to='nhs.Metric')),
                ('subhospital', models.ForeignKey(to='nhs.SubHospital')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Values',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=255, null=True, blank=True)),
                ('text', models.CharField(max_length=255, null=True, blank=True)),
                ('hospital', models.ForeignKey(to='nhs.Hospital')),
                ('metric', models.ForeignKey(to='nhs.Metric')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='service',
            name='provider',
            field=models.ForeignKey(to='nhs.ServiceProvideer'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='service',
            name='subhospital',
            field=models.ForeignKey(blank=True, to='nhs.SubHospital', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='address',
            name='hospital',
            field=models.ForeignKey(to='nhs.Hospital'),
            preserve_default=True,
        ),
    ]
