from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import (AutoSlugField, CreationDateTimeField, ModificationDateTimeField)


class Hospital(models.Model):
    code = models.CharField(max_length=5)
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering =['name']

    def get_address(self):
        address = []
        adds = Address.objects.filter(hospital=self)

        for i in adds:
            dt = {}
            dt['higherhealthauthority'] = i.higherhealthauthority
            dt['address_one'] = i.address_one
            dt['address_two'] = i.address_two
            dt['address_three'] = i.address_three
            dt['address_four'] = i.address_four
            dt['address_five'] = i.address_five
            dt['postcode'] = i.postcode
            dt['telephone'] = i.telephone
            dt['latitude'] = i.latitude
            dt['longitude'] = i.longitude
            dt['email'] = i.email
            dt['website'] = i.website
            return dt

    def get_metric_value(self):
        values = []
        val = Values.objects.filter(hospital=self)
        for i in val:
            dt ={}
            dt['metric'] = i.metric.title
            dt['type'] = i.metric.resulttype
            dt['value'] = i.value
            dt['text'] = i.text
            values.append(dt)
        return values

    def get_service(self):
        servie = []
        ser = Service.objects.filter(hospital=self)
        for i in ser:
            dt = {}
            dt['provider'] = i.provider.title
            dt['name'] = i.name
            servie.append(dt)
        return servie


class Metric(models.Model):
    VALUE_TYPE = (
        ('rate', 'Rating'),
        ('percentage', 'Percentage'),
        ('text', 'text'),
        ('number', 'number'),
        ('scale', 'scale'),
        ('price', 'price'),
    )
    title = models.CharField(max_length=255)
    resulttype = models.CharField(max_length=20, choices=VALUE_TYPE, blank=True, null=True)

    def __unicode__(self):
        return self.title


class Values(models.Model):
    hospital = models.ForeignKey(Hospital)
    metric = models.ForeignKey(Metric)
    value = models.CharField(max_length=255, blank=True, null=True)
    text = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return "{0}, {1}".format(self.hospital, self.metric)


class SubHospital(models.Model):
    hospital = models.ForeignKey(Hospital)
    code = models.CharField(max_length=5)
    name = models.CharField(max_length=255)
    address_one = models.CharField(max_length=255, blank=True, null=True)
    address_two = models.CharField(max_length=255, blank=True, null=True)
    address_three = models.CharField(max_length=255, blank=True, null=True)
    address_four = models.CharField(max_length=255, blank=True, null=True)
    address_five = models.CharField(max_length=255, blank=True, null=True)
    postcode = models.CharField(max_length=255, blank=True, null=True)
    telephone = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    website = models.CharField(max_length=255, blank=True, null=True)
    fax = models.CharField(max_length=255, blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)

    def get_metric_value(self):
        values = []
        val = SubValues.objects.filter(subhospital=self)
        for i in val:
            dt ={}
            dt['metric'] = i.metric.title
            dt['type'] = i.metric.resulttype
            dt['value'] = i.value
            dt['text'] = i.text
            values.append(dt)
        return values

    def get_service(self):
        servie = []
        ser = HospitalService.objects.filter(hospital=self)
        for i in ser:
            dt = {}
            dt['provider'] = i.provider.title
            dt['name'] = i.name
            servie.append(dt)
        return servie

    def __unicode__(self):
        return "{0}, {1}".format(self.hospital, self.name)


class SubValues(models.Model):
    subhospital = models.ForeignKey(SubHospital)
    metric = models.ForeignKey(Metric)
    value = models.CharField(max_length=255, blank=True, null=True)
    text = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return "{0}, {1}".format(self.subhospital, self.metric)


class Address(models.Model):
    hospital = models.ForeignKey(Hospital)
    higherhealthauthority = models.CharField(max_length=255, blank=True, null=True)
    address_one = models.CharField(max_length=255, blank=True, null=True)
    address_two = models.CharField(max_length=255, blank=True, null=True)
    address_three = models.CharField(max_length=255, blank=True, null=True)
    address_four = models.CharField(max_length=255, blank=True, null=True)
    address_five = models.CharField(max_length=255, blank=True, null=True)
    postcode = models.CharField(max_length=255, blank=True, null=True)
    telephone = models.CharField(max_length=255, blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    website = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return "{0}, {1}".format(self.hospital, self.postcode)


class ServiceProvideer(models.Model):
    title = models.CharField(max_length=255)

    def __unicode__(self):
        return self.title


class Service(models.Model):
    hospital = models.ForeignKey(Hospital)
    subhospital = models.ForeignKey(SubHospital, blank=True, null=True)
    provider = models.ForeignKey(ServiceProvideer)
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return "{0}, {1}, {2}".format(self.hospital, self.provider, self.name)


class HospitalService(models.Model):
    hospital = models.ForeignKey(SubHospital)
    provider = models.ForeignKey(ServiceProvideer)
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return "{0}, {1}, {2}".format(self.hospital, self.provider, self.name)
