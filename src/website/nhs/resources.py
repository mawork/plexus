__author__ = 'sam'
from .models import *
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from website.views.cors import CORSResource
from django.db.models import Q
import math


class TrustResources(ModelResource, CORSResource):

    def dehydrate(self, bundle):
        bundle.data['contacts'] = bundle.obj.get_address()
        bundle.data['metrics'] = bundle.obj.get_metric_value()
        bundle.data['service'] = bundle.obj.get_service()
        return bundle

    class Meta:
        queryset = Hospital.objects.all()
        resource_name = 'trusts'
        detail_allowed_methods = ['get',]
        limit = 0
        max_limit = 0
        always_return_data = True
        filtering = {
            'code': ALL,
            'name': ALL,
            'id':ALL,
        }


class MetricResources(ModelResource):

    def patch_response(self, response):
        allowed_headers = ['Content-Type', 'Authorization']
        response['Access-Control-Allow-Origin'] = '*'
        response['Access-Control-Allow-Headers'] = ','.join(allowed_headers)
        return response

    class Meta:
        queryset = Metric.objects.all()
        resource_name = 'metrics'
        detail_allowed_methods = ['get',]
        limit = 0
        max_limit = 0
        always_return_data = True
        filtering = {
            'title': ALL,
            'id':ALL,
        }


class TrustValuesResources(ModelResource):
    hospital = fields.ForeignKey(TrustResources, 'hospital',full=True, full_detail=True, full_list=False, null=True)
    metric = fields.ForeignKey(MetricResources, 'metric',full=True, full_detail=True, full_list=False, null=True)

    def patch_response(self, response):
        allowed_headers = ['Content-Type', 'Authorization']
        response['Access-Control-Allow-Origin'] = '*'
        response['Access-Control-Allow-Headers'] = ','.join(allowed_headers)
        return response

    class Meta:
        queryset = Values.objects.all()
        resources_name = 'trustsvalues'
        detail_allowed_methods = ['get',]
        limit = 0
        max_limit = 0
        always_return_data = True
        filtering = {
            'hospital': ALL_WITH_RELATIONS,
            'metric': ALL_WITH_RELATIONS,
            'value': ALL,
            'id':ALL,
        }


class HospitalResources(ModelResource):
    points = None
    trust = fields.ForeignKey(TrustResources, 'hospital', full=True, null=True)

    def dehydrate(self, bundle):
        bundle.data['metrics'] = bundle.obj.get_metric_value()
        bundle.data['service'] = bundle.obj.get_service()
        if self.points is not None:
            bundle.data['distance'] = self.getDistance(bundle)
        return bundle

    def getDistance(self, bundle):
        if self.points is not None:
            from math import radians, cos, sin, asin, sqrt
            lon1, lat1, lon2, lat2 = map(radians, [float(self.points[1]), float(self.points[0]), bundle.obj.longitude, bundle.obj.latitude])
            # haversine formula
            dlon = lon2 - lon1
            dlat = lat2 - lat1
            a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
            c = 2 * asin(sqrt(a))
            km = 6367 * c
            return km
        else:
            return '0'

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}
        orm_filters = super(HospitalResources, self).build_filters(filters)

        if 'points' in filters and 'distance' in filters:
            loc = filters['points'].split(',')
            self.points = loc
            try:
                lng = float(loc[1])
                lat = float(loc[0])
                R = 6371
                dist = float(filters['distance'])

                x1 = lng - math.degrees(dist/R/math.cos(math.radians(lat)))
                x2 = lng + math.degrees(dist/R/math.cos(math.radians(lat)))
                y1 = lat - math.degrees(dist/R)
                y2 = lat + math.degrees(dist/R)
                qset = (
                    Q(latitude__gte=y1) & Q(latitude__lt=y2) & Q(longitude__gt=x1) & Q(longitude__lt=x2)
                )
            except:
                pass

            orm_filters.update({'custome':qset})


        return orm_filters

    def apply_filters(self, request, applicable_filters):
        if 'custome' in applicable_filters:
            custom = applicable_filters.pop('custome')
        else:
            custom = None

        semi_filtered = super(HospitalResources, self).apply_filters(request, applicable_filters).distinct()

        return semi_filtered.filter(custom) if custom else semi_filtered


    class Meta:
        queryset = SubHospital.objects.all()
        resources_name = 'hospitals'
        detail_allowed_methods = ['get',]
        limit = 0
        max_limit = 0
        always_return_data = True
        filtering = {
            'trust': ALL_WITH_RELATIONS,
            'code': ALL,
            'name': ALL,
            'id':ALL,
            'address_one':ALL,
            'address_two':ALL,
            'address_three':ALL,
            'address_four':ALL,
            'address_five':ALL,
            'postcode':ALL,
            'telephone':ALL,
            'email':ALL,
            'website':ALL,
            'fax':ALL,
            'latitude': ALL,
            'longitude': ALL,
        }


class HospitalValuesResources(ModelResource):
    hospital = fields.ForeignKey(HospitalResources, 'subhospital',full=True, full_detail=True, full_list=False, null=True)
    metric = fields.ForeignKey(MetricResources, 'metric',full=True, full_detail=True, full_list=False, null=True)

    class Meta:
        queryset = SubValues.objects.all()
        resources_name = 'hospitalsvalues'
        detail_allowed_methods = ['get',]
        limit = 0
        max_limit = 0
        always_return_data = True
        filtering = {
            'hospital': ALL_WITH_RELATIONS,
            'metric': ALL_WITH_RELATIONS,
            'value': ALL,
            'id':ALL,
        }


class TrustAddressResources(ModelResource):
    trust = fields.ForeignKey(TrustResources, 'hospital',full=True, full_detail=True, full_list=False, null=True)
    points = None

    def dehydrate(self, bundle):
        if self.points is not None:
            bundle.data['distance'] = self.getDistance(bundle)
        return bundle

    def getDistance(self, bundle):
        if self.points is not None:
            from math import radians, cos, sin, asin, sqrt
            lon1, lat1, lon2, lat2 = map(radians, [float(self.points[1]), float(self.points[0]), bundle.obj.longitude, bundle.obj.latitude])
            # haversine formula
            dlon = lon2 - lon1
            dlat = lat2 - lat1
            a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
            c = 2 * asin(sqrt(a))
            km = 6367 * c
            return km
        else:
            return '0'


    def build_filters(self, filters=None):
        if filters is None:
            filters = {}
        orm_filters = super(TrustAddressResources, self).build_filters(filters)

        if 'points' in filters and 'distance' in filters:
            loc = filters['points'].split(',')
            try:
                lng = float(loc[1])
                lat = float(loc[0])
                R = 6371
                dist = float(filters['distance'])

                x1 = lng - math.degrees(dist/R/math.cos(math.radians(lat)))
                x2 = lng + math.degrees(dist/R/math.cos(math.radians(lat)))
                y1 = lat - math.degrees(dist/R)
                y2 = lat + math.degrees(dist/R)
                qset = (
                    Q(latitude__gte=y1) & Q(latitude__lt=y2) & Q(longitude__gt=x1) & Q(longitude__lt=x2)
                )
            except:
                pass

            orm_filters.update({'custome':qset})


        return orm_filters

    def apply_filters(self, request, applicable_filters):
        if 'custome' in applicable_filters:
            custom = applicable_filters.pop('custome')
        else:
            custom = None

        semi_filtered = super(TrustAddressResources, self).apply_filters(request, applicable_filters).distinct()

        return semi_filtered.filter(custom) if custom else semi_filtered



    class Meta:
        queryset = Address.objects.all()
        resources_name = 'trustsaddress'
        detail_allowed_methods = ['get',]
        limit = 0
        max_limit = 0
        always_return_data = True
        filtering = {
            'trust': ALL_WITH_RELATIONS,
            'higherhealthauthority': ALL,
            'id':ALL,
            'address_one':ALL,
            'address_two':ALL,
            'address_three':ALL,
            'address_four':ALL,
            'address_five':ALL,
            'postcode':ALL,
            'telephone':ALL,
            'email':ALL,
            'website':ALL,
            'latitude':ALL,
            'longitude':ALL,
        }


class ServiceProvResources(ModelResource):
    class Meta:
        queryset = ServiceProvideer.objects.all()
        resources_name = 'seriveprovider'
        detail_allowed_methods = ['get',]
        limit = 0
        max_limit = 0
        always_return_data = True
        filtering = {
            'id': ALL,
            'title': ALL
        }


class TrustService(ModelResource):
    trust = fields.ForeignKey(TrustResources, 'hospital',full=True, full_detail=True, full_list=False, null=True)
    provider = fields.ForeignKey(ServiceProvResources, 'provider',full=True, full_detail=True, full_list=False, null=True)

    class Meta:
        queryset = Service.objects.all()
        resources_name = 'trustservice'
        detail_allowed_methods = ['get',]
        limit = 0
        max_limit = 0
        always_return_data = True
        fields = ['hospital', 'provider', 'name']
        filtering = {
            'id': ALL,
            'name': ALL,
            'trust': ALL_WITH_RELATIONS,
            'provider': ALL_WITH_RELATIONS
        }


class HospitalService(ModelResource):
    hospital = fields.ForeignKey(HospitalResources, 'hospital',full=True, full_detail=True, full_list=False, null=True)
    provider = fields.ForeignKey(ServiceProvResources, 'provider',full=True, full_detail=True, full_list=False, null=True)

    class Meta:
        queryset = HospitalService.objects.all()
        resources_name = 'hospitalservice'
        detail_allowed_methods = ['get',]
        limit = 0
        max_limit = 0
        always_return_data = True
        filtering = {
            'id': ALL,
            'name': ALL,
            'hospital': ALL_WITH_RELATIONS,
            'provider': ALL_WITH_RELATIONS
        }
