# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mediastore.fields.related


class Migration(migrations.Migration):

    dependencies = [
        ('mediastore', '__first__'),
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='blog',
            name='arthor_image',
            field=mediastore.fields.related.MediaField(related_name='blog_arthorimage', blank=True, to='mediastore.Media', null=True),
            preserve_default=True,
        ),
    ]
