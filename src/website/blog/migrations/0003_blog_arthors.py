# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('team', '0001_initial'),
        ('blog', '0002_blog_arthor_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='blog',
            name='arthors',
            field=models.ForeignKey(blank=True, to='team.Team', null=True),
            preserve_default=True,
        ),
    ]
