# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import mediastore.fields.related
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('mediastore', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('slug', django_extensions.db.fields.AutoSlugField(editable=False, populate_from=b'title', blank=True, unique=True)),
                ('arthor', models.CharField(max_length=255, null=True, verbose_name='Arthur', blank=True)),
                ('date', models.DateField(verbose_name='Date')),
                ('headline', models.TextField(null=True, verbose_name='Headline', blank=True)),
                ('body', models.TextField(null=True, verbose_name='Body text', blank=True)),
                ('is_public', models.BooleanField(default=False, verbose_name='Public')),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True)),
                ('list_image', mediastore.fields.related.MediaField(related_name='blog_listimage', blank=True, to='mediastore.Media', null=True)),
                ('media', mediastore.fields.related.MultipleMediaField(related_name='blog_media', to='mediastore.Media', blank=True, help_text=None, sorted=False, null=True)),
            ],
            options={
                'ordering': ['-date'],
            },
            bases=(models.Model,),
        ),
    ]
