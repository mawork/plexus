# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mediastore.fields.related


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_blog_arthors'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blog',
            name='media',
            field=mediastore.fields.related.MultipleMediaField(related_name='blog_media', null=True, to='mediastore.Media', sorted=False, blank=True),
            preserve_default=True,
        ),
    ]
