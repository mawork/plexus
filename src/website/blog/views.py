from .models import *
from django.views.generic import DetailView, ListView
from django.shortcuts import render_to_response, get_object_or_404

class BlogList(ListView):
    template_name='blog/list.html'
    model=Blog
    context_object_name = "blog"
    paginate_by = 10


class BlogDetails(DetailView):
    template_name = 'blog/detail.html'
    context_object_name = 'post'

    def get_object(self):
        return get_object_or_404(Blog, slug__iexact=self.kwargs['slug'])


blog_list = BlogList.as_view()
blog_post = BlogDetails.as_view()
