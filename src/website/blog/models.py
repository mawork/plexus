from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import (AutoSlugField, CreationDateTimeField, ModificationDateTimeField)
from mediastore.fields import MediaField, MultipleMediaField
from website.team.models import Team


class Blog (models.Model):
    title = models.CharField(_('Title'), max_length=255)
    slug = AutoSlugField(populate_from='title', unique=True)
    arthors = models.ForeignKey(Team, blank=True, null=True )
    arthor = models.CharField(_('Arthur'), max_length=255, blank=True, null=True)
    arthor_image = MediaField(
        related_name='blog_arthorimage',
        limit_choices_to={'content_type__model': 'image'},
        null=True, blank=True)
    date = models.DateField(_('Date'))
    headline = models.TextField(_('Headline'), blank=True, null=True)
    body = models.TextField(_('Body text'), blank=True, null=True)
    is_public = models.BooleanField(_('Public'), default=False)
    list_image = MediaField(
        related_name='blog_listimage',
        limit_choices_to={'content_type__model': 'image'},
        null=True, blank=True)
    media = MultipleMediaField(related_name='blog_media',
                               limit_choices_to={'content_type__model__in': ('embeded', 'image', 'downloads')},
                               blank=True, null=True)
    created = CreationDateTimeField()
    modified = ModificationDateTimeField()

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['-date',]

    def __get_nearby(self,offset):
        if not hasattr(self, '_nearby_objects'):
            qs = Blog.objects.filter(is_public=True)
            qs = qs.values_list('id', flat=True)
            self._nearby_objects = list(qs)
        index = self._nearby_objects.index(self.pk)
        if index + offset < 0:
            nearby_id = self._nearby_objects[-1]
        try:
            nearby_id = self._nearby_objects[index+offset]
        except IndexError:
            nearby_id = self._nearby_objects[0]
            return Blog.objects.get(pk=nearby_id)
        return Blog.objects.get(pk=nearby_id)

    def get_next(self):
        return self.__get_nearby(1).slug

    def get_prev(self):
        return self.__get_nearby(-1).slug
