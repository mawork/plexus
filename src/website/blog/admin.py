from django.contrib import admin
from .models import *
from mediastore.admin import ModelAdmin

class BlogAdmin(ModelAdmin):
    pass


admin.site.register(Blog, BlogAdmin)

