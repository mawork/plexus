'use strict';

angular.module('rework.controllers',['rework.services','rework.directive', 'geolocation','ngSanitize'])


    .filter('commabreak', function(){
        return function(value){
            if(!value){return null};
            return value =  value.replace(/ *, */g, ',<br>');
        }
    })

    .filter('to_trusted', ['$sce', function($sce){
        return function(text) {
            return $sce.trustAsHtml(text);
        };
    }])

    .filter('htmlToPlaintext', function() {
        return function(text) {
            return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
        };
    })
    .filter('datestring', function(){
        return function(value){
            if(!value){return 'None'}
            var dt = new Date(value);
            return ""+dt.getDate()+"/"+dt.getMonth()+"/"+dt.getUTCFullYear();
        }
    })
    .filter('unique', function () {

        return function (items, filterOn) {

            if (filterOn === false) {
                return items;
            }

            if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
                var hashCheck = {}, newItems = [];

                var extractValueToCompare = function (item) {
                    if (angular.isObject(item) && angular.isString(filterOn)) {
                        return item[filterOn];
                    } else {
                        return item;
                    }
                };

                angular.forEach(items, function (item) {
                    var valueToCheck, isDuplicate = false;

                    for (var i = 0; i < newItems.length; i++) {
                        if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                            isDuplicate = true;
                            break;
                        }
                    }
                    if (!isDuplicate) {
                        newItems.push(item);
                    }

                });
                items = newItems;
            }
            return items;
        };
    })

    .controller('LoaderCtrl',['$scope',function($scope){
        $scope.class='';
    }])

    .controller('SplashCtrl',['$scope','onlineData','$state','splashserv','USER', 'ALLOW', function($scope, onlineData, $state, splashserv, USER, ALLOW){
        $scope.class = 'splash';
        $scope.list = {};

        $scope.visited = function(){
            if(splashserv.hasvisited()){
                if(USER.testskip() || USER.loggedin()){
                    $state.go('home');
                }else{
                    $state.go('startsign');
                }

            }else{
                onlineData.getSlider().then(function(res){
                    $scope.list = res.objects;
                });

                $scope.prev = function(index){
                    console.log(index);
                    splashSwiper.slidePrev(true, 300);
                };

                $scope.next = function(index){
                    if($scope.list.length == index) {
                        splashserv.setvisited();
                        if(USER.testskip()){
                            $state.go('home');
                        }else{
                            $state.go('startsign');
                        }
                    }else{
                        splashSwiper.slideNext(true, 300);
                    }
                };

                $scope.allow = function(index){
                    ALLOW.allow();
                    splashSwiper.slideNext(true, 300);
                };
            }
        };


    }])

    .controller('MainCtrl', ['$scope', 'onlineData', 'siteData','USER','flatblocks', function($scope, onlineData, siteData, USER, flatblocks){
        onlineData.getSections().then(function(res){
            $scope.backtowork = res.objects;
        });
        siteData.getLegal().then(function(res) {
            $scope.legal = res.objects;
        });
        siteData.flatblock().then(function(res){
            for(var i =0; i < res.objects.length; i++){
                flatblocks.addFlat(res.objects[i]);
            }
        });

        $scope.notifys = USER.notifycheck();

        $scope.addnot = function(){
            USER.addnotify();
            $('.notifyer').slideUp();
            $scope.notifys = true;
        };
        $scope.$on("$destroy", function(){
            USER.addnotify();
        });
    }])

    .controller('SearchCtrl', [
        '$scope',
        '$http',
        'locposts',
        '$q',
        '$state',
        '$compile',
        function($scope,
                 $http,
                 locposts,
                 $q,
                 $state,
                 $compile
        ){
            $scope.postcode = '';
            $scope.distance = '';
            $scope.location = {};

            navigator.geolocation.getCurrentPosition(function(data){
                $scope.location = {lat:data.coords.latitude, lng:data.coords.longitude};
            }, function(err){
                console.log("Geocoder failed");
                console.log(err);
            })
        }])

    .controller('SupportCtrl', [
        '$scope',
        '$controller',

        function($scope,
                 $controller){

            $controller('SearchCtrl',{$scope:$scope});
            $scope.class = 'support';
            $scope.title = 'support & services';

        }])

    .controller('SupportSearchCtrl', [
        '$scope',
        'poslocs',
        'locposts',
        '$state',
        '$controller',
        function($scope,
                 poslocs,
                 locposts,
                 $state,
                 $controller){

            $controller('SearchCtrl',{$scope:$scope});

            $scope.class = 'support';
            $scope.title = 'support & services';
            $scope.update = false;

            $scope.postchange = false;

            $scope.start = function(){
                var loc = locposts.haslocation();
                if(loc){
                    $state.go('support_results');
                }
            };

            $scope.servicebtn = function(){
                $state.go('support_service');
            };

            $scope.start();

            $scope.useloc = function(){
                /*
                 Search using gps
                 */
                locposts.addLocation($scope.location);
                poslocs.locpost($scope.location).then(function(res){
                    $scope.postcode = res;
                    locposts.addPostcode($scope.postcode);
                    $state.go('support_results');
                })
            };

            $scope.postcodechange = function(){
                $scope.postchange = true;
            };

            $scope.postcodeenter = function(){
                var item = $scope.postcode;
                locposts.addPostcode($scope.postcode);
                poslocs.getpost(item).then(function(res){
                    $scope.location = res;
                    locposts.addLocation($scope.location);
                    $state.go('support_results');
                })
            };

            $scope.distancechange = function(){
                locposts.addDist($scope.distance);
                if($scope.postchange){
                    $scope.postcodeenter();
                }else{
                    $scope.useloc();
                }
            };
            $scope.doTheBack = function() {
                $state.go('home');
            };
        }])

    .controller('SupportServices',[
        '$scope',
        'locposts',
        'mindData',
        '$rootScope',
        '$state',
        function($scope,
                 locposts,
                 mindData,
                 $rootScope,
                 $state){
            $scope.class = 'support';
            $scope.title = 'support & services';
            $scope.servicesel = locposts.getservices();
            mindData.grouping().then(function(res){
                $scope.checklist = res.objects;
            });

            $scope.addid = function(id, event){
                var index = $scope.servicesel.indexOf(id);
                if(index > -1){
                    $scope.servicesel.splice(index, 1);
                }else{
                    $scope.servicesel.push(id);
                }
            };

            $scope.updatebtn = function(){
                locposts.addservice($scope.servicesel);
                $state.go('support_results');
            }
        }
    ])

    .controller('SupportUpdateCtrl', [
        '$scope',
        'poslocs',
        'locposts',
        '$state',
        '$controller',
        function($scope,
                 poslocs,
                 locposts,
                 $state,
                 $controller){

            $controller('SearchCtrl',{$scope:$scope});

            $scope.class = 'support';
            $scope.title = 'support & services';
            $scope.update = true;
            $scope.postchange = false;
            $scope.distchange = false;

            $scope.start = function(){
                $scope.postchange = false;
                $scope.distchange = false;
            };

            $scope.start();

            $scope.useloc = function(){
                /*
                 Search using gps
                 */
                locposts.addLocation($scope.location);
                poslocs.locpost($scope.location).then(function(res){
                    $scope.postcode = res;
                    locposts.addPostcode($scope.postcode);
                })
            };

            $scope.postcodechange = function(){
                $scope.postchange = true;
            };

            $scope.distancechange = function(){
                $scope.distchange = true;

            };
            $scope.doTheBack = function() {
                $state.go('home');
            };

            $scope.servicebtn = function(){
                $state.go('support_service');
            };

            $scope.updatebtn = function(){
                locposts.clear();
                if($scope.postchange){
                    poslocs.getpost($scope.postcode).then(function(res){
                        $scope.location = res;
                        locposts.addLocation($scope.location);

                        if($scope.distchange){
                            console.log($scope.distance);
                            locposts.addDist($scope.distance);
                        }
                        $state.go('support_results');
                    })
                }else{
                    if($scope.distchange){
                        locposts.addDist($scope.distance);
                        $state.go('support_results');
                    }else{
                        $state.go('support_results');
                    }
                }
            };
        }])

    .controller('SupportResultsCtrl',[
        '$scope',
        'locposts',
        'poslocs',
        'hospitalData',
        'mindData',
        'leafletData',
        'GEO',
        '$filter',
        '$state',
        'nhsDetails',
        function(
            $scope,
            locposts,
            poslocs,
            hospitalData,
            mindData,
            leafletData,
            GEO,
            $filter,
            $state,
            nhsDetails
        ){
            $scope.class = 'support';
            $scope.title = 'support & services';
            $scope.location = locposts.getLocation();
            $scope.postcode = locposts.getPostcode();
            $scope.distance = locposts.getDist();
            $scope.multiple = true;
            $scope.markers = {};
            $scope.nhsmarkers = {};
            $scope.mindmarkers = {};
            $scope.currenttab = 'mind';
            $scope.services = locposts.getservices();
            $scope.filter = false;

            $scope.tiles = {
                url: "https://1.aerial.maps.cit.api.here.com/maptile/2.1/maptile/newest/terrain.day/{z}/{x}/{y}/256/png8?app_id=yaB0SQ2nEdvwdE7LGMOd&app_code=cfm3SIyf49nYHrTIL0L4gw",
            };

            leafletData.getMap("searchres").then(function(map) {
                map.invalidateSize();
            });

            $scope.details = null;
            $scope.detailtype = null;

            $scope.curtab = function(item){
                $scope.currenttab = item;
            };

            $scope.filterGo = function(){
                $state.go('support_service');
            };


            $scope.useloc = function(){
                /*
                 Search using gps
                 */
                navigator.geolocation.getCurrentPosition(function(data){
                    $scope.location = {lat:data.coords.latitude, lng:data.coords.longitude};
                    locposts.addLocation($scope.location);
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                        locposts.addPostcode($scope.postcode);

                        $scope.getdata();

                    })
                }, function(err){
                    console.log("Geocoder failed");
                    console.log(err);
                });

            };

            $scope.updateloc = function(){
                $state.go('support_update');
            };

            $scope.doTheBack = function() {
                $state.go('home');
            };

            $scope.updateLoc = function(){
                locposts.addDist($scope.distance);
                var item = $scope.postcode;
                locposts.addPostcode($scope.postcode);
                poslocs.getpost(item).then(function(res){
                    $scope.location = res;
                    locposts.addLocation($scope.location);
                    $scope.getdata();
                })
            };

            $scope.getdata = function(){
                $scope.center = {};
                $scope.markers = {};

                try{
                    //var hosdata = hospitalData.getall();
                    hospitalData.getLatlng($scope.location, $scope.distance).then(function(data){
                        $scope.nhs = data.objects;
                        for(var i in $scope.nhs){
                            $scope.nhsmarkers[$scope.nhs[i].name.replace('-','') ] = {
                                'lat':parseFloat($scope.nhs[i].latitude),
                                'lng':parseFloat($scope.nhs[i].longitude),
                                'message':$scope.nhs[i].name.replace('-',''),
                                'icon':{
                                    'iconUrl':'/static/img/circle-grey.svg',
                                    'iconSize':[30,30],
                                    'iconAnchor':[15,15],
                                    'popupAnchor':[0,0]
                                }
                            };
                            $scope.nhs[i].dist = parseFloat(GEO.cal($scope.location, {lat:$scope.nhs[i].latitude, lng:$scope.nhs[i].longitude},6));
                            $scope.nhs[i] = nhsDetails.checkDetails($scope.nhs[i]);
                        }
                        $scope.nhs = $filter('orderBy')($scope.nhs, 'dist');
                        $scope.markers = angular.extend($scope.nhsmarkers, $scope.mindmarkers);
                    }, function(err){
                        $scope.nhs = [];
                        console.log('error with nhs items');
                    });
                    mindData.serviceLat($scope.services, $scope.location, $scope.distance).then(function(data){
                        $scope.mind = [];
                        if($scope.services.length > 0){
                            $scope.filter=true;
                            for (var i in data.objects){
                                if (data.objects[i].service_groupings.length > 0){
                                    for(var b in data.objects[i].service_groupings){
                                        if($scope.services.indexOf( data.objects[i].service_groupings[b][0]) > -1){
                                            $scope.mind.push(data.objects[i]);
                                        }
                                    }
                                }
                            }
                        }else{
                            $scope.mind = data.objects;
                            $scope.filter=false;
                        }

                        for(var i in $scope.mind){
                            $scope.mindmarkers[$scope.mind[i].name ] = {
                                'lat':parseFloat($scope.mind[i]._latitude_postcode),
                                'lng':parseFloat($scope.mind[i]._longitude_postcode),
                                'message':$scope.mind[i].name,
                                'icon':{
                                    'iconUrl':'/static/img/circle-blue.svg',
                                    'iconSize':[30,30],
                                    'iconAnchor':[15,15],
                                    'popupAnchor':[0,0]
                                }
                            };
                            $scope.mind[i].dist = parseFloat(GEO.cal($scope.location, {lat:$scope.mind[i]._latitude_postcode, lng:$scope.mind[i]._longitude_postcode},6));
                        }
                        $scope.mind = $filter('orderBy')($scope.mind, 'dist');
                        $scope.markers = angular.extend($scope.nhsmarkers, $scope.mindmarkers);

                    }, function(err){
                        $scope.mind = [];
                        console.log('error with mind items');
                    });

                    $scope.center = {
                        lat: $scope.location.lat,
                        lng: $scope.location.lng,
                        zoom: 15
                    };
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                    });


                }catch(err){console.log(err);}
            };

            $scope.getdata();

            $scope.openItem = function(id, type){
                if(type == 'mind'){
                    $scope.details = $scope.mind[id];
                    $scope.detailtype = 'mind';
                }else if (type == 'nhs'){
                    $scope.details = $scope.nhs[id];
                    $scope.detailtype = 'nhs';
                }else{
                    $scope.details = null;
                    $scope.detailtype = null;
                }
            }
        }])

    .controller('SupportDetailCtrl',[
        '$scope',
        '$http',
        '$stateParams',
        'hospitalData',
        'mindData',
        function(
            $scope,
            $http,
            $stateParams,
            hospitalData,
            mindData
        ){
            $scope.class = 'support';
            $scope.title = 'support & services';

            $scope.id = $stateParams.supportid;
            $scope.type = $stateParams.supporttype;

            $scope.doTheBack = function() {
                window.history.back();
            };
        }
    ])

    .controller('VoluntCtrl',[
        '$scope',
        'locposts',
        'poslocs',
        '$state',
        '$sanitize',
        '$controller',
        '$location',
        '$stateParams',
        function(
            $scope,
            locposts,
            poslocs,
            $state,
            $sanitize,
            $controller,
            $location,
            $stateParams
        ){
            $scope.class = 'backtowork';
            $scope.title = 'back to work';
            $scope.multiple = false;
            $scope.update = false;
            $scope.type = $stateParams['type'];

            $scope.postcode = '';
            $scope.distance = '';
            $scope.location = {};

            $scope.postchange = false;

            $scope.start = function(){
                var loc = locposts.haslocation();
                if(loc){
                    if($scope.type == 'volunteering'){
                        $state.go('backtowork_vol_list');
                    }else if($scope.type == 'new_work'){
                        $state.go('backtowork_new');
                    }
                }
            };
            $scope.start();


            $scope.doTheBack = function() {
                $state.go('home');
            };

            $scope.useloc = function(){
                /*
                 Search using gps
                 */
                navigator.geolocation.getCurrentPosition(function(data) {
                    $scope.location = {lat:data.coords.latitude, lng:data.coords.longitude};
                    locposts.addLocation($scope.location);
                    poslocs.locpost($scope.location).then(function (res) {
                        $scope.postcode = res;
                        locposts.addPostcode($scope.postcode);
                        if ($scope.type == 'volunteering') {
                            $state.go('backtowork_vol_list');
                        } else if ($scope.type == 'new_work') {
                            $state.go('backtowork_new');
                        }else{
                            $state.go('backtowork_vol_list');
                        }

                    })
                }, function(err){
                    console.log("Geocoder failed");
                    console.log(err);
                });
            };

            $scope.postcodechange = function(){
                $scope.postchange = true;
            };

            $scope.postcodeenter = function(){
                var item = $scope.postcode;
                locposts.addPostcode($scope.postcode);
                poslocs.getpost(item).then(function(res){
                    $scope.location = res;
                    locposts.addLocation($scope.location);
                    if($scope.type == 'volunteering'){
                        $state.go('backtowork_vol_list');
                    }else if($scope.type == 'new_work'){
                        $state.go('backtowork_new');
                    }else{
                        $state.go('backtowork_vol_list');
                    }
                })
            };

            $scope.distancechange = function(){
                locposts.addDist($scope.distance);
                if($scope.postchange){
                    $scope.postcodeenter();
                }else{
                    $scope.useloc();
                }
            };

        }])

    .controller('BacktoworkUpdateCtrl',[
        '$scope',
        'locposts',
        'poslocs',
        '$state',
        '$sanitize',
        '$controller',
        '$location',
        '$stateParams',
        function(
            $scope,
            locposts,
            poslocs,
            $state,
            $sanitize,
            $controller,
            $location,
            $stateParams
        ){
            $scope.class = 'backtowork';
            $scope.title = 'back to work';
            $scope.multiple = false;
            $scope.type = $stateParams['type'];

            $scope.postcode = '';
            $scope.distance = '';
            $scope.location = {};

            $scope.update = true;
            $scope.postchange = false;
            $scope.distchange = false;

            $scope.start = function(){
                $scope.postchange = false;
                $scope.distchange = false;
            };

            $scope.start();

            $scope.doTheBack = function() {
                $state.go('home');
            };

            $scope.useloc = function(){
                /*
                 Search using gps
                 */
                navigator.geolocation.getCurrentPosition(function(data) {
                    $scope.location = {lat:data.coords.latitude, lng:data.coords.longitude};
                    locposts.addLocation($scope.location);
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                        locposts.addPostcode($scope.postcode);
                    })
                }, function(err){
                    console.log("Geocoder failed");
                    console.log(err);
                });
            };

            $scope.postcodechange = function(){
                $scope.postchange = true;
            };

            $scope.distancechange = function(){
                $scope.distchange = true;
            };


            $scope.updatebtn = function(){
                locposts.clear();
                if($scope.postchange){
                    console.log($scope.postcode);
                    poslocs.getpost($scope.postcode).then(function(res){
                        $scope.location = res;
                        console.log(res);
                        locposts.addLocation($scope.location);

                        if($scope.distchange){
                            console.log($scope.distance);
                            locposts.addDist($scope.distance);
                        }
                        if($scope.type == 'volunteering'){
                            $state.go('backtowork_vol_list');
                        }else if($scope.type == 'new_work'){
                            $state.go('backtowork_new');
                        }
                    })
                }else{
                    if($scope.distchange){
                        console.log($scope.distance);
                        locposts.addDist($scope.distance);
                        if($scope.type == 'volunteering'){
                            $state.go('backtowork_vol_list');
                        }else if($scope.type == 'new_work'){
                            $state.go('backtowork_new');
                        }
                    }else{
                        if($scope.type == 'volunteering'){
                            $state.go('backtowork_vol_list');
                        }else if($scope.type == 'new_work'){
                            $state.go('backtowork_new');
                        }
                    }
                }
            };

        }])

    .controller('VoluntTabCtrl',[
        '$scope',
        '$http',
        'locposts',
        'poslocs',
        'onlineData',
        '$state',
        function(
            $scope,
            $http,
            locposts,
            poslocs,
            onlineData,
            $state
        ){
            $scope.class = 'backtowork';
            $scope.title = 'back to work';
            $scope.showMapSwip = false;
            $scope.checklocation = function() {
                var loca = locposts.haslocation();
                if (loca) {
                    $scope.showMapSwip = true;
                } else {
                    $scope.showMapSwip = false;
                }
            };

            $scope.setlocation = function(){
                console.log('saerch');
                $state.go('backtowork_vol');
            };

            $scope.update = true;

            onlineData.getSections().then(function(res){
                $scope.glist = res.objects;
            });

            $scope.doTheBack = function() {
                window.history.back();
            };

        }])

    .controller('VoluntListCtrl',[
        '$scope',
        '$http',
        'locposts',
        'poslocs',
        'doitData',
        'leafletData',
        'GEO',
        '$state',
        function(
            $scope,
            $http,
            locposts,
            poslocs,
            doitData,
            leafletData,
            GEO,
            $state
        ){
            $scope.class = 'backtowork';
            $scope.title = 'back to work';
            $scope.location = locposts.getLocation();
            $scope.postcode = locposts.getPostcode();
            $scope.distance = locposts.getDist();
            $scope.center = {
                lat: $scope.location.lat,
                lng:  $scope.location.lng,
                zoom: 12
            };
            $scope.markers = {};
            $scope.lastname = '';
            $scope.thisname = 'volunteering';
            $scope.nextname = 'new work'.slice(0,4);
            $scope.haslocation = false;



            $scope.tiles = {
                //url: "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                //url: "https://tiles.lyrk.org/ls/{z}/{x}/{y}",
                url: "https://1.aerial.maps.cit.api.here.com/maptile/2.1/maptile/newest/terrain.day/{z}/{x}/{y}/256/png8?app_id=yaB0SQ2nEdvwdE7LGMOd&app_code=cfm3SIyf49nYHrTIL0L4gw",
            };


            $scope.showMap = true;
            $scope.$watch("showMap", function(value) {
                if (value === true) {
                    leafletData.getMap("oppmap").then(function(map) {
                        map.invalidateSize();
                    });
                }
            });

            $scope.cid = 'volctrl';
            $scope.details = null;
            $scope.jobs = "";

            $scope.doTheBack = function() {
                window.history.back();
            };

            $scope.useloc = function(){
                /*
                 Search using gps
                 */
                navigator.geolocation.getCurrentPosition(function(data){
                    $scope.location = {lat:data.coords.latitude, lng:data.coords.longitude};
                    locposts.addLocation($scope.location);
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                        locposts.addPostcode($scope.postcode);

                        $scope.getdata();

                    })
                }, function(err){
                    console.log("Geocoder failed");
                    console.log(err);
                });

            };

            $scope.updateloc = function(){
                $state.go('backtowork_update',{"type":'volunteering'})
            };

            $scope.getdata = function(){

                try{
                    var loc = locposts.getLocation();
                    var dist = locposts.getDist();
                    var datas = doitData.oppLatlng(loc, dist);
                    datas.then(function(data){

                        $scope.doit = data.objects;
                        $scope.showMap = true;
                        for(var i in $scope.doit){
                            $scope.markers[$scope.doit[i].title.replace('-','') ] = {
                                'lat':$scope.doit[i].latitude,
                                'lng':$scope.doit[i].longitude,
                                'message':$scope.doit[i].title.replace('-',''),
                                'icon':{
                                    'iconUrl':'/static/img/circle-lightpurlple.svg',
                                    'iconSize':[30,30],
                                    'iconAnchor':[15,15],
                                    'popupAnchor':[0,0]
                                }
                            };
                            $scope.doit[i].dist = parseFloat(GEO.cal($scope.location, {lat:$scope.doit[i].latitude, lng:$scope.doit[i].longitude},6));
                            if($scope.doit[i].apply_url.length < 5){
                                $scope.doit[i].apply_url = 'https://do-it.org/opportunities/'+$scope.doit[i].opid+'/';
                            };
                        }
                    });

                    angular.extend($scope, {
                        center: {
                            lat: $scope.location.lat,
                            lng: $scope.location.lng,
                            zoom: 12
                        },
                        markers : $scope.markers
                    });
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                    });
                }catch(err){console.log(err);}

            };
            $scope.getdata();
        }
    ])

    .controller('ReturnCtrl',[
        '$scope',
        '$http',
        'locposts',
        'poslocs',
        'onlineData',
        function(
            $scope,
            $http,
            locposts,
            poslocs,
            onlineData
        ){
            $scope.class = 'backtowork';
            $scope.title = 'back to work';

            $scope.lastname = '';
            $scope.thisname = '';
            $scope.nextname = '';

            $scope.sect = function(slug, index){
                $scope.slug = slug.slug;
                onlineData.getItems($scope.slug).then(function(res){
                    $scope.glist = res.objects;
                });
                if (index == 0){
                    $scope.lastname = 'new work';
                }else{
                    $scope.lastname = $scope.glist[index-1].title.slice(-5);
                }
                $scope.thisname = slug.title;
                try{
                    $scope.nextname = $scope.glist[index+1].title.slice(0,4);
                }catch(err){
                    $scope.nextname = '';
                }

            };

            $scope.details = null;

            $scope.doTheBack = function() {
                window.history.back();
            };



            $scope.openclick = function(id){
                if(id == null){
                    $scope.details = null;

                }else{
                    $scope.details = $scope.glist[id];
                }
            }
        }])

    .controller('NewWorkCtrl',[
        '$scope',
        '$http',
        'locposts',
        'poslocs',
        'doitJobs',
        'leafletData',
        '$timeout',
        'onlineData',
        'GEO',
        '$state',
        function(
            $scope,
            $http,
            locposts,
            poslocs,
            doitJobs,
            leafletData,
            $timeout,
            onlineData,
            GEO,
            $state
        ){
            $scope.class = 'backtowork';
            $scope.title = 'back to work';
            $scope.location = locposts.getLocation();
            $scope.postcode = locposts.getPostcode();
            $scope.distance = locposts.getDist();
            $scope.center = {
                lat: $scope.location.lat,
                lng:  $scope.location.lng,
                zoom: 12
            };
            $scope.lastname = 'volunteering'.slice(-5);
            $scope.thisname = 'new work';
            $scope.nextname = '';

            $scope.nextsect = null;
            $scope.markers = {};

            $scope.showMap = true;


            $scope.$watch("showMap", function(value) {
                if (value === true) {
                    leafletData.getMap("jobmap").then(function(map) {
                        map.invalidateSize();
                    });
                }
            });

            onlineData.getSections().then(function(res){
                $scope.nextname = res.objects[0].title.slice(0,4);
            });

            $scope.tiles = {
                //url: "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                //url: "https://tiles.lyrk.org/ls/{z}/{x}/{y}",
                url: "https://1.aerial.maps.cit.api.here.com/maptile/2.1/maptile/newest/terrain.day/{z}/{x}/{y}/256/png8?app_id=yaB0SQ2nEdvwdE7LGMOd&app_code=cfm3SIyf49nYHrTIL0L4gw",
            };

            $scope.cid = 'newctrl';
            $scope.details = null;
            $scope.jobs = "";

            $scope.doTheBack = function() {
                window.history.back();
            };

            $scope.useloc = function(){
                /*
                 Search using gps
                 */
                navigator.geolocation.getCurrentPosition(function(data){
                    $scope.location = {lat:data.coords.latitude, lng:data.coords.longitude};
                    locposts.addLocation($scope.location);
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                        locposts.addPostcode($scope.postcode);

                        $scope.getdata();

                    })
                }, function(err){
                    console.log("Geocoder failed");
                    console.log(err);
                });

            };

            $scope.updateloc = function(){
                console.log('click');
                $state.go('backtowork_update',{"type":'new_work'})
            };

            $scope.getdata = function(){
                try{
                    var loc = locposts.getLocation();
                    var dist = locposts.getDist();
                    var datas = doitJobs.jobLatlng(loc, dist);
                    datas.then(function(data){
                        $scope.Jobdoit = data.objects;
                        $scope.showMap = true;

                        for(var i in $scope.Jobdoit){
                            $scope.markers[$scope.Jobdoit[i].title.replace('-','') ] = {
                                    'lat':$scope.Jobdoit[i].latitude,
                                    'lng':$scope.Jobdoit[i].longitude,
                                    'message':$scope.Jobdoit[i].title.replace('-',''),
                                    'icon':{
                                        'iconUrl':'/static/img/circle-purple.svg',
                                        'iconSize':[30,30],
                                        'iconAnchor':[15,15],
                                        'popupAnchor':[0,0]
                                    }
                            };
                            if($scope.Jobdoit[i].apply_url.length < 5){
                                $scope.Jobdoit[i].apply_url = 'https://do-it.org/jobs/'+$scope.Jobdoit[i].jdid+'/';
                            };
                            $scope.Jobdoit[i].dist = parseFloat(GEO.cal($scope.location, {lat:$scope.Jobdoit[i].latitude, lng:$scope.Jobdoit[i].longitude},6));
                        }
                    });

                    angular.extend($scope, {
                        center: {
                            lat: $scope.location.lat,
                            lng: $scope.location.lng,
                            zoom: 12
                        },
                        markers : $scope.markers
                    });
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                    });
                }catch(err){console.log(err);}

            };

            $scope.getdata();
        }])


    .controller('legalCtrl', [
        '$scope',
        '$sanitize',
        'siteData',
        '$location',
        function($scope,
                 $sanitize,
                 siteData,
                 $location){
            $scope.class = 'legalright';
            $scope.title = 'legal rights';
            $scope.url = 'legal_rights';
            $scope.sect = $location.$$url.substr($location.$$url.indexOf('#') + 1);

            $scope.doTheBack = function() {
                window.history.back();
            };

            siteData.getLegal().then(function(res){
                $scope.list = res.objects;
                for(var i = 0; i < $scope.list.length; i++){
                    if(i>0){
                        $scope.list[i].lastname = $scope.list[i-1].title.slice(-5);
                    }
                    if(i<$scope.list.length){
                        try{
                            $scope.list[i].nextname = $scope.list[i+1].title.slice(0,4);
                        }catch(err){}
                    }
                }
            });

        }])

    .controller('OurDataCtrl', [
        '$scope',
        '$sanitize',
        'siteData',
        '$location',
        'userlog',
        function($scope,
                 $sanitize,
                 siteData,
                 $location,
                 userlog
                 ){

            $scope.class = 'ourdata';
            $scope.title = 'our data';
            $scope.url = 'our_data';

            $scope.name = '';
            $scope.email = '';
            $scope.mailchimp = {};
            $scope.mailchimp.username ='ma-work';
            $scope.mailchimp.dc ='us2';
            $scope.mailchimp.u ='1942b6bbcbfd553cd7714a3f4';
            $scope.mailchimp.id ='1713ea2e97';

            $scope.sect = $location.$$url.substr($location.$$url.indexOf('#') + 1);

            $scope.submit = function(){
                userlog.MailUser($scope.mailchimp);
            };


            $scope.doTheBack = function() {
                window.history.back();
            };

            siteData.getOur().then(function(res){
                $scope.list = res.objects;
                for(var i = 0; i < $scope.list.length; i++){
                    if(i>0){
                        $scope.list[i].lastname = $scope.list[i-1].title.slice(-5);
                    }
                    if(i<$scope.list.length){
                        try{
                            $scope.list[i].nextname = $scope.list[i+1].title.slice(0,4);
                        }catch(err){}
                    }
                }
            });

        }])

    .controller('SignUpCtrl',['$scope','USER','$state', 'Facebook','userlog', function($scope, USER, $state, Facebook,userlog){
        $scope.class = 'details';
        $scope.title = 'Profile details';

        if($state.current.data.start){
            $scope.start = $state.current.data.start;
        }

        if(USER.loggedin()){
            if($state.current.data.start){
                $state.go('home');
            }else{
                $state.go('mydetail');
            }
        }

        $scope.createprof = function(){
            if($state.current.data.start){
                $state.go('start_register');
            }else{
                $state.go('register');

            }
        };

        $scope.login = function(){
            if($state.current.data.start){
                $state.go('start_login');
            }else{
                $state.go('login');
            }

        };

        $scope.tomain = function(){
            USER.setskip();
            $state.go('home');
        };

        $scope.facebook = function(){
            Facebook.login(function(response){
                var statuscode;
                var data;
                Facebook.getLoginStatus(function(reponse){
                    if (response.status === 'connected') {
                        statuscode = response.authResponse.accessToken;
                        Facebook.api('/me', {fields: 'name, first_name, last_name, age_range, gender, locale, email'}, function (response) {
                            data = response;
                            data['statuscode'] = statuscode;
                            var us = userlog.fbUser(data);
                            us.then(function (res) {
                                if (res.status == 200) {
                                    if($scope.start){
                                        $state.go('home');
                                    }else{
                                        $state.go('mydetail');
                                    }

                                } else {
                                    $scope.error = true;
                                }
                            })
                        });
                    }
                });
            },{scope:'email'})
        };

        $scope.$on('event:google-plus-signin-success', function (event,authResult) {
            var actok = authResult.access_token;
            gapi.client.load('plus', 'v1', function () {
                gapi.client.plus.people.get({'userId': 'me' }).execute(function (resp) {
                    var data = resp;
                    data['statuscode'] = actok;
                    var us = userlog.GlUser(data);
                    us.then(function (res) {
                        if (res.status == 200) {
                            if($scope.start){
                                $state.go('home');
                            }else{
                                $state.go('mydetail');
                            }
                        } else {
                            $scope.error = true;
                        }
                    })
                });
            });
        });
        $scope.$on('event:google-plus-signin-failure', function (event,authResult) {

        });

        $scope.doTheBack = function() {
            window.history.back();
        };

    }])

    .controller('RegisterCtrl',['$scope','USER', '$state','userlog', function($scope, USER,$state,userlog){
        $scope.class = 'details';
        $scope.title = 'Profile details';
        $scope.error = null;
        $scope.data = null;


        $scope.submit = function(data){
            var item = userlog.registeruser(data);
            item.then(function(res){
                if(res.status == 200){
                    if($state.current.data.start){
                        $state.go('start_update');
                    }else{
                        $state.go('update');
                    }

                }else{
                    $scope.error = true;
                }
            });
        };

        $scope.doTheBack = function() {
            window.history.back();
        };
    }])

    .controller('UpdateFormCtrl',['$scope','USER', '$state','userlog','$sanitize', function($scope, USER, $state,userlog, $sanitize){
        $scope.class = 'details';
        $scope.title = 'Profile details';
        $scope.error = null;
        $scope.data = {};

        $scope.data = USER.getuserdata();

        if($scope.data.bio != null){
            if($scope.data.bio.indexOf(/<[^>]+>/gm)){
                $scope.data.bio = $scope.data.bio.replace(/<[^>]+>/gm, '');
            }
        }

        $scope.submit = function(data){
            var bk = userlog.update(data);
            try{
                bk.then(function(res){
                    console.log(res);
                    if(res.statusText == 'ACCEPTED'){
                        if($state.current.data.start){
                            $state.go('home');
                        }else{
                            $state.go('mydetail');
                        }
                    }else{
                        $scope.error = true;
                    }
                });
            }catch(err){
                $scope.error = true;
            }
        };

        $scope.doTheBack = function() {
            window.history.back();
        };
    }])

    .controller('LogInCtrl',['$scope','USER', '$state','userlog', function($scope, USER, $state,userlog){
        $scope.class = 'details';
        $scope.title = 'Login details';
        $scope.error = null;
        $scope.email = '';
        $scope.password = '';

        $scope.submit = function(email, password){
            if(email !== null && password !== null){
                var data = JSON.stringify({"email":email, "password":password});
                var us = userlog.login(data);
                us.then(function(res){
                    if(res.status == 200){
                        if($state.current.data.start){
                            $state.go('home');
                        }else{
                            $state.go('mydetail');
                        }
                    }else{
                        $scope.error = true;
                    }
                })
            }
        };

        $scope.doTheBack = function() {
            window.history.back();
        };
    }])

    .controller('ImgUpCtrl',['$scope','$state', 'USER', function($scope, $state, USER){
        $scope.class = 'details';
        $scope.title = 'Image upload';
        $scope.error = null;
        $scope.session = USER.getsession();
        $scope.completed = function(){
            $state.go('mydetail');
        }
    }])

    .controller('DetailCtrl', [
        '$scope',
        'USER',
        '$state',
        'userlog',
        'siteData',
        'flatblocks',
        function($scope,USER, $state,userlog, siteData,flatblocks){
            $scope.class = 'details';
            $scope.title = 'my details';

            if(USER.loggedin() == false){
                $state.go('signup');
            }else{
                userlog.getdata().then(function(res){
                    $scope.data = res;
                    USER.setuserdata(res);

                });

                $scope.update = function(){
                    $state.go('update');
                };

                $scope.logout = function(){
                    var log = USER.logout();
                    if(log){
                        console.log('logged out');
                    }
                    $state.go('home');
                };

                $scope.gotoImage = function(){
                    $state.go('imageupload');
                };

                $scope.getflat = function(){
                    if(flatblocks.getFlat('achievement')){
                        return flatblocks.getFlat('achievement');
                    }else{
                        return '';
                    }

                }
            }
            $scope.doTheBack = function() {
                window.history.back();
            };
        }])

    .controller('AchiveCtrl', [
        '$scope',
        function($scope){
            $scope.class = 'details';
            $scope.title = 'achivements';
            $scope.doTheBack = function() {
                window.history.back();
            };
        }])

    .controller('AroundSearchCtrl', [
        '$scope',
        'USER',
        '$state',
        'locposts',
        'userlog',
        'siteData',
        'flatblocks',
        'localAuthData',
        'poslocs',
        function($scope,USER, $state, locposts, userlog, siteData,flatblocks,localAuthData, poslocs) {
            $scope.class = 'around';
            $scope.title = 'around you';
            $scope.postchange = false;
            $scope.distchange = false;
            $scope.postcode = '';
            $scope.distance = '';
            $scope.location = {};
            $scope.update = false;
            $scope.error = null;



            navigator.geolocation.getCurrentPosition(function(data){
                $scope.location = {lat:data.coords.latitude, lng:data.coords.longitude};
            }, function(err){
                console.log("Geocoder failed");
                console.log(err);
            });

            localAuthData.getData().then(function(res){
                $scope.loauth = res.objects;
            });

            if($state.current.data.error){
                $scope.error = true;
            }else{
                $scope.error = false;
            }

            if($state.current.data.update){
                $scope.update = true;

                $scope.useloc = function(){
                    locposts.addLocation($scope.location);
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                        locposts.addPostcode($scope.postcode);
                    })
                };

                $scope.postcodechange = function(){
                    $scope.postchange = true;
                };

                $scope.distancechange = function(){
                    $scope.distchange = true;

                };

                $scope.updatebtn = function(){
                    $scope.update = false;
                    locposts.addRegion('');
                    if($scope.postchange){
                        poslocs.getpost($scope.postcode).then(function(res){
                            $scope.location = res;
                            locposts.addLocation($scope.location);

                            if($scope.distchange){
                                locposts.addRegion($scope.distance);
                            }
                            $state.go('aroundyou_details');
                        })
                    }else{
                        if($scope.distchange){
                            locposts.addRegion($scope.distance);
                            $state.go('aroundyou_details');
                        }else{
                            $state.go('aroundyou_details');
                        }
                    }
                };

            }else{
                locposts.getRegion().then(function(res){
                    $state.go('aroundyou_details');
                }, function(resone){
                    console.log(resone);
                });

                $scope.update = false;
                var loc = locposts.haslocation();
                if(loc){
                    $state.go('aroundyou_update');
                }

                $scope.useloc = function(){
                    /*
                     Search using gps
                     */
                    locposts.addLocation($scope.location);
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                        locposts.addPostcode($scope.postcode);
                        $state.go('aroundyou_details');
                    })
                };

                $scope.postcodechange = function(){
                    $scope.postchange = true;
                };

                $scope.postcodeenter = function(){
                    var item = $scope.postcode;
                    locposts.addPostcode($scope.postcode);
                    poslocs.getpost(item).then(function(res){
                        $scope.location = res;
                        locposts.addLocation($scope.location);
                        $state.go('aroundyou_details');
                    })
                };
                $scope.distancechange = function(){
                    locposts.addRegion($scope.distance);
                    $state.go('aroundyou_details');
                };
            }

            $scope.doTheBack = function() {
                $state.go('home');
            };
        }])

    .controller('AroundDetailCtrl', [
        '$scope',
        'USER',
        '$state',
        'userlog',
        'siteData',
        'flatblocks',
        'locposts',
        'localAuthData',
        '$filter',
        '$location',
        function($scope,USER, $state,userlog, siteData,flatblocks, locposts, localAuthData, $filter, $location) {
            $scope.class = 'around';
            $scope.title = 'around you';

            $scope.postcode = locposts.getPostcode();
            $scope.data = null;


            locposts.getRegion().then(function(res){
                $scope.region = res;
                localAuthData.getLocal($scope.region).then(function(res){
                    if(res[0]){
                        $scope.data = res[0];
                        $scope.unemp = $filter('orderBy')($scope.data.unemployment, '-year');
                        $scope.unfirst = $scope.unemp[0];
                        $scope.unsecond = $scope.unemp[1];
                        $scope.unfirstnum = ($scope.unemp[0].percentage * 250) / 100;
                        $scope.unsecondnum = ($scope.unemp[2].percentage * 250) / 100;
                        $scope.inemp = $filter('orderBy')($scope.data.inemployment, '-year');
                        $scope.infirst = $scope.inemp[0];
                        $scope.insecond = $scope.inemp[1];
                        $scope.infirstnum = ($scope.inemp[0].percentage * 250) / 100;
                        $scope.insecondnum = ($scope.inemp[2].percentage * 250) / 100;
                        $scope.jobs = $filter('orderBy')($scope.data.jobseeking, '-date');
                        for(var i = 0; i < $scope.jobs.length; i++){
                            $scope.jobs[i].size = ($scope.jobs[i].percentage * 50) / 100;
                        }
                        $scope.pastjobs = $scope.jobs.slice(1,-1);
                        $scope.jobfirst = $scope.jobs[0];
                        $scope.jobfirstcir = ($scope.jobs[0].percentage * 250) / 100;
                        $scope.disable = $filter('orderBy')($scope.data.disabilityliving.total, '-date');
                        $scope.disablefirst = $scope.disable[0];
                        $scope.disablefirstcir = ($scope.disablefirst.percetange * 250) / 100;
                        $scope.dislearning = $filter('orderBy')($scope.data.disabilityliving['learning difficulties'], '-date');
                        $scope.disbehav = $filter('orderBy')($scope.data.disabilityliving['behavioural disorder'], '-date');
                        $scope.dispersonal = $filter('orderBy')($scope.data.disabilityliving['personality disorder'], '-date');
                        $scope.dispsych = $filter('orderBy')($scope.data.disabilityliving['psychoneurosis'], '-date');
                        $scope.dissever = $filter('orderBy')($scope.data.disabilityliving['severely mentally impaired'], '-date');
                    }else{
                        $state.go('aroundyou_error');
                    }
                });
            }, function(res){
                console.log(res);
                $state.go('aroundyou');
            });

            localAuthData.getRegion().then(function(res){
                $scope.regiondata = res;
            });

            $scope.updateloc = function(){
                $state.go('aroundyou_update');
            };

            $scope.gotoserver = function(){
                $state.go('support_results');
            };

            $scope.gotocv = function(){
                $state.go('backtowork_tabs');
            };

        }]);
