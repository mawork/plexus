'use strict';

angular.module('rework', [
                        'ngSanitize',
                        'ngRoute',
                        'ngCookies',
                        'rework.controllers',
                        'ui.router',
                        'ngAnimate',
                        'leaflet-directive',
                        'ngUpload',
                        'facebook',
                        'directive.g+signin',
                        'mailchimp',
                        'angulartics',
                        'angulartics.google.analytics'
                    ])

    .config([
        '$httpProvider',
        '$interpolateProvider',
        function($httpProvider, $interpolateProvider) {
            $interpolateProvider.startSymbol('{$');
            $interpolateProvider.endSymbol('$}');
            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
            $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        }])

    .config(function ($analyticsProvider) {
        $analyticsProvider.firstPageview(true); /* Records pages that don't use $state or $route */
        $analyticsProvider.withAutoBase(true);  /* Records full path */
        $analyticsProvider.withBase(true);
    })

    .run([
        '$http',
        '$cookies',
        '$route',
        '$rootScope',
        '$state',
        function($http, $cookies,$route,$rootScope,$state) {
            $http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;
            $rootScope.$on("$stateChangeError", console.log.bind(console));
            $rootScope.tomenu = function(){
                $state.go('home');
            };
            $rootScope.goBack = function() {
                window.history.back();
            };
            $rootScope.previousState;
            $rootScope.previousStateParams;
            $rootScope.currentState;

            $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
                $rootScope.previousState = from.name;
                $rootScope.previousStateParams = fromParams;
                $rootScope.currentState = to.name;
            });
        }])
    .config(function($stateProvider, $urlRouterProvider,$locationProvider){

        $stateProvider
            .state("loader", {
                url:"/",
                templateUrl:'/static/templates/loader.html',
                controller:'LoaderCtrl'
            })
            .state("splash",{
                url:"/splash/",
                templateUrl:'/static/templates/splash.html',
                controller:'SplashCtrl'
            })
            .state('home',{
                url: "/menu/",
                templateUrl:'/static/templates/home.html',
                controller:'MainCtrl'
            })
            .state('support',{
                url:'/support',
                templateUrl:'/static/templates/search/home.html',
                controller:'SupportSearchCtrl'
            })
            .state('support_update',{
                url:'/support/update',
                templateUrl:'/static/templates/search/home.html',
                controller:'SupportUpdateCtrl'
            })
            .state('support_results',{
                url:'/support/results',
                templateUrl:'/static/templates/search/results.html',
                controller:'SupportResultsCtrl'
            })
            .state('support_service',{
                url:'/support/services',
                templateUrl:'/static/templates/search/servicesellect.html',
                controller:'SupportServices'
            })
            .state('support_detail',{
                url:'/support/:supporttype/:supportid',
                templateUrl:'/static/templates/search/details.html',
                controller:'SupportDetailCtrl'
            })
            .state('backtowork_vol',{
                url:'/backtowork/search/:type',
                templateUrl:'/static/templates/backtowork/home.html',
                controller:'VoluntCtrl'
            })
            .state('backtowork_update',{
                url:'/backtowork/update/:type',
                templateUrl:'/static/templates/backtowork/home.html',
                controller:'BacktoworkUpdateCtrl'
            })
            .state('backtowork_tabs',{
                url:'/backtowork',
                templateUrl:'/static/templates/backtowork/swipertabs.html',
                controller:'VoluntTabCtrl'
            })
            .state('backtowork_vol_list',{
                url:'/backtowork#volunteering',
                templateUrl:'/static/templates/backtowork/swipertabs.html',
                controller:'VoluntListCtrl'
            })
            .state('backtowork_return',{
                url:'/backtowork#return_to_work',
                templateUrl:'/static/templates/backtowork/swipertabs.html',
                controller:'ReturnCtrl'
            })
            .state('backtowork_new',{
                url:'/backtowork#new_work',
                templateUrl:'/static/templates/backtowork/swipertabs.html',
                controller:'NewWorkCtrl'
            })
            .state('backtowork_guidance',{
                url:'/backtowork#guidance',
                templateUrl:'/static/templates/backtowork/swipertabs.html',
                controller:'GuidanceListCtrl'
            })
            .state('backtowork_swiper',{
                url:'/backtowork/sweep',
                templateUrl:'/static/templates/backtowork/swipertabs.html',
                controller:'VoluntTabCtrl'
            })
            .state('legal',{
                url:'/legal_right',
                templateUrl:'/static/templates/tabsections/sweeptab.html',
                controller: 'legalCtrl'
            })
            .state('signup',{
                url:'/mydetails/signup',
                templateUrl:'/static/templates/details/signup.html',
                controller:'SignUpCtrl',
                data:{
                    start:false
                }
            })
            .state('register',{
                url:'/mydetails/register',
                templateUrl:'/static/templates/details/register.html',
                controller:'RegisterCtrl',
                data:{
                    start:false
                }
            })
            .state('update',{
                url:'/mydetails/update',
                templateUrl:'/static/templates/details/update.html',
                controller:'UpdateFormCtrl',
                data:{
                    start:false
                }
            })
            .state('login',{
                url:'/mydetails/login',
                templateUrl:'/static/templates/details/login.html',
                controller:'LogInCtrl',
                data:{
                    start:false
                }
            })
            .state('start_register',{
                url:'/mydetails/register',
                templateUrl:'/static/templates/details/register.html',
                controller:'RegisterCtrl',
                data:{
                    start:true
                }
            })
            .state('start_update',{
                url:'/mydetails/update',
                templateUrl:'/static/templates/details/update.html',
                controller:'UpdateFormCtrl',
                data:{
                    start:true
                }
            })
            .state('start_login',{
                url:'/mydetails/login',
                templateUrl:'/static/templates/details/login.html',
                controller:'LogInCtrl',
                data:{
                    start:true
                }
            })
            .state('mydetail',{
                url:'/mydetails',
                templateUrl:'/static/templates/details/swipedetails.html',
                controller:'DetailCtrl'
            })
            .state('imageupload', {
                url:'/mydetails/image',
                templateUrl:'/static/templates/details/imageupload.html',
                controller:'ImgUpCtrl'
            })
            .state('achivements',{
                url:'/achivements',
                templateUrl:'/static/templates/details/achivements.html',
                controller:'AchiveCtrl'
            })
            .state('ourdata',{
                url:'/our_data',
                templateUrl:'/static/templates/tabsections/sweeptab.html',
                controller:'OurDataCtrl'
            })
            .state('startsign',{
                url:'/start',
                templateUrl:'/static/templates/details/signup.html',
                controller:'SignUpCtrl',
                data:{
                    start:true
                }
            })
            .state('aroundyou',{
                url:'/aroundyou',
                templateUrl:'/static/templates/aroundyou/home.html',
                controller:'AroundSearchCtrl',
                data:{
                    update:false,
                    error:false
                }
            })
            .state('aroundyou_update',{
                url:'/aroundyou',
                templateUrl:'/static/templates/aroundyou/home.html',
                controller:'AroundSearchCtrl',
                data:{
                    update:true,
                    error:false
                }
            })
            .state('aroundyou_error',{
                url:'/aroundyou/error',
                templateUrl:'/static/templates/aroundyou/error.html',
                controller:'AroundSearchCtrl',
                data:{
                    update:true,
                    error:true
                }
            })
            .state('aroundyou_details',{
                url:'/aroundyou/results',
                templateUrl:'/static/templates/aroundyou/sweeppage.html',
                controller:'AroundDetailCtrl'
            });
    })

    .config(function(FacebookProvider){
        FacebookProvider.init('1795428174017119');
    });
