'use strict';

angular.module('rework.services',['rework.controllers', 'geolocation'])

    .service('poslocs', ['$http', '$q', function($http, $q){
        function getpost(query){
            var ret = $q.defer();
            if(query.length > 4){
                $http.get('https://api.postcodes.io/postcodes/'+query).then(function(response){
                    ret.resolve({'lat':response.data.result.latitude, 'lng':response.data.result.longitude});
                });
            }else{
                $http.get('https://api.postcodes.io/outcodes/'+query).then(function(response){
                    ret.resolve({'lat':response.data.result.latitude, 'lng':response.data.result.longitude});
                });
            }

            return ret.promise;
        }

        function locpost(obj) {
            var ret = $q.defer();
            if(obj.lng){
                $http.get('https://api.postcodes.io/outcodes?lon=' + obj.lng + '&lat=' + obj.lat).then(function (response) {
                    ret.resolve(response.data.result[0].outcode);
                });
            }else{
                console.log('error with lat lng');
                ret.reject('no lat or lng');
            }
            return ret.promise;
        }

        function getRegion(query){
            console.log(query);
            var ret = $q.defer();
            if(query.length > 4){
                $http.get('https://api.postcodes.io/postcodes/'+query).then(function(response){
                    ret.resolve({'region':response.data.result.admin_district});
                });
            }else{
                ret.reject('not full postcode');
            }

            return ret.promise;
        }
        function getRegionLat(obj){
            var ret = $q.defer();
            if(obj.lng){
                $http.get('https://api.postcodes.io/postcodes?lon=' + obj.lng + '&lat=' + obj.lat).then(function (response) {
                    ret.resolve(response.data.result[0].admin_district);
                });
            }else{
                console.log('error with lat lng');
                ret.reject('no lat or lng');
            }
            return ret.promise;
        }

        return{
            getpost:getpost,
            locpost:locpost,
            getRegion:getRegion,
            getRegionLat:getRegionLat
        };


    }])

    .service('userlog', ['$http','$q','USER','poslocs', function($http, $q,USER,poslocs){
        function registeruser(data){
            var dt = JSON.stringify(data);
            console.log(dt);
            return $http({
                method: 'POST',
                url: '/user/registration/',
                headers: { 'Content-Type': 'application/json'},
                data: dt
            }).then(function(response){
                console.log(response);
                if(response.status == 200){
                    USER.addusername(data.username);
                    USER.addsession(response.data.token);
                    return response;
                }
                return response;
            }).catch(function(err){
                return err;
            });
        }

        function updateuser(data){
            var session = USER.getsession();
            if(data.postcode != null){
                poslocs.getpost(data.postcode).then(function(res){
                    data['latitude'] = res.lat;
                    data['longitude'] = res.lng;
                });
            }
            if(session != null){
                data['token'] = session;
                var dt = JSON.stringify(data);
                return $http({
                    method:'POST',
                    url:'/user/update/',
                    headers:{'Content-Type': 'application/json'},
                    data: dt
                }).then(function(response){
                    return response;
                }).catch(function(err){
                    return err;
                })
            }
            return false;
        }

        function loginuser(data){
            return $http({
                method:'POST',
                url:'/user/login/',
                headers:{'Content-Type':'application/json'},
                data:data
            }).then(function(response){
                if(response.status == 200){
                    USER.addusername(response.data.username);
                    USER.addsession(response.data.token);
                    return response;
                }
            }).catch(function(err){
                return err
            })
        }

        function getdata(){
            var data = {"token":USER.getsession()};
            var dt = JSON.stringify(data);
            return $http({
                method:'POST',
                url:'/user/',
                header:{'Content-Type':'application/json'},
                data:dt
            }).then(function(resp){
                return resp.data;
            }).catch(function(err){
                return err;
            })
        }

        function fbUser(data){
            return $http({
                method:'POST',
                url:'/user/facebooklogin/',
                headers:{'Content-Type':'application/json'},
                data:data
            }).then(function(response){
                if(response.status == 200){
                    USER.addusername(response.data.username);
                    USER.addsession(response.data.token);
                    return response;
                }
            }).catch(function(err){
                return err
            })
        }

        function GlUser(data){
            return $http({
                method:'POST',
                url:'/user/googlelogin/',
                headers:{'Content-Type':'application/json'},
                data:data
            }).then(function(response){
                if(response.status == 200){
                    console.log(response);
                    if(response.data.username == "None"){
                        USER.addusername(response.config.data.id);
                    }else{
                        USER.addusername(response.data.username);
                    }
                    USER.addsession(response.data.token);
                    return response;
                }
            }).catch(function(err){
                return err
            })
        }

        function MailUser(data){
            return $http({
                method:'POST',
                url:'/user/maillist/',
                headers:{'Content-Type':'application/json'},
                data:data
            }).then(function(response){
                if(response.status == 202){
                    return response;
                }
            }).catch(function(err){
                return err
            })
        }

        return{
            registeruser:registeruser,
            update: updateuser,
            login: loginuser,
            getdata: getdata,
            fbUser: fbUser,
            GlUser: GlUser,
            MailUser: MailUser
        }
    }])

    .service('ALLOW',['$cookies',function($cookies){
        var allow = function(){
            var expireData = new Date(3000,10,10,13,30,30,30);
            $cookies.put('plexusAllow','true',{'expires':expireData});
        };

        var check = function(){
            if($cookies.get('plexusAllow')){
                return true;
            }else{
                return false;
            }
        };

        return{
            allow: allow,
            check: check
        }
    }])

    .service('nhsDetails',[function(){
        var checkDetails = function(item){
            if(item.telephone.length < 4){
                if(item.trust.contacts.telephone){
                    item.telephone = item.trust.contacts.telephone;
                }
            }
            if(item.email.length < 4){
                if(item.trust.contacts.email.length > 1){
                    item.email = item.trust.contacts.email;
                }
            }
            if(item.website.length < 4){
                if(item.trust.contacts.website){
                    item.website = item.trust.contacts.website;
                }
            }
            return item;
        };

        return{
            checkDetails: checkDetails
        }
    }])

    .service('GEO',[function(){

        var toDegree = function(red){
            return red * 180 / Math.PI;
        };

        var toRadians = function(deg){
            return deg * Math.PI / 180;
        };

        var calculateDistance = function(starting, ending, dec){
            var KM_RATIO = 6371;
            try {
                var dLat = toRadians(parseFloat(ending.lat) - parseFloat(starting.lat));
                var dLon = toRadians(parseFloat(ending.lng) - parseFloat(starting.lng));
                var lat1Rad = toRadians(starting.lat);
                var lat2Rad = toRadians(ending.lat);
                var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1Rad) * Math.cos(lat2Rad);
                var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                var d = KM_RATIO * c;
                return d.toFixed(dec);
            } catch(e) {
                return -1;
            }
        };

        var posdiff = function(x1, x2){
            // 1 is positive, 0 negative
            var at = null;
            var bt = null;
            if(x1 > 0.0){
                at = 1;
            }else{
                at = 0;
            }
            if(x2 > 0.0){
                bt = 1;
            }else{
                bt = 0;
            }
            if (at == bt){
                return true;
            }else{
                return false;
            }
        };

        return{
            toDegree:toDegree,
            toRadians:toRadians,
            cal:calculateDistance,
            posdiff: posdiff
        }
    }])

    .service('splashserv',['$cookies','ALLOW', function($cookies, ALLOW){
        var setvisited = function(){
            if(ALLOW.check()){
                var expireData = new Date(3000,10,10,13,30,30,30);
                $cookies.put('plexusVisit','visited',{'expires':expireData});
            }
        };

        var hasvisited = function(){
            if($cookies.get('plexusVisit')){
                return true;
            }else{
                return false;
            }
        };

        return{
            setvisited: setvisited,
            hasvisited: hasvisited
        }

    }])


    .service('USER',['$cookies','ALLOW', function($cookies, ALLOW){
        var username = null;
        var session = null;
        var token = null;
        var userdata = null;
        var notif = true;

        var addusername = function(newObj){
            username = newObj;
        };

        var addsession = function(newObj){

            session = newObj;
            if(ALLOW.check()) {
                var expireData = new Date();
                expireData.setDate(expireData.getDate() + 30);
                $cookies.put('plexusToken', newObj, {'expires': expireData});
                if ($cookies.get('plexusSkip')) {
                    $cookies.remove('plexusSkip');
                }

            }
        };

        var addtoken = function(newObj){
            token = newObj;
        };

        var getusername = function(){
            return username;
        };

        var getsession = function(){
            if (session == null){
                var use = $cookies.get('plexusToken');
                if (use){
                    session = use;
                }
            }
            return session;
        };

        var gettoken = function(){
            return token;
        };

        var loggedin = function(){

            if(session != null){
                return true;
            }

            if($cookies.get('plexusToken')){
                return true;
            }

            return false;
        };

        var logout = function(){
            if(session != null){
                if( $cookies.get('plexusToken')){
                    $cookies.remove('plexusToken');
                }
                session = null;
                username = null;
                token = null;
                userdata = null;
                return true;
            }
            return false;
        };

        var setuserdata = function(newObj){
            userdata = newObj;
        };

        var getuserdata = function(){
            return userdata;
        };

        var setskip = function(){
            if(ALLOW.check()) {
                var expireData = new Date();
                expireData.setDate(expireData.getDate() + 30);
                $cookies.put('plexusSkip', 'true', {'expires': expireData});
            }
        };

        var removeskip = function(){
            if( $cookies.get('plexusSkip')){
                $cookies.remove('plexusSkip');
            }
        };

        var testskip = function(){
            if( $cookies.get('plexusSkip')){
                return true;
            }else{
                return false
            }
        };

        var addnotify = function(){
            if(ALLOW.check()) {
                var expireData = new Date(3000,10,10,13,30,30,30);
                $cookies.put('plexusAdded','true',{'expires':expireData});
            }
            notif = false;
        };

        var notifycheck = function(){
            if(notif){
                if( $cookies.get('plexusAdded')){
                    return true;
                }else{
                    return false
                }
            }else{
                return true;
            }

        };

        return{
            addusername: addusername,
            addsession: addsession,
            addtoken: addtoken,
            getusername: getusername,
            getsession: getsession,
            gettokem: gettoken,
            loggedin: loggedin,
            logout: logout,
            setuserdata: setuserdata,
            getuserdata: getuserdata,
            setskip: setskip,
            removeskip:removeskip,
            testskip:testskip,
            addnotify:addnotify,
            notifycheck:notifycheck
        }

    }])


    .service('locposts', ['poslocs','$cookies', 'ALLOW','$q', function(poslocs, $cookies, ALLOW, $q){
        var location = null;
        var postcode = null;
        var distance = 10;
        var region = null;
        var services = [];

        var addLocation = function(newObj){
            location = newObj;
            if(ALLOW.check()) {
                $cookies.put('plexusLoclat', newObj.lat);
                $cookies.put('plexusLoclng', newObj.lng);
            }
            poslocs.locpost(newObj).then(function (res) {
                addPostcode(res);
            });
        };

        var addRegion = function(newObj){
            region = newObj;
        };

        var addPostcode = function(newObj){
            postcode = newObj;
            if(ALLOW.check()) {
                $cookies.put('plexusPostCode', newObj);
            }
        };

        var addDist = function(newObj){
            distance = newObj;
        };

        var getLocation = function(){
            var use = {lat:null, lng:null};
            use.lat = parseFloat($cookies.get('plexusLoclat'));
            use.lng = parseFloat($cookies.get('plexusLoclng'));
            if(use.lat){
                location = use;
                return location;
            }else{
                return false;
            }
        };

        var getRegion = function(){
            var ret = $q.defer();
            if(region){
                ret.resolve(region);
            }
            if(location){
                poslocs.getRegionLat(location).then(function(res){
                    ret.resolve(res.replace(/\s+/g, '-').toLowerCase());
                })
            }else{
                ret.reject('no location');
            }
            return ret.promise;
        };

        var getPostcode = function(){
            var use = $cookies.get('plexusPostCode');
            var post;
            if(use){
                post = use;
            }else{
                post = postcode;
            }
            return post;
        };

        var getDist = function(){
            return distance;
        };

        var haslocation = function(){
            if(location != null){
                return getLocation();
            }else{
                return false;
            }
        };

        var clear = function(){
            location = null;
            postcode = null;
            distance = 10;
            region = null;
            $cookies.remove('plexusLoclat');
            $cookies.remove('plexusLoclng');
            $cookies.remove('plexusPostCode');
        };

        var addservice = function(newobj){
            services = [];
            for(var i in newobj){
                services.push(newobj[i]);
            }
        };

        var clearservice = function(){
            services = [];
        };

        var getservices = function(){
            return services;
        };

        return {
            addLocation: addLocation,
            addPostcode: addPostcode,
            addDist: addDist,
            getLocation: getLocation,
            getPostcode: getPostcode,
            getDist: getDist,
            haslocation: haslocation,
            clear: clear,
            addRegion:addRegion,
            getRegion:getRegion,
            addservice:addservice,
            clearservice:clearservice,
            getservices: getservices
        };
    }])

    .service("markercreate", ['$q', function($q){

        function mind(lst){
            var parker = $q.defer();
            var parklocation = {};
            var markers = {'mind':'img/circle-purple.svg','nhs':'img/circle-blue.svg','doit':'img/circle-grey.svg'};
            for (var i =0; i > lst.length ; i++){
                parklocation[i] = {
                    'lat':parseFloat(lst[i]._latitude_postcode),
                    'lng':parseFloat(lst[i]._longitude_postcode),
                    'message':lst[i].title,
                    'icon':{
                        iconUrl:markers['mind'],
                        iconSize:     [30, 30],
                        iconAnchor:   [15, 15],
                        popupAnchor:  [0, 0]
                    }
                }
            }
            parket.resolve(parkLocations);
            return parket.promise;
        };

        function nhs(lst){
            var parker = $q.defer();
            var parklocation = {};
            var markers = {'mind':'img/circle-purple.svg','nhs':'img/circle-blue.svg','doit':'img/circle-red.svg'};
            for (var i =0; i > lst.length ; i++){
                parklocation[i] = {
                    'lat':parseFloat(lst[i]._latitude_postcode),
                    'lng':parseFloat(lst[i]._longitude_postcode),
                    'message':lst[i].title,
                    'icon':{
                        iconUrl:markers['nhs'],
                        iconSize:     [30, 30],
                        iconAnchor:   [15, 15],
                        popupAnchor:  [0, 0]
                    }
                }
            }
            parket.resolve(parkLocations);
            return parket.promise;
        }


        return{
            mind:mind,
            nhs:nhs
        };
    }])

    .factory("locpost", ["$http", "GEO", "$q", function($http, GEO, $q){
        return {
            query: function(lng, lat){
                if(lng && lat){
                    return $http.get('https://api.postcodes.io/outcodes?lon='+lng+'&lat='+lat).then(function(response){
                        return response.result[0].outcode;
                    })
                }else{
                    return $q.reject('no lat or lng');
                }
            }
        }
    }])

    .factory("doitData", ['$http', '$q', function($http, $q){
        var doitData = function(data){
            angular.extend(this, data);
        };
        var url = "https://knowledge.plexus.support.support/api/v1/"; // May have it all the other bits

        function toDegree(red){
            return red * 180 / Math.PI;
        }

        function toRadians(deg){
            return deg * Math.PI / 180;
        }

        doitData.getJobsid = function(id){
            /*
             THis will get the jobs of a certain id
             */
            return $http.get(url + 'jobs/'+ id +' ?format=json').then(function(response){
                return new doitData(response.data);
            })
        };

        doitData.getOppid = function(id){
            /*
             This will return the opp of a certin id
             */
            return $http.get(url + 'opportunities/'+ id +' ?format=json').then(function(response){
                return new doitData(response.data);
            })
        };

        doitData.jobLatlng =function(loc, dist){
            /*
             This will query the mind with location and distance from it
             distance section
             */
            console.log(loc);
            if(loc.lat != undefined){
                if(dist<5){
                    dist = 5;
                }
                var R = 6371;
                var x1 = loc.lng - toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var x2 = loc.lng + toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var y1 = loc.lat - toDegree(dist/R);
                var y2 = loc.lat + toDegree(dist/R);

                if(GEO.posdiff(x1,x2)) {
                    return $http.jsonp(url + 'jobs/?latitude__gt=' + y1 + '&latitude__lt=' + y2 + '&longitude__gt=' + x1 + '&longitude__lt=' + x2 + '&callback=JSON_CALLBACK').then(function (response) {
                        return new doitData(response.data);
                    });
                }else{
                    return $http.jsonp(url + 'jobs/?latitude__gt=' + y1 + '&latitude__lt=' + y2 + '&longitude__gt=' + x1 +  '&callback=JSON_CALLBACK').then(function (response) {
                        return new doitData(response.data);
                    });
                }
            }else{
                return new doitData(null);
            }
        };

        doitData.oppLatlng =function(loc, dist){
            /*
             This will query the mind with location and distance from it
             distance section
             */

            if(loc.lat != undefined){
                if(dist<5){
                    dist = 5;
                }
                var R = 6371;
                var x1 = loc.lng - toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var x2 = loc.lng + toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var y1 = loc.lat - toDegree(dist/R);
                var y2 = loc.lat + toDegree(dist/R);

                return $http.jsonp(url + 'opportunities/?latitude__gt='+ y1 +'&latitude__lt='+ y2 +'&longitude__gt='+ x1 +'&longitude__lt='+ x2 +'&callback=JSON_CALLBACK').then(function(response) {
                    return new doitData(response.data);
                });
            }else{
                return new doitData(null);
            }
        };
        return doitData;

    }])

    .factory("doitJobs", ['$http', 'GEO', function($http, GEO){
        var doitJobs = function(data){
            angular.extend(this, data);
        };
        var url = "https://knowledge.plexus.support.support/api/v1/"; // May have it all the other bits

        function toDegree(red){
            return red * 180 / Math.PI;
        }

        function toRadians(deg){
            return deg * Math.PI / 180;
        }

        doitJobs.getJobsid = function(id){
            /*
             THis will get the jobs of a certain id
             */
            return $http.get(url + 'jobs/'+ id +' ?format=json').then(function(response){
                return new doitJobs(response.data);
            })
        };

        doitJobs.getOppid = function(id){
            /*
             This will return the opp of a certin id
             */
            return $http.get(url + 'opportunities/'+ id +' ?format=json').then(function(response){
                return new doitJobs(response.data);
            })
        };

        doitJobs.jobLatlng =function(loc, dist){
            /*
             This will query the mind with location and distance from it
             distance section
             */
            if(loc.lat != undefined) {
                if (dist < 10) {
                    dist = 15;
                }
                var R = 6371;
                var x1 = loc.lng - toDegree(dist / R / Math.cos(toRadians(loc.lat)));
                var x2 = loc.lng + toDegree(dist / R / Math.cos(toRadians(loc.lat)));
                var y1 = loc.lat - toDegree(dist / R);
                var y2 = loc.lat + toDegree(dist / R);
                if(GEO.posdiff(x1,x2)) {
                    return $http.jsonp(url + 'jobs/?latitude__gt=' + y1 + '&latitude__lt=' + y2 + '&longitude__gt=' + x1 + '&longitude__lt=' + x2 + '&callback=JSON_CALLBACK').then(function (response) {
                        return new doitJobs(response.data);
                    });
                }else{
                    return $http.jsonp(url + 'jobs/?latitude__gt=' + y1 + '&latitude__lt=' + y2 + '&longitude__gt=' + x1 + '&callback=JSON_CALLBACK').then(function (response) {
                        return new doitJobs(response.data);
                    });
                }
            }else{
                return new doitData(null);
            }
        };


        return doitJobs;

    }])

    .factory("hospitalData", ['$http','GEO','$q', function($http, GEO, $q){
        var nhsData = function(data){
            angular.extend(this, data);
        };
        var url = "/api/v1/"; // May have it all in one go

        function toDegree(red){
            return red * 180 / Math.PI;
        }

        function toRadians(deg){
            return deg * Math.PI / 180;
        }

        nhsData.getall = function(){
            /*
             This will get all the data for the hospital
             */
            return $http.get(url + 'hospitals/?format=json').then(function(response){
                return new nhsData(response.data);
            });
        };

        nhsData.getid = function(id){
            /*
             This will get the data from the hospital with a id
             */
            return $http.get(url + 'hospitals/'+ id +'/?format=json').then(function(response){
                return new nhsData(response.data);
            });
        };

        nhsData.getLatlng =function(loc, dist){
            /*
             This will query the mind with location and distance from it
             distance section
             */
            if(loc.lat != undefined) {
                if(dist<5){
                    dist = 5;
                }
                var R = 6371;
                var x1 = loc.lng - toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var x2 = loc.lng + toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var y1 = loc.lat - toDegree(dist/R);
                var y2 = loc.lat + toDegree(dist/R);
                if(GEO.posdiff(x1,x2)) {
                    return $http.jsonp(url + 'hospitals/?latitude__gt=' + y1 + '&latitude__lte=' + y2 + '&longitude__lte=' + x2 + '&longitude__gte=' + x1 + '&callback=JSON_CALLBACK').then(function (response) {
                        return new nhsData(response.data);
                    });

                }else{
                    //return $http.jsonp(url + 'hospitals/?latitude__gt=' + y1 + '&latitude__lte=' + y2 + '&longitude__lte=' + x2 + '&callback=JSON_CALLBACK').then(function (response) {
                    return $http.jsonp(url + 'hospitals/?latitude__gt=' + y1 + '&latitude__lte=' + y2 + '&longitude__lte=' + x2 + '&longitude__gte=' + x1 + '&callback=JSON_CALLBACK').then(function (response) {
                        return new nhsData(response.data);
                    });
                }
            }else{
                return new nhsData($q.reject('no loc'));
            }
        };

        return nhsData;

    }])

    .factory("onlineData",['$http', function($http){
        var onlineData = function(data){
            angular.extend(this,data);
        };

        var url = "/api/v1/";

        onlineData.getSections = function(){
            return $http.get(url + 'old_section?format=json').then(function(response){
                return new onlineData(response.data)
            });
        };

        onlineData.getItems = function(section){
            return $http.get(url + 'old_btwitem?section__slug__exact='+section+'&format=json').then(function(response){
                return new onlineData(response.data)
            });
        };

        onlineData.getReturn = function(){
            return $http.get(url + 'old_btwitem?section__slug__exact=returning-to-work&format=json').then(function(response){
                return new onlineData(response.data)
            });
        };

        onlineData.getGuidance = function(){
            return $http.get(url + 'old_btwitem?section__slug__exact=application-guidance&format=json').then(function(response){
                return new onlineData(response.data)
            });
        };

        onlineData.getSlider = function(){
            return $http.get(url + 'frontslide?format=json').then(function(response){
                return new onlineData(response.data)
            });
        };

        return onlineData;
    }])

    .factory("mindData",['$http', 'GEO', '$q', function($http, GEO, $q){
        var mindData = function(data) {
            angular.extend(this, data);
        };
        var url = "/data/";

        function toDegree(red){
            return red * 180 / Math.PI;
        }

        function toRadians(deg){
            return deg * Math.PI / 180;
        }

        mindData.getall = function(){
            /*
             This will produce a list of all minds
             */
            return $http.get(url + 'minds/?format=json').then(function(response) {
                return new mindData(response.data);
            });
        };

        mindData.mindid = function(id){
            /*
             This is to get a mind by id
             */
            return $http.get(url +'minds/' + id + '?format=json').then(function(response) {
                return new mindData(response.data);
            });
        };

        mindData.mindLatlng =function(loc, dists){
            /*
             This will query the mind with location and distance from it
             distance section
             */
            var dist = parseInt(dists);
            if(loc.lat != undefined) {
                if (dist < 10){
                    dist = 15;
                }
                var R = 6371;
                var x1 = loc.lng - toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var x2 = loc.lng + toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var y1 = loc.lat - toDegree(dist/R);
                var y2 = loc.lat + toDegree(dist/R);

                if(GEO.posdiff(x1,x2)){
                    return $http.jsonp(url + 'minds/?_latitude_postcode__gt='+ y1 +'&_latitude_postcode__lt='+ y2 +'&_longitude_postcode__lt='+ x1+'&_longitude_postcode__gt='+ x2 +'&callback=JSON_CALLBACK').then(function(response) {
                        return new mindData(response.data);
                    });
                }else{
                    return $http.jsonp(url + 'minds/?_latitude_postcode__gt='+ y1 +'&_latitude_postcode__lt='+ y2 +'&_longitude_postcode__lt='+ x1+'&callback=JSON_CALLBACK').then(function(response) {
                        return new mindData(response.data);
                    });
                }

            }else{
                return new mindData($q.reject('no loc'));
            }
        };

        mindData.mindServices = function(){
            /*
             This is a function to get all the services
             */
            return $http.get(url + 'services/?format=json').then(function(response){
                return new mindData(response.data);
            })
        };

        mindData.service = function(id){
            /*
             Get the service from id
             */
            return $http.get(url + 'services/' + id + '?format=json').then(function(response){
                return new mindData(response.data);
            })
        };

        mindData.serviceLM = function(id){
            /*
             Get the services for a certain local mind
             */
            return $http.get(url + 'services/?minds__id'+ id + '&format=json').then(function(response){
                return new mindData(response.data);
            })
        };

        mindData.grouping = function(){
            return $http.jsonp(url + 'service_grouping/?callback=JSON_CALLBACK').then(function(response){
                return new mindData(response.data);
            })
        };

        mindData.servicegroup = function(idlist){
            var sect = '';
            for(var i in idlist){
                sect += 'services__category='+ idlist[i] + '&';
            }
            return $http.jsonp(url +'minds/?'+ sect + 'callback=JSON_CALLBACK').then(function(response){
                return new mindData(response.data);
            })
        };

        mindData.serviceLat = function(idlist, loc, dists){
            var sect = '';
            for(var i in idlist){
                sect += 'services__category='+ idlist[i] + '&';
            }
            var dist = parseInt(dists);
            if(loc.lat != undefined) {
                if (dist < 10){
                    dist = 15;
                }
                var R = 6371;
                var x1 = loc.lng - toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var x2 = loc.lng + toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var y1 = loc.lat - toDegree(dist/R);
                var y2 = loc.lat + toDegree(dist/R);

                if(GEO.posdiff(x1,x2)){
                    return $http.jsonp(url + 'minds/?'+ sect + '_latitude_postcode__gt='+ y1 +'&_latitude_postcode__lt='+ y2 +'&_longitude_postcode__lt='+ x1+'&_longitude_postcode__gt='+ x2 +'&callback=JSON_CALLBACK').then(function(response) {
                        return new mindData(response.data);
                    });
                }else{
                    return $http.jsonp(url + 'minds/?'+ sect + '_latitude_postcode__gt='+ y1 +'&_latitude_postcode__lt='+ y2 +'&_longitude_postcode__lt='+ x1+'&callback=JSON_CALLBACK').then(function(response) {
                        return new mindData(response.data);
                    });
                }
            }else{
                return new mindData(null);
            }
        };

        return mindData;
    }])

    .service('flatblocks',[function(){
        var flatblock = {};

        var addFlat = function(obj){
            flatblock[obj.slug] = obj;
        };

        var getFlat = function(obj){
            if(flatblock[obj]){
                return flatblock[obj].content
            }else{
                return ' '
            }
        };

        return{
            addFlat: addFlat,
            getFlat: getFlat
        }
    }])

    .factory('siteData', ['$http','flatblocks', function($http,flatblocks){
        var siteData = function(data) {
            angular.extend(this, data);
        };

        var url = "/api/v1/";

        siteData.getLegal = function(){
            return $http.get(url + 'legal').then(function(response){
                return new siteData(response.data);
            });
        };

        siteData.getOur = function(){
            return $http.get(url +'data/').then(function(response){
                return new siteData(response.data);
            });
        };

        siteData.addmail = function(paramer){
            return $http({
                method: 'POST',
                url: url + 'maillist/',
                headers: { 'Content-Type': 'application/json; charset=UTF-8'},
                data: paramer
            }).then(function(response){
                return new siteData(response.data)
            });
        };

        siteData.flatblock = function(){
            return $http.get(url + 'flatblock/').then(function(response){

                return new siteData(response.data);
            })
        };

        return siteData;

    }])


    .service('aroundLocalAuth',[function(){
        var loclauth = {};

        var addRegion = function(obj){
            loclauth[obj.slug] = obj;
        };

        var getRegion = function(obj){
            if(loclauth[obj]){
                return loclauth[obj];
            }else{
                return none;
            }
        };

        var allRegions = function(){
            return loclauth;
        };

        return{
            addRegion:addRegion,
            getRegion:getRegion,
            allRegion:allRegions
        }
    }])

    .factory('localAuthData', ['aroundLocalAuth', '$http', function(aroundRegion, $http){
        var regionData = function(data) {
            angular.extend(this, data);
        };

        var url = "/api/v1/";

        regionData.getData = function(){
            return $http.get(url + 'localathority_name/?format=json').then(function(response) {
            return new regionData(response.data)
            });
        };

        regionData.getLocal = function(param){
            return $http.get(url + 'localathority?slug='+ param +'&format=json').then(function(response) {
                return new regionData(response.data.objects)
            });
        };

        regionData.getRegion = function(param){
            return $http.get(url + 'region?format=json').then(function(response) {
                return new regionData(response.data.objects)
            });
        };

        return regionData;
    }])


;


