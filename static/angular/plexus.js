'use strict';

angular.module('rework', [
                        'ngSanitize',
                        'ngRoute',
                        'ngCookies',
                        'rework.controllers',
                        'ui.router',
                        'ngAnimate',
                        'leaflet-directive',
                        'ngUpload',
                        'facebook',
                        'directive.g+signin',
                        'mailchimp',
                        'angulartics',
                        'angulartics.google.analytics'
                    ])

    .config([
        '$httpProvider',
        '$interpolateProvider',
        function($httpProvider, $interpolateProvider) {
            $interpolateProvider.startSymbol('{$');
            $interpolateProvider.endSymbol('$}');
            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
            $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        }])

    .config(function ($analyticsProvider) {
        $analyticsProvider.firstPageview(true); /* Records pages that don't use $state or $route */
        $analyticsProvider.withAutoBase(true);  /* Records full path */
        $analyticsProvider.withBase(true);
    })

    .run([
        '$http',
        '$cookies',
        '$route',
        '$rootScope',
        '$state',
        function($http, $cookies,$route,$rootScope,$state) {
            $http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;
            $rootScope.$on("$stateChangeError", console.log.bind(console));
            $rootScope.tomenu = function(){
                $state.go('home');
            };
            $rootScope.goBack = function() {
                window.history.back();
            };
            $rootScope.previousState;
            $rootScope.previousStateParams;
            $rootScope.currentState;

            $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
                $rootScope.previousState = from.name;
                $rootScope.previousStateParams = fromParams;
                $rootScope.currentState = to.name;
            });
        }])
    .config(function($stateProvider, $urlRouterProvider,$locationProvider){

        $stateProvider
            .state("loader", {
                url:"/",
                templateUrl:'/static/templates/loader.html',
                controller:'LoaderCtrl'
            })
            .state("splash",{
                url:"/splash/",
                templateUrl:'/static/templates/splash.html',
                controller:'SplashCtrl'
            })
            .state('home',{
                url: "/menu/",
                templateUrl:'/static/templates/home.html',
                controller:'MainCtrl'
            })
            .state('support',{
                url:'/support',
                templateUrl:'/static/templates/search/home.html',
                controller:'SupportSearchCtrl'
            })
            .state('support_update',{
                url:'/support/update',
                templateUrl:'/static/templates/search/home.html',
                controller:'SupportUpdateCtrl'
            })
            .state('support_results',{
                url:'/support/results',
                templateUrl:'/static/templates/search/results.html',
                controller:'SupportResultsCtrl'
            })
            .state('support_service',{
                url:'/support/services',
                templateUrl:'/static/templates/search/servicesellect.html',
                controller:'SupportServices'
            })
            .state('support_detail',{
                url:'/support/:supporttype/:supportid',
                templateUrl:'/static/templates/search/details.html',
                controller:'SupportDetailCtrl'
            })
            .state('backtowork_vol',{
                url:'/backtowork/search/:type',
                templateUrl:'/static/templates/backtowork/home.html',
                controller:'VoluntCtrl'
            })
            .state('backtowork_update',{
                url:'/backtowork/update/:type',
                templateUrl:'/static/templates/backtowork/home.html',
                controller:'BacktoworkUpdateCtrl'
            })
            .state('backtowork_tabs',{
                url:'/backtowork',
                templateUrl:'/static/templates/backtowork/swipertabs.html',
                controller:'VoluntTabCtrl'
            })
            .state('backtowork_vol_list',{
                url:'/backtowork#volunteering',
                templateUrl:'/static/templates/backtowork/swipertabs.html',
                controller:'VoluntListCtrl'
            })
            .state('backtowork_return',{
                url:'/backtowork#return_to_work',
                templateUrl:'/static/templates/backtowork/swipertabs.html',
                controller:'ReturnCtrl'
            })
            .state('backtowork_new',{
                url:'/backtowork#new_work',
                templateUrl:'/static/templates/backtowork/swipertabs.html',
                controller:'NewWorkCtrl'
            })
            .state('backtowork_guidance',{
                url:'/backtowork#guidance',
                templateUrl:'/static/templates/backtowork/swipertabs.html',
                controller:'GuidanceListCtrl'
            })
            .state('backtowork_swiper',{
                url:'/backtowork/sweep',
                templateUrl:'/static/templates/backtowork/swipertabs.html',
                controller:'VoluntTabCtrl'
            })
            .state('legal',{
                url:'/legal_right',
                templateUrl:'/static/templates/tabsections/sweeptab.html',
                controller: 'legalCtrl'
            })
            .state('signup',{
                url:'/mydetails/signup',
                templateUrl:'/static/templates/details/signup.html',
                controller:'SignUpCtrl',
                data:{
                    start:false
                }
            })
            .state('register',{
                url:'/mydetails/register',
                templateUrl:'/static/templates/details/register.html',
                controller:'RegisterCtrl',
                data:{
                    start:false
                }
            })
            .state('update',{
                url:'/mydetails/update',
                templateUrl:'/static/templates/details/update.html',
                controller:'UpdateFormCtrl',
                data:{
                    start:false
                }
            })
            .state('login',{
                url:'/mydetails/login',
                templateUrl:'/static/templates/details/login.html',
                controller:'LogInCtrl',
                data:{
                    start:false
                }
            })
            .state('start_register',{
                url:'/mydetails/register',
                templateUrl:'/static/templates/details/register.html',
                controller:'RegisterCtrl',
                data:{
                    start:true
                }
            })
            .state('start_update',{
                url:'/mydetails/update',
                templateUrl:'/static/templates/details/update.html',
                controller:'UpdateFormCtrl',
                data:{
                    start:true
                }
            })
            .state('start_login',{
                url:'/mydetails/login',
                templateUrl:'/static/templates/details/login.html',
                controller:'LogInCtrl',
                data:{
                    start:true
                }
            })
            .state('mydetail',{
                url:'/mydetails',
                templateUrl:'/static/templates/details/swipedetails.html',
                controller:'DetailCtrl'
            })
            .state('imageupload', {
                url:'/mydetails/image',
                templateUrl:'/static/templates/details/imageupload.html',
                controller:'ImgUpCtrl'
            })
            .state('achivements',{
                url:'/achivements',
                templateUrl:'/static/templates/details/achivements.html',
                controller:'AchiveCtrl'
            })
            .state('ourdata',{
                url:'/our_data',
                templateUrl:'/static/templates/tabsections/sweeptab.html',
                controller:'OurDataCtrl'
            })
            .state('startsign',{
                url:'/start',
                templateUrl:'/static/templates/details/signup.html',
                controller:'SignUpCtrl',
                data:{
                    start:true
                }
            })
            .state('aroundyou',{
                url:'/aroundyou',
                templateUrl:'/static/templates/aroundyou/home.html',
                controller:'AroundSearchCtrl',
                data:{
                    update:false,
                    error:false
                }
            })
            .state('aroundyou_update',{
                url:'/aroundyou',
                templateUrl:'/static/templates/aroundyou/home.html',
                controller:'AroundSearchCtrl',
                data:{
                    update:true,
                    error:false
                }
            })
            .state('aroundyou_error',{
                url:'/aroundyou/error',
                templateUrl:'/static/templates/aroundyou/error.html',
                controller:'AroundSearchCtrl',
                data:{
                    update:true,
                    error:true
                }
            })
            .state('aroundyou_details',{
                url:'/aroundyou/results',
                templateUrl:'/static/templates/aroundyou/sweeppage.html',
                controller:'AroundDetailCtrl'
            });
    })

    .config(function(FacebookProvider){
        FacebookProvider.init('1795428174017119');
    });
'use strict';

angular.module('rework.controllers',['rework.services','rework.directive', 'geolocation','ngSanitize'])


    .filter('commabreak', function(){
        return function(value){
            if(!value){return null};
            return value =  value.replace(/ *, */g, ',<br>');
        }
    })

    .filter('to_trusted', ['$sce', function($sce){
        return function(text) {
            return $sce.trustAsHtml(text);
        };
    }])

    .filter('htmlToPlaintext', function() {
        return function(text) {
            return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
        };
    })
    .filter('datestring', function(){
        return function(value){
            if(!value){return 'None'}
            var dt = new Date(value);
            return ""+dt.getDate()+"/"+dt.getMonth()+"/"+dt.getUTCFullYear();
        }
    })
    .filter('unique', function () {

        return function (items, filterOn) {

            if (filterOn === false) {
                return items;
            }

            if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
                var hashCheck = {}, newItems = [];

                var extractValueToCompare = function (item) {
                    if (angular.isObject(item) && angular.isString(filterOn)) {
                        return item[filterOn];
                    } else {
                        return item;
                    }
                };

                angular.forEach(items, function (item) {
                    var valueToCheck, isDuplicate = false;

                    for (var i = 0; i < newItems.length; i++) {
                        if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                            isDuplicate = true;
                            break;
                        }
                    }
                    if (!isDuplicate) {
                        newItems.push(item);
                    }

                });
                items = newItems;
            }
            return items;
        };
    })

    .controller('LoaderCtrl',['$scope',function($scope){
        $scope.class='';
    }])

    .controller('SplashCtrl',['$scope','onlineData','$state','splashserv','USER', 'ALLOW', function($scope, onlineData, $state, splashserv, USER, ALLOW){
        $scope.class = 'splash';
        $scope.list = {};

        $scope.visited = function(){
            if(splashserv.hasvisited()){
                if(USER.testskip() || USER.loggedin()){
                    $state.go('home');
                }else{
                    $state.go('startsign');
                }

            }else{
                onlineData.getSlider().then(function(res){
                    $scope.list = res.objects;
                });

                $scope.prev = function(index){
                    console.log(index);
                    splashSwiper.slidePrev(true, 300);
                };

                $scope.next = function(index){
                    if($scope.list.length == index) {
                        splashserv.setvisited();
                        if(USER.testskip()){
                            $state.go('home');
                        }else{
                            $state.go('startsign');
                        }
                    }else{
                        splashSwiper.slideNext(true, 300);
                    }
                };

                $scope.allow = function(index){
                    ALLOW.allow();
                    splashSwiper.slideNext(true, 300);
                };
            }
        };


    }])

    .controller('MainCtrl', ['$scope', 'onlineData', 'siteData','USER','flatblocks', function($scope, onlineData, siteData, USER, flatblocks){
        onlineData.getSections().then(function(res){
            $scope.backtowork = res.objects;
        });
        siteData.getLegal().then(function(res) {
            $scope.legal = res.objects;
        });
        siteData.flatblock().then(function(res){
            for(var i =0; i < res.objects.length; i++){
                flatblocks.addFlat(res.objects[i]);
            }
        });

        $scope.notifys = USER.notifycheck();

        $scope.addnot = function(){
            USER.addnotify();
            $('.notifyer').slideUp();
            $scope.notifys = true;
        };
        $scope.$on("$destroy", function(){
            USER.addnotify();
        });
    }])

    .controller('SearchCtrl', [
        '$scope',
        '$http',
        'locposts',
        '$q',
        '$state',
        '$compile',
        function($scope,
                 $http,
                 locposts,
                 $q,
                 $state,
                 $compile
        ){
            $scope.postcode = '';
            $scope.distance = '';
            $scope.location = {};

            navigator.geolocation.getCurrentPosition(function(data){
                $scope.location = {lat:data.coords.latitude, lng:data.coords.longitude};
            }, function(err){
                console.log("Geocoder failed");
                console.log(err);
            })
        }])

    .controller('SupportCtrl', [
        '$scope',
        '$controller',

        function($scope,
                 $controller){

            $controller('SearchCtrl',{$scope:$scope});
            $scope.class = 'support';
            $scope.title = 'support & services';

        }])

    .controller('SupportSearchCtrl', [
        '$scope',
        'poslocs',
        'locposts',
        '$state',
        '$controller',
        function($scope,
                 poslocs,
                 locposts,
                 $state,
                 $controller){

            $controller('SearchCtrl',{$scope:$scope});

            $scope.class = 'support';
            $scope.title = 'support & services';
            $scope.update = false;

            $scope.postchange = false;

            $scope.start = function(){
                var loc = locposts.haslocation();
                if(loc){
                    $state.go('support_results');
                }
            };

            $scope.servicebtn = function(){
                $state.go('support_service');
            };

            $scope.start();

            $scope.useloc = function(){
                /*
                 Search using gps
                 */
                locposts.addLocation($scope.location);
                poslocs.locpost($scope.location).then(function(res){
                    $scope.postcode = res;
                    locposts.addPostcode($scope.postcode);
                    $state.go('support_results');
                })
            };

            $scope.postcodechange = function(){
                $scope.postchange = true;
            };

            $scope.postcodeenter = function(){
                var item = $scope.postcode;
                locposts.addPostcode($scope.postcode);
                poslocs.getpost(item).then(function(res){
                    $scope.location = res;
                    locposts.addLocation($scope.location);
                    $state.go('support_results');
                })
            };

            $scope.distancechange = function(){
                locposts.addDist($scope.distance);
                if($scope.postchange){
                    $scope.postcodeenter();
                }else{
                    $scope.useloc();
                }
            };
            $scope.doTheBack = function() {
                $state.go('home');
            };
        }])

    .controller('SupportServices',[
        '$scope',
        'locposts',
        'mindData',
        '$rootScope',
        '$state',
        function($scope,
                 locposts,
                 mindData,
                 $rootScope,
                 $state){
            $scope.class = 'support';
            $scope.title = 'support & services';
            $scope.servicesel = locposts.getservices();
            mindData.grouping().then(function(res){
                $scope.checklist = res.objects;
            });

            $scope.addid = function(id, event){
                var index = $scope.servicesel.indexOf(id);
                if(index > -1){
                    $scope.servicesel.splice(index, 1);
                }else{
                    $scope.servicesel.push(id);
                }
            };

            $scope.updatebtn = function(){
                locposts.addservice($scope.servicesel);
                $state.go('support_results');
            }
        }
    ])

    .controller('SupportUpdateCtrl', [
        '$scope',
        'poslocs',
        'locposts',
        '$state',
        '$controller',
        function($scope,
                 poslocs,
                 locposts,
                 $state,
                 $controller){

            $controller('SearchCtrl',{$scope:$scope});

            $scope.class = 'support';
            $scope.title = 'support & services';
            $scope.update = true;
            $scope.postchange = false;
            $scope.distchange = false;

            $scope.start = function(){
                $scope.postchange = false;
                $scope.distchange = false;
            };

            $scope.start();

            $scope.useloc = function(){
                /*
                 Search using gps
                 */
                locposts.addLocation($scope.location);
                poslocs.locpost($scope.location).then(function(res){
                    $scope.postcode = res;
                    locposts.addPostcode($scope.postcode);
                })
            };

            $scope.postcodechange = function(){
                $scope.postchange = true;
            };

            $scope.distancechange = function(){
                $scope.distchange = true;

            };
            $scope.doTheBack = function() {
                $state.go('home');
            };

            $scope.servicebtn = function(){
                $state.go('support_service');
            };

            $scope.updatebtn = function(){
                locposts.clear();
                if($scope.postchange){
                    poslocs.getpost($scope.postcode).then(function(res){
                        $scope.location = res;
                        locposts.addLocation($scope.location);

                        if($scope.distchange){
                            console.log($scope.distance);
                            locposts.addDist($scope.distance);
                        }
                        $state.go('support_results');
                    })
                }else{
                    if($scope.distchange){
                        locposts.addDist($scope.distance);
                        $state.go('support_results');
                    }else{
                        $state.go('support_results');
                    }
                }
            };
        }])

    .controller('SupportResultsCtrl',[
        '$scope',
        'locposts',
        'poslocs',
        'hospitalData',
        'mindData',
        'leafletData',
        'GEO',
        '$filter',
        '$state',
        'nhsDetails',
        function(
            $scope,
            locposts,
            poslocs,
            hospitalData,
            mindData,
            leafletData,
            GEO,
            $filter,
            $state,
            nhsDetails
        ){
            $scope.class = 'support';
            $scope.title = 'support & services';
            $scope.location = locposts.getLocation();
            $scope.postcode = locposts.getPostcode();
            $scope.distance = locposts.getDist();
            $scope.multiple = true;
            $scope.markers = {};
            $scope.nhsmarkers = {};
            $scope.mindmarkers = {};
            $scope.currenttab = 'mind';
            $scope.services = locposts.getservices();
            $scope.filter = false;

            $scope.tiles = {
                url: "https://1.aerial.maps.cit.api.here.com/maptile/2.1/maptile/newest/terrain.day/{z}/{x}/{y}/256/png8?app_id=yaB0SQ2nEdvwdE7LGMOd&app_code=cfm3SIyf49nYHrTIL0L4gw",
            };

            leafletData.getMap("searchres").then(function(map) {
                map.invalidateSize();
            });

            $scope.details = null;
            $scope.detailtype = null;

            $scope.curtab = function(item){
                $scope.currenttab = item;
            };

            $scope.filterGo = function(){
                $state.go('support_service');
            };


            $scope.useloc = function(){
                /*
                 Search using gps
                 */
                navigator.geolocation.getCurrentPosition(function(data){
                    $scope.location = {lat:data.coords.latitude, lng:data.coords.longitude};
                    locposts.addLocation($scope.location);
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                        locposts.addPostcode($scope.postcode);

                        $scope.getdata();

                    })
                }, function(err){
                    console.log("Geocoder failed");
                    console.log(err);
                });

            };

            $scope.updateloc = function(){
                $state.go('support_update');
            };

            $scope.doTheBack = function() {
                $state.go('home');
            };

            $scope.updateLoc = function(){
                locposts.addDist($scope.distance);
                var item = $scope.postcode;
                locposts.addPostcode($scope.postcode);
                poslocs.getpost(item).then(function(res){
                    $scope.location = res;
                    locposts.addLocation($scope.location);
                    $scope.getdata();
                })
            };

            $scope.getdata = function(){
                $scope.center = {};
                $scope.markers = {};

                try{
                    //var hosdata = hospitalData.getall();
                    hospitalData.getLatlng($scope.location, $scope.distance).then(function(data){
                        $scope.nhs = data.objects;
                        for(var i in $scope.nhs){
                            $scope.nhsmarkers[$scope.nhs[i].name.replace('-','') ] = {
                                'lat':parseFloat($scope.nhs[i].latitude),
                                'lng':parseFloat($scope.nhs[i].longitude),
                                'message':$scope.nhs[i].name.replace('-',''),
                                'icon':{
                                    'iconUrl':'/static/img/circle-grey.svg',
                                    'iconSize':[30,30],
                                    'iconAnchor':[15,15],
                                    'popupAnchor':[0,0]
                                }
                            };
                            $scope.nhs[i].dist = parseFloat(GEO.cal($scope.location, {lat:$scope.nhs[i].latitude, lng:$scope.nhs[i].longitude},6));
                            $scope.nhs[i] = nhsDetails.checkDetails($scope.nhs[i]);
                        }
                        $scope.nhs = $filter('orderBy')($scope.nhs, 'dist');
                        $scope.markers = angular.extend($scope.nhsmarkers, $scope.mindmarkers);
                    }, function(err){
                        $scope.nhs = [];
                        console.log('error with nhs items');
                    });
                    mindData.serviceLat($scope.services, $scope.location, $scope.distance).then(function(data){
                        $scope.mind = [];
                        if($scope.services.length > 0){
                            $scope.filter=true;
                            for (var i in data.objects){
                                if (data.objects[i].service_groupings.length > 0){
                                    for(var b in data.objects[i].service_groupings){
                                        if($scope.services.indexOf( data.objects[i].service_groupings[b][0]) > -1){
                                            $scope.mind.push(data.objects[i]);
                                        }
                                    }
                                }
                            }
                        }else{
                            $scope.mind = data.objects;
                            $scope.filter=false;
                        }

                        for(var i in $scope.mind){
                            $scope.mindmarkers[$scope.mind[i].name ] = {
                                'lat':parseFloat($scope.mind[i]._latitude_postcode),
                                'lng':parseFloat($scope.mind[i]._longitude_postcode),
                                'message':$scope.mind[i].name,
                                'icon':{
                                    'iconUrl':'/static/img/circle-blue.svg',
                                    'iconSize':[30,30],
                                    'iconAnchor':[15,15],
                                    'popupAnchor':[0,0]
                                }
                            };
                            $scope.mind[i].dist = parseFloat(GEO.cal($scope.location, {lat:$scope.mind[i]._latitude_postcode, lng:$scope.mind[i]._longitude_postcode},6));
                        }
                        $scope.mind = $filter('orderBy')($scope.mind, 'dist');
                        $scope.markers = angular.extend($scope.nhsmarkers, $scope.mindmarkers);

                    }, function(err){
                        $scope.mind = [];
                        console.log('error with mind items');
                    });

                    $scope.center = {
                        lat: $scope.location.lat,
                        lng: $scope.location.lng,
                        zoom: 15
                    };
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                    });


                }catch(err){console.log(err);}
            };

            $scope.getdata();

            $scope.openItem = function(id, type){
                if(type == 'mind'){
                    $scope.details = $scope.mind[id];
                    $scope.detailtype = 'mind';
                }else if (type == 'nhs'){
                    $scope.details = $scope.nhs[id];
                    $scope.detailtype = 'nhs';
                }else{
                    $scope.details = null;
                    $scope.detailtype = null;
                }
            }
        }])

    .controller('SupportDetailCtrl',[
        '$scope',
        '$http',
        '$stateParams',
        'hospitalData',
        'mindData',
        function(
            $scope,
            $http,
            $stateParams,
            hospitalData,
            mindData
        ){
            $scope.class = 'support';
            $scope.title = 'support & services';

            $scope.id = $stateParams.supportid;
            $scope.type = $stateParams.supporttype;

            $scope.doTheBack = function() {
                window.history.back();
            };
        }
    ])

    .controller('VoluntCtrl',[
        '$scope',
        'locposts',
        'poslocs',
        '$state',
        '$sanitize',
        '$controller',
        '$location',
        '$stateParams',
        function(
            $scope,
            locposts,
            poslocs,
            $state,
            $sanitize,
            $controller,
            $location,
            $stateParams
        ){
            $scope.class = 'backtowork';
            $scope.title = 'back to work';
            $scope.multiple = false;
            $scope.update = false;
            $scope.type = $stateParams['type'];

            $scope.postcode = '';
            $scope.distance = '';
            $scope.location = {};

            $scope.postchange = false;

            $scope.start = function(){
                var loc = locposts.haslocation();
                if(loc){
                    if($scope.type == 'volunteering'){
                        $state.go('backtowork_vol_list');
                    }else if($scope.type == 'new_work'){
                        $state.go('backtowork_new');
                    }
                }
            };
            $scope.start();


            $scope.doTheBack = function() {
                $state.go('home');
            };

            $scope.useloc = function(){
                /*
                 Search using gps
                 */
                navigator.geolocation.getCurrentPosition(function(data) {
                    $scope.location = {lat:data.coords.latitude, lng:data.coords.longitude};
                    locposts.addLocation($scope.location);
                    poslocs.locpost($scope.location).then(function (res) {
                        $scope.postcode = res;
                        locposts.addPostcode($scope.postcode);
                        if ($scope.type == 'volunteering') {
                            $state.go('backtowork_vol_list');
                        } else if ($scope.type == 'new_work') {
                            $state.go('backtowork_new');
                        }else{
                            $state.go('backtowork_vol_list');
                        }

                    })
                }, function(err){
                    console.log("Geocoder failed");
                    console.log(err);
                });
            };

            $scope.postcodechange = function(){
                $scope.postchange = true;
            };

            $scope.postcodeenter = function(){
                var item = $scope.postcode;
                locposts.addPostcode($scope.postcode);
                poslocs.getpost(item).then(function(res){
                    $scope.location = res;
                    locposts.addLocation($scope.location);
                    if($scope.type == 'volunteering'){
                        $state.go('backtowork_vol_list');
                    }else if($scope.type == 'new_work'){
                        $state.go('backtowork_new');
                    }else{
                        $state.go('backtowork_vol_list');
                    }
                })
            };

            $scope.distancechange = function(){
                locposts.addDist($scope.distance);
                if($scope.postchange){
                    $scope.postcodeenter();
                }else{
                    $scope.useloc();
                }
            };

        }])

    .controller('BacktoworkUpdateCtrl',[
        '$scope',
        'locposts',
        'poslocs',
        '$state',
        '$sanitize',
        '$controller',
        '$location',
        '$stateParams',
        function(
            $scope,
            locposts,
            poslocs,
            $state,
            $sanitize,
            $controller,
            $location,
            $stateParams
        ){
            $scope.class = 'backtowork';
            $scope.title = 'back to work';
            $scope.multiple = false;
            $scope.type = $stateParams['type'];

            $scope.postcode = '';
            $scope.distance = '';
            $scope.location = {};

            $scope.update = true;
            $scope.postchange = false;
            $scope.distchange = false;

            $scope.start = function(){
                $scope.postchange = false;
                $scope.distchange = false;
            };

            $scope.start();

            $scope.doTheBack = function() {
                $state.go('home');
            };

            $scope.useloc = function(){
                /*
                 Search using gps
                 */
                navigator.geolocation.getCurrentPosition(function(data) {
                    $scope.location = {lat:data.coords.latitude, lng:data.coords.longitude};
                    locposts.addLocation($scope.location);
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                        locposts.addPostcode($scope.postcode);
                    })
                }, function(err){
                    console.log("Geocoder failed");
                    console.log(err);
                });
            };

            $scope.postcodechange = function(){
                $scope.postchange = true;
            };

            $scope.distancechange = function(){
                $scope.distchange = true;
            };


            $scope.updatebtn = function(){
                locposts.clear();
                if($scope.postchange){
                    console.log($scope.postcode);
                    poslocs.getpost($scope.postcode).then(function(res){
                        $scope.location = res;
                        console.log(res);
                        locposts.addLocation($scope.location);

                        if($scope.distchange){
                            console.log($scope.distance);
                            locposts.addDist($scope.distance);
                        }
                        if($scope.type == 'volunteering'){
                            $state.go('backtowork_vol_list');
                        }else if($scope.type == 'new_work'){
                            $state.go('backtowork_new');
                        }
                    })
                }else{
                    if($scope.distchange){
                        console.log($scope.distance);
                        locposts.addDist($scope.distance);
                        if($scope.type == 'volunteering'){
                            $state.go('backtowork_vol_list');
                        }else if($scope.type == 'new_work'){
                            $state.go('backtowork_new');
                        }
                    }else{
                        if($scope.type == 'volunteering'){
                            $state.go('backtowork_vol_list');
                        }else if($scope.type == 'new_work'){
                            $state.go('backtowork_new');
                        }
                    }
                }
            };

        }])

    .controller('VoluntTabCtrl',[
        '$scope',
        '$http',
        'locposts',
        'poslocs',
        'onlineData',
        '$state',
        function(
            $scope,
            $http,
            locposts,
            poslocs,
            onlineData,
            $state
        ){
            $scope.class = 'backtowork';
            $scope.title = 'back to work';
            $scope.showMapSwip = false;
            $scope.checklocation = function() {
                var loca = locposts.haslocation();
                if (loca) {
                    $scope.showMapSwip = true;
                } else {
                    $scope.showMapSwip = false;
                }
            };

            $scope.setlocation = function(){
                console.log('saerch');
                $state.go('backtowork_vol');
            };

            $scope.update = true;

            onlineData.getSections().then(function(res){
                $scope.glist = res.objects;
            });

            $scope.doTheBack = function() {
                window.history.back();
            };

        }])

    .controller('VoluntListCtrl',[
        '$scope',
        '$http',
        'locposts',
        'poslocs',
        'doitData',
        'leafletData',
        'GEO',
        '$state',
        function(
            $scope,
            $http,
            locposts,
            poslocs,
            doitData,
            leafletData,
            GEO,
            $state
        ){
            $scope.class = 'backtowork';
            $scope.title = 'back to work';
            $scope.location = locposts.getLocation();
            $scope.postcode = locposts.getPostcode();
            $scope.distance = locposts.getDist();
            $scope.center = {
                lat: $scope.location.lat,
                lng:  $scope.location.lng,
                zoom: 12
            };
            $scope.markers = {};
            $scope.lastname = '';
            $scope.thisname = 'volunteering';
            $scope.nextname = 'new work'.slice(0,4);
            $scope.haslocation = false;



            $scope.tiles = {
                //url: "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                //url: "https://tiles.lyrk.org/ls/{z}/{x}/{y}",
                url: "https://1.aerial.maps.cit.api.here.com/maptile/2.1/maptile/newest/terrain.day/{z}/{x}/{y}/256/png8?app_id=yaB0SQ2nEdvwdE7LGMOd&app_code=cfm3SIyf49nYHrTIL0L4gw",
            };


            $scope.showMap = true;
            $scope.$watch("showMap", function(value) {
                if (value === true) {
                    leafletData.getMap("oppmap").then(function(map) {
                        map.invalidateSize();
                    });
                }
            });

            $scope.cid = 'volctrl';
            $scope.details = null;
            $scope.jobs = "";

            $scope.doTheBack = function() {
                window.history.back();
            };

            $scope.useloc = function(){
                /*
                 Search using gps
                 */
                navigator.geolocation.getCurrentPosition(function(data){
                    $scope.location = {lat:data.coords.latitude, lng:data.coords.longitude};
                    locposts.addLocation($scope.location);
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                        locposts.addPostcode($scope.postcode);

                        $scope.getdata();

                    })
                }, function(err){
                    console.log("Geocoder failed");
                    console.log(err);
                });

            };

            $scope.updateloc = function(){
                $state.go('backtowork_update',{"type":'volunteering'})
            };

            $scope.getdata = function(){

                try{
                    var loc = locposts.getLocation();
                    var dist = locposts.getDist();
                    var datas = doitData.oppLatlng(loc, dist);
                    datas.then(function(data){

                        $scope.doit = data.objects;
                        $scope.showMap = true;
                        for(var i in $scope.doit){
                            $scope.markers[$scope.doit[i].title.replace('-','') ] = {
                                'lat':$scope.doit[i].latitude,
                                'lng':$scope.doit[i].longitude,
                                'message':$scope.doit[i].title.replace('-',''),
                                'icon':{
                                    'iconUrl':'/static/img/circle-lightpurlple.svg',
                                    'iconSize':[30,30],
                                    'iconAnchor':[15,15],
                                    'popupAnchor':[0,0]
                                }
                            };
                            $scope.doit[i].dist = parseFloat(GEO.cal($scope.location, {lat:$scope.doit[i].latitude, lng:$scope.doit[i].longitude},6));
                            if($scope.doit[i].apply_url.length < 5){
                                $scope.doit[i].apply_url = 'https://do-it.org/opportunities/'+$scope.doit[i].opid+'/';
                            };
                        }
                    });

                    angular.extend($scope, {
                        center: {
                            lat: $scope.location.lat,
                            lng: $scope.location.lng,
                            zoom: 12
                        },
                        markers : $scope.markers
                    });
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                    });
                }catch(err){console.log(err);}

            };
            $scope.getdata();
        }
    ])

    .controller('ReturnCtrl',[
        '$scope',
        '$http',
        'locposts',
        'poslocs',
        'onlineData',
        function(
            $scope,
            $http,
            locposts,
            poslocs,
            onlineData
        ){
            $scope.class = 'backtowork';
            $scope.title = 'back to work';

            $scope.lastname = '';
            $scope.thisname = '';
            $scope.nextname = '';

            $scope.sect = function(slug, index){
                $scope.slug = slug.slug;
                onlineData.getItems($scope.slug).then(function(res){
                    $scope.glist = res.objects;
                });
                if (index == 0){
                    $scope.lastname = 'new work';
                }else{
                    $scope.lastname = $scope.glist[index-1].title.slice(-5);
                }
                $scope.thisname = slug.title;
                try{
                    $scope.nextname = $scope.glist[index+1].title.slice(0,4);
                }catch(err){
                    $scope.nextname = '';
                }

            };

            $scope.details = null;

            $scope.doTheBack = function() {
                window.history.back();
            };



            $scope.openclick = function(id){
                if(id == null){
                    $scope.details = null;

                }else{
                    $scope.details = $scope.glist[id];
                }
            }
        }])

    .controller('NewWorkCtrl',[
        '$scope',
        '$http',
        'locposts',
        'poslocs',
        'doitJobs',
        'leafletData',
        '$timeout',
        'onlineData',
        'GEO',
        '$state',
        function(
            $scope,
            $http,
            locposts,
            poslocs,
            doitJobs,
            leafletData,
            $timeout,
            onlineData,
            GEO,
            $state
        ){
            $scope.class = 'backtowork';
            $scope.title = 'back to work';
            $scope.location = locposts.getLocation();
            $scope.postcode = locposts.getPostcode();
            $scope.distance = locposts.getDist();
            $scope.center = {
                lat: $scope.location.lat,
                lng:  $scope.location.lng,
                zoom: 12
            };
            $scope.lastname = 'volunteering'.slice(-5);
            $scope.thisname = 'new work';
            $scope.nextname = '';

            $scope.nextsect = null;
            $scope.markers = {};

            $scope.showMap = true;


            $scope.$watch("showMap", function(value) {
                if (value === true) {
                    leafletData.getMap("jobmap").then(function(map) {
                        map.invalidateSize();
                    });
                }
            });

            onlineData.getSections().then(function(res){
                $scope.nextname = res.objects[0].title.slice(0,4);
            });

            $scope.tiles = {
                //url: "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                //url: "https://tiles.lyrk.org/ls/{z}/{x}/{y}",
                url: "https://1.aerial.maps.cit.api.here.com/maptile/2.1/maptile/newest/terrain.day/{z}/{x}/{y}/256/png8?app_id=yaB0SQ2nEdvwdE7LGMOd&app_code=cfm3SIyf49nYHrTIL0L4gw",
            };

            $scope.cid = 'newctrl';
            $scope.details = null;
            $scope.jobs = "";

            $scope.doTheBack = function() {
                window.history.back();
            };

            $scope.useloc = function(){
                /*
                 Search using gps
                 */
                navigator.geolocation.getCurrentPosition(function(data){
                    $scope.location = {lat:data.coords.latitude, lng:data.coords.longitude};
                    locposts.addLocation($scope.location);
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                        locposts.addPostcode($scope.postcode);

                        $scope.getdata();

                    })
                }, function(err){
                    console.log("Geocoder failed");
                    console.log(err);
                });

            };

            $scope.updateloc = function(){
                console.log('click');
                $state.go('backtowork_update',{"type":'new_work'})
            };

            $scope.getdata = function(){
                try{
                    var loc = locposts.getLocation();
                    var dist = locposts.getDist();
                    var datas = doitJobs.jobLatlng(loc, dist);
                    datas.then(function(data){
                        $scope.Jobdoit = data.objects;
                        $scope.showMap = true;

                        for(var i in $scope.Jobdoit){
                            $scope.markers[$scope.Jobdoit[i].title.replace('-','') ] = {
                                    'lat':$scope.Jobdoit[i].latitude,
                                    'lng':$scope.Jobdoit[i].longitude,
                                    'message':$scope.Jobdoit[i].title.replace('-',''),
                                    'icon':{
                                        'iconUrl':'/static/img/circle-purple.svg',
                                        'iconSize':[30,30],
                                        'iconAnchor':[15,15],
                                        'popupAnchor':[0,0]
                                    }
                            };
                            if($scope.Jobdoit[i].apply_url.length < 5){
                                $scope.Jobdoit[i].apply_url = 'https://do-it.org/jobs/'+$scope.Jobdoit[i].jdid+'/';
                            };
                            $scope.Jobdoit[i].dist = parseFloat(GEO.cal($scope.location, {lat:$scope.Jobdoit[i].latitude, lng:$scope.Jobdoit[i].longitude},6));
                        }
                    });

                    angular.extend($scope, {
                        center: {
                            lat: $scope.location.lat,
                            lng: $scope.location.lng,
                            zoom: 12
                        },
                        markers : $scope.markers
                    });
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                    });
                }catch(err){console.log(err);}

            };

            $scope.getdata();
        }])


    .controller('legalCtrl', [
        '$scope',
        '$sanitize',
        'siteData',
        '$location',
        function($scope,
                 $sanitize,
                 siteData,
                 $location){
            $scope.class = 'legalright';
            $scope.title = 'legal rights';
            $scope.url = 'legal_rights';
            $scope.sect = $location.$$url.substr($location.$$url.indexOf('#') + 1);

            $scope.doTheBack = function() {
                window.history.back();
            };

            siteData.getLegal().then(function(res){
                $scope.list = res.objects;
                for(var i = 0; i < $scope.list.length; i++){
                    if(i>0){
                        $scope.list[i].lastname = $scope.list[i-1].title.slice(-5);
                    }
                    if(i<$scope.list.length){
                        try{
                            $scope.list[i].nextname = $scope.list[i+1].title.slice(0,4);
                        }catch(err){}
                    }
                }
            });

        }])

    .controller('OurDataCtrl', [
        '$scope',
        '$sanitize',
        'siteData',
        '$location',
        'userlog',
        function($scope,
                 $sanitize,
                 siteData,
                 $location,
                 userlog
                 ){

            $scope.class = 'ourdata';
            $scope.title = 'our data';
            $scope.url = 'our_data';

            $scope.name = '';
            $scope.email = '';
            $scope.mailchimp = {};
            $scope.mailchimp.username ='ma-work';
            $scope.mailchimp.dc ='us2';
            $scope.mailchimp.u ='1942b6bbcbfd553cd7714a3f4';
            $scope.mailchimp.id ='1713ea2e97';

            $scope.sect = $location.$$url.substr($location.$$url.indexOf('#') + 1);

            $scope.submit = function(){
                userlog.MailUser($scope.mailchimp);
            };


            $scope.doTheBack = function() {
                window.history.back();
            };

            siteData.getOur().then(function(res){
                $scope.list = res.objects;
                for(var i = 0; i < $scope.list.length; i++){
                    if(i>0){
                        $scope.list[i].lastname = $scope.list[i-1].title.slice(-5);
                    }
                    if(i<$scope.list.length){
                        try{
                            $scope.list[i].nextname = $scope.list[i+1].title.slice(0,4);
                        }catch(err){}
                    }
                }
            });

        }])

    .controller('SignUpCtrl',['$scope','USER','$state', 'Facebook','userlog', function($scope, USER, $state, Facebook,userlog){
        $scope.class = 'details';
        $scope.title = 'Profile details';

        if($state.current.data.start){
            $scope.start = $state.current.data.start;
        }

        if(USER.loggedin()){
            if($state.current.data.start){
                $state.go('home');
            }else{
                $state.go('mydetail');
            }
        }

        $scope.createprof = function(){
            if($state.current.data.start){
                $state.go('start_register');
            }else{
                $state.go('register');

            }
        };

        $scope.login = function(){
            if($state.current.data.start){
                $state.go('start_login');
            }else{
                $state.go('login');
            }

        };

        $scope.tomain = function(){
            USER.setskip();
            $state.go('home');
        };

        $scope.facebook = function(){
            Facebook.login(function(response){
                var statuscode;
                var data;
                Facebook.getLoginStatus(function(reponse){
                    if (response.status === 'connected') {
                        statuscode = response.authResponse.accessToken;
                        Facebook.api('/me', {fields: 'name, first_name, last_name, age_range, gender, locale, email'}, function (response) {
                            data = response;
                            data['statuscode'] = statuscode;
                            var us = userlog.fbUser(data);
                            us.then(function (res) {
                                if (res.status == 200) {
                                    if($scope.start){
                                        $state.go('home');
                                    }else{
                                        $state.go('mydetail');
                                    }

                                } else {
                                    $scope.error = true;
                                }
                            })
                        });
                    }
                });
            },{scope:'email'})
        };

        $scope.$on('event:google-plus-signin-success', function (event,authResult) {
            var actok = authResult.access_token;
            gapi.client.load('plus', 'v1', function () {
                gapi.client.plus.people.get({'userId': 'me' }).execute(function (resp) {
                    var data = resp;
                    data['statuscode'] = actok;
                    var us = userlog.GlUser(data);
                    us.then(function (res) {
                        if (res.status == 200) {
                            if($scope.start){
                                $state.go('home');
                            }else{
                                $state.go('mydetail');
                            }
                        } else {
                            $scope.error = true;
                        }
                    })
                });
            });
        });
        $scope.$on('event:google-plus-signin-failure', function (event,authResult) {

        });

        $scope.doTheBack = function() {
            window.history.back();
        };

    }])

    .controller('RegisterCtrl',['$scope','USER', '$state','userlog', function($scope, USER,$state,userlog){
        $scope.class = 'details';
        $scope.title = 'Profile details';
        $scope.error = null;
        $scope.data = null;


        $scope.submit = function(data){
            var item = userlog.registeruser(data);
            item.then(function(res){
                if(res.status == 200){
                    if($state.current.data.start){
                        $state.go('start_update');
                    }else{
                        $state.go('update');
                    }

                }else{
                    $scope.error = true;
                }
            });
        };

        $scope.doTheBack = function() {
            window.history.back();
        };
    }])

    .controller('UpdateFormCtrl',['$scope','USER', '$state','userlog','$sanitize', function($scope, USER, $state,userlog, $sanitize){
        $scope.class = 'details';
        $scope.title = 'Profile details';
        $scope.error = null;
        $scope.data = {};

        $scope.data = USER.getuserdata();

        if($scope.data.bio != null){
            if($scope.data.bio.indexOf(/<[^>]+>/gm)){
                $scope.data.bio = $scope.data.bio.replace(/<[^>]+>/gm, '');
            }
        }

        $scope.submit = function(data){
            var bk = userlog.update(data);
            try{
                bk.then(function(res){
                    console.log(res);
                    if(res.statusText == 'ACCEPTED'){
                        if($state.current.data.start){
                            $state.go('home');
                        }else{
                            $state.go('mydetail');
                        }
                    }else{
                        $scope.error = true;
                    }
                });
            }catch(err){
                $scope.error = true;
            }
        };

        $scope.doTheBack = function() {
            window.history.back();
        };
    }])

    .controller('LogInCtrl',['$scope','USER', '$state','userlog', function($scope, USER, $state,userlog){
        $scope.class = 'details';
        $scope.title = 'Login details';
        $scope.error = null;
        $scope.email = '';
        $scope.password = '';

        $scope.submit = function(email, password){
            if(email !== null && password !== null){
                var data = JSON.stringify({"email":email, "password":password});
                var us = userlog.login(data);
                us.then(function(res){
                    if(res.status == 200){
                        if($state.current.data.start){
                            $state.go('home');
                        }else{
                            $state.go('mydetail');
                        }
                    }else{
                        $scope.error = true;
                    }
                })
            }
        };

        $scope.doTheBack = function() {
            window.history.back();
        };
    }])

    .controller('ImgUpCtrl',['$scope','$state', 'USER', function($scope, $state, USER){
        $scope.class = 'details';
        $scope.title = 'Image upload';
        $scope.error = null;
        $scope.session = USER.getsession();
        $scope.completed = function(){
            $state.go('mydetail');
        }
    }])

    .controller('DetailCtrl', [
        '$scope',
        'USER',
        '$state',
        'userlog',
        'siteData',
        'flatblocks',
        function($scope,USER, $state,userlog, siteData,flatblocks){
            $scope.class = 'details';
            $scope.title = 'my details';

            if(USER.loggedin() == false){
                $state.go('signup');
            }else{
                userlog.getdata().then(function(res){
                    $scope.data = res;
                    USER.setuserdata(res);

                });

                $scope.update = function(){
                    $state.go('update');
                };

                $scope.logout = function(){
                    var log = USER.logout();
                    if(log){
                        console.log('logged out');
                    }
                    $state.go('home');
                };

                $scope.gotoImage = function(){
                    $state.go('imageupload');
                };

                $scope.getflat = function(){
                    if(flatblocks.getFlat('achievement')){
                        return flatblocks.getFlat('achievement');
                    }else{
                        return '';
                    }

                }
            }
            $scope.doTheBack = function() {
                window.history.back();
            };
        }])

    .controller('AchiveCtrl', [
        '$scope',
        function($scope){
            $scope.class = 'details';
            $scope.title = 'achivements';
            $scope.doTheBack = function() {
                window.history.back();
            };
        }])

    .controller('AroundSearchCtrl', [
        '$scope',
        'USER',
        '$state',
        'locposts',
        'userlog',
        'siteData',
        'flatblocks',
        'localAuthData',
        'poslocs',
        function($scope,USER, $state, locposts, userlog, siteData,flatblocks,localAuthData, poslocs) {
            $scope.class = 'around';
            $scope.title = 'around you';
            $scope.postchange = false;
            $scope.distchange = false;
            $scope.postcode = '';
            $scope.distance = '';
            $scope.location = {};
            $scope.update = false;
            $scope.error = null;



            navigator.geolocation.getCurrentPosition(function(data){
                $scope.location = {lat:data.coords.latitude, lng:data.coords.longitude};
            }, function(err){
                console.log("Geocoder failed");
                console.log(err);
            });

            localAuthData.getData().then(function(res){
                $scope.loauth = res.objects;
            });

            if($state.current.data.error){
                $scope.error = true;
            }else{
                $scope.error = false;
            }

            if($state.current.data.update){
                $scope.update = true;

                $scope.useloc = function(){
                    locposts.addLocation($scope.location);
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                        locposts.addPostcode($scope.postcode);
                    })
                };

                $scope.postcodechange = function(){
                    $scope.postchange = true;
                };

                $scope.distancechange = function(){
                    $scope.distchange = true;

                };

                $scope.updatebtn = function(){
                    $scope.update = false;
                    locposts.addRegion('');
                    if($scope.postchange){
                        poslocs.getpost($scope.postcode).then(function(res){
                            $scope.location = res;
                            locposts.addLocation($scope.location);

                            if($scope.distchange){
                                locposts.addRegion($scope.distance);
                            }
                            $state.go('aroundyou_details');
                        })
                    }else{
                        if($scope.distchange){
                            locposts.addRegion($scope.distance);
                            $state.go('aroundyou_details');
                        }else{
                            $state.go('aroundyou_details');
                        }
                    }
                };

            }else{
                locposts.getRegion().then(function(res){
                    $state.go('aroundyou_details');
                }, function(resone){
                    console.log(resone);
                });

                $scope.update = false;
                var loc = locposts.haslocation();
                if(loc){
                    $state.go('aroundyou_update');
                }

                $scope.useloc = function(){
                    /*
                     Search using gps
                     */
                    locposts.addLocation($scope.location);
                    poslocs.locpost($scope.location).then(function(res){
                        $scope.postcode = res;
                        locposts.addPostcode($scope.postcode);
                        $state.go('aroundyou_details');
                    })
                };

                $scope.postcodechange = function(){
                    $scope.postchange = true;
                };

                $scope.postcodeenter = function(){
                    var item = $scope.postcode;
                    locposts.addPostcode($scope.postcode);
                    poslocs.getpost(item).then(function(res){
                        $scope.location = res;
                        locposts.addLocation($scope.location);
                        $state.go('aroundyou_details');
                    })
                };
                $scope.distancechange = function(){
                    locposts.addRegion($scope.distance);
                    $state.go('aroundyou_details');
                };
            }

            $scope.doTheBack = function() {
                $state.go('home');
            };
        }])

    .controller('AroundDetailCtrl', [
        '$scope',
        'USER',
        '$state',
        'userlog',
        'siteData',
        'flatblocks',
        'locposts',
        'localAuthData',
        '$filter',
        '$location',
        function($scope,USER, $state,userlog, siteData,flatblocks, locposts, localAuthData, $filter, $location) {
            $scope.class = 'around';
            $scope.title = 'around you';

            $scope.postcode = locposts.getPostcode();
            $scope.data = null;


            locposts.getRegion().then(function(res){
                $scope.region = res;
                localAuthData.getLocal($scope.region).then(function(res){
                    if(res[0]){
                        $scope.data = res[0];
                        $scope.unemp = $filter('orderBy')($scope.data.unemployment, '-year');
                        $scope.unfirst = $scope.unemp[0];
                        $scope.unsecond = $scope.unemp[1];
                        $scope.unfirstnum = ($scope.unemp[0].percentage * 250) / 100;
                        $scope.unsecondnum = ($scope.unemp[2].percentage * 250) / 100;
                        $scope.inemp = $filter('orderBy')($scope.data.inemployment, '-year');
                        $scope.infirst = $scope.inemp[0];
                        $scope.insecond = $scope.inemp[1];
                        $scope.infirstnum = ($scope.inemp[0].percentage * 250) / 100;
                        $scope.insecondnum = ($scope.inemp[2].percentage * 250) / 100;
                        $scope.jobs = $filter('orderBy')($scope.data.jobseeking, '-date');
                        for(var i = 0; i < $scope.jobs.length; i++){
                            $scope.jobs[i].size = ($scope.jobs[i].percentage * 50) / 100;
                        }
                        $scope.pastjobs = $scope.jobs.slice(1,-1);
                        $scope.jobfirst = $scope.jobs[0];
                        $scope.jobfirstcir = ($scope.jobs[0].percentage * 250) / 100;
                        $scope.disable = $filter('orderBy')($scope.data.disabilityliving.total, '-date');
                        $scope.disablefirst = $scope.disable[0];
                        $scope.disablefirstcir = ($scope.disablefirst.percetange * 250) / 100;
                        $scope.dislearning = $filter('orderBy')($scope.data.disabilityliving['learning difficulties'], '-date');
                        $scope.disbehav = $filter('orderBy')($scope.data.disabilityliving['behavioural disorder'], '-date');
                        $scope.dispersonal = $filter('orderBy')($scope.data.disabilityliving['personality disorder'], '-date');
                        $scope.dispsych = $filter('orderBy')($scope.data.disabilityliving['psychoneurosis'], '-date');
                        $scope.dissever = $filter('orderBy')($scope.data.disabilityliving['severely mentally impaired'], '-date');
                    }else{
                        $state.go('aroundyou_error');
                    }
                });
            }, function(res){
                console.log(res);
                $state.go('aroundyou');
            });

            localAuthData.getRegion().then(function(res){
                $scope.regiondata = res;
            });

            $scope.updateloc = function(){
                $state.go('aroundyou_update');
            };

            $scope.gotoserver = function(){
                $state.go('support_results');
            };

            $scope.gotocv = function(){
                $state.go('backtowork_tabs');
            };

        }]);
/**
 * Created by sam on 26/08/15.
 */
angular.module('rework.directive',[])
    .directive("searchsect", function($templateCache){
        return {
            templateUrl: '/static/templates/directives/search.html'
        };
    })
    .directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })
    .directive("searchresults", function(){
        return{
            templateUrl: '/static/templates/directives/results.html'
        };
    })

    .directive("checkboxGroup", function() {
        return {
            restrict: "A",
            link: function(scope, elem, attrs) {
                // Determine initial checked boxes
                if (scope.array.indexOf(scope.item.id) !== -1) {
                    elem[0].checked = true;
                }

                // Update array on click
                elem.bind('click', function() {
                    var index = scope.array.indexOf(scope.item.id);
                    // Add if checked
                    if (elem[0].checked) {
                        if (index === -1) scope.array.push(scope.item.id);
                    }
                    // Remove if unchecked
                    else {
                        if (index !== -1) scope.array.splice(index, 1);
                    }
                    // Sort and update DOM display
                    scope.$apply(scope.array.sort(function(a, b) {
                        return a - b
                    }));
                });
            }
        }
    });
'use strict';

angular.module('rework.services',['rework.controllers', 'geolocation'])

    .service('poslocs', ['$http', '$q', function($http, $q){
        function getpost(query){
            var ret = $q.defer();
            if(query.length > 4){
                $http.get('https://api.postcodes.io/postcodes/'+query).then(function(response){
                    ret.resolve({'lat':response.data.result.latitude, 'lng':response.data.result.longitude});
                });
            }else{
                $http.get('https://api.postcodes.io/outcodes/'+query).then(function(response){
                    ret.resolve({'lat':response.data.result.latitude, 'lng':response.data.result.longitude});
                });
            }

            return ret.promise;
        }

        function locpost(obj) {
            var ret = $q.defer();
            if(obj.lng){
                $http.get('https://api.postcodes.io/outcodes?lon=' + obj.lng + '&lat=' + obj.lat).then(function (response) {
                    ret.resolve(response.data.result[0].outcode);
                });
            }else{
                console.log('error with lat lng');
                ret.reject('no lat or lng');
            }
            return ret.promise;
        }

        function getRegion(query){
            console.log(query);
            var ret = $q.defer();
            if(query.length > 4){
                $http.get('https://api.postcodes.io/postcodes/'+query).then(function(response){
                    ret.resolve({'region':response.data.result.admin_district});
                });
            }else{
                ret.reject('not full postcode');
            }

            return ret.promise;
        }
        function getRegionLat(obj){
            var ret = $q.defer();
            if(obj.lng){
                $http.get('https://api.postcodes.io/postcodes?lon=' + obj.lng + '&lat=' + obj.lat).then(function (response) {
                    ret.resolve(response.data.result[0].admin_district);
                });
            }else{
                console.log('error with lat lng');
                ret.reject('no lat or lng');
            }
            return ret.promise;
        }

        return{
            getpost:getpost,
            locpost:locpost,
            getRegion:getRegion,
            getRegionLat:getRegionLat
        };


    }])

    .service('userlog', ['$http','$q','USER','poslocs', function($http, $q,USER,poslocs){
        function registeruser(data){
            var dt = JSON.stringify(data);
            console.log(dt);
            return $http({
                method: 'POST',
                url: '/user/registration/',
                headers: { 'Content-Type': 'application/json'},
                data: dt
            }).then(function(response){
                console.log(response);
                if(response.status == 200){
                    USER.addusername(data.username);
                    USER.addsession(response.data.token);
                    return response;
                }
                return response;
            }).catch(function(err){
                return err;
            });
        }

        function updateuser(data){
            var session = USER.getsession();
            if(data.postcode != null){
                poslocs.getpost(data.postcode).then(function(res){
                    data['latitude'] = res.lat;
                    data['longitude'] = res.lng;
                });
            }
            if(session != null){
                data['token'] = session;
                var dt = JSON.stringify(data);
                return $http({
                    method:'POST',
                    url:'/user/update/',
                    headers:{'Content-Type': 'application/json'},
                    data: dt
                }).then(function(response){
                    return response;
                }).catch(function(err){
                    return err;
                })
            }
            return false;
        }

        function loginuser(data){
            return $http({
                method:'POST',
                url:'/user/login/',
                headers:{'Content-Type':'application/json'},
                data:data
            }).then(function(response){
                if(response.status == 200){
                    USER.addusername(response.data.username);
                    USER.addsession(response.data.token);
                    return response;
                }
            }).catch(function(err){
                return err
            })
        }

        function getdata(){
            var data = {"token":USER.getsession()};
            var dt = JSON.stringify(data);
            return $http({
                method:'POST',
                url:'/user/',
                header:{'Content-Type':'application/json'},
                data:dt
            }).then(function(resp){
                return resp.data;
            }).catch(function(err){
                return err;
            })
        }

        function fbUser(data){
            return $http({
                method:'POST',
                url:'/user/facebooklogin/',
                headers:{'Content-Type':'application/json'},
                data:data
            }).then(function(response){
                if(response.status == 200){
                    USER.addusername(response.data.username);
                    USER.addsession(response.data.token);
                    return response;
                }
            }).catch(function(err){
                return err
            })
        }

        function GlUser(data){
            return $http({
                method:'POST',
                url:'/user/googlelogin/',
                headers:{'Content-Type':'application/json'},
                data:data
            }).then(function(response){
                if(response.status == 200){
                    console.log(response);
                    if(response.data.username == "None"){
                        USER.addusername(response.config.data.id);
                    }else{
                        USER.addusername(response.data.username);
                    }
                    USER.addsession(response.data.token);
                    return response;
                }
            }).catch(function(err){
                return err
            })
        }

        function MailUser(data){
            return $http({
                method:'POST',
                url:'/user/maillist/',
                headers:{'Content-Type':'application/json'},
                data:data
            }).then(function(response){
                if(response.status == 202){
                    return response;
                }
            }).catch(function(err){
                return err
            })
        }

        return{
            registeruser:registeruser,
            update: updateuser,
            login: loginuser,
            getdata: getdata,
            fbUser: fbUser,
            GlUser: GlUser,
            MailUser: MailUser
        }
    }])

    .service('ALLOW',['$cookies',function($cookies){
        var allow = function(){
            var expireData = new Date(3000,10,10,13,30,30,30);
            $cookies.put('plexusAllow','true',{'expires':expireData});
        };

        var check = function(){
            if($cookies.get('plexusAllow')){
                return true;
            }else{
                return false;
            }
        };

        return{
            allow: allow,
            check: check
        }
    }])

    .service('nhsDetails',[function(){
        var checkDetails = function(item){
            if(item.telephone.length < 4){
                if(item.trust.contacts.telephone){
                    item.telephone = item.trust.contacts.telephone;
                }
            }
            if(item.email.length < 4){
                if(item.trust.contacts.email.length > 1){
                    item.email = item.trust.contacts.email;
                }
            }
            if(item.website.length < 4){
                if(item.trust.contacts.website){
                    item.website = item.trust.contacts.website;
                }
            }
            return item;
        };

        return{
            checkDetails: checkDetails
        }
    }])

    .service('GEO',[function(){

        var toDegree = function(red){
            return red * 180 / Math.PI;
        };

        var toRadians = function(deg){
            return deg * Math.PI / 180;
        };

        var calculateDistance = function(starting, ending, dec){
            var KM_RATIO = 6371;
            try {
                var dLat = toRadians(parseFloat(ending.lat) - parseFloat(starting.lat));
                var dLon = toRadians(parseFloat(ending.lng) - parseFloat(starting.lng));
                var lat1Rad = toRadians(starting.lat);
                var lat2Rad = toRadians(ending.lat);
                var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1Rad) * Math.cos(lat2Rad);
                var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                var d = KM_RATIO * c;
                return d.toFixed(dec);
            } catch(e) {
                return -1;
            }
        };

        var posdiff = function(x1, x2){
            // 1 is positive, 0 negative
            var at = null;
            var bt = null;
            if(x1 > 0.0){
                at = 1;
            }else{
                at = 0;
            }
            if(x2 > 0.0){
                bt = 1;
            }else{
                bt = 0;
            }
            if (at == bt){
                return true;
            }else{
                return false;
            }
        };

        return{
            toDegree:toDegree,
            toRadians:toRadians,
            cal:calculateDistance,
            posdiff: posdiff
        }
    }])

    .service('splashserv',['$cookies','ALLOW', function($cookies, ALLOW){
        var setvisited = function(){
            if(ALLOW.check()){
                var expireData = new Date(3000,10,10,13,30,30,30);
                $cookies.put('plexusVisit','visited',{'expires':expireData});
            }
        };

        var hasvisited = function(){
            if($cookies.get('plexusVisit')){
                return true;
            }else{
                return false;
            }
        };

        return{
            setvisited: setvisited,
            hasvisited: hasvisited
        }

    }])


    .service('USER',['$cookies','ALLOW', function($cookies, ALLOW){
        var username = null;
        var session = null;
        var token = null;
        var userdata = null;
        var notif = true;

        var addusername = function(newObj){
            username = newObj;
        };

        var addsession = function(newObj){

            session = newObj;
            if(ALLOW.check()) {
                var expireData = new Date();
                expireData.setDate(expireData.getDate() + 30);
                $cookies.put('plexusToken', newObj, {'expires': expireData});
                if ($cookies.get('plexusSkip')) {
                    $cookies.remove('plexusSkip');
                }

            }
        };

        var addtoken = function(newObj){
            token = newObj;
        };

        var getusername = function(){
            return username;
        };

        var getsession = function(){
            if (session == null){
                var use = $cookies.get('plexusToken');
                if (use){
                    session = use;
                }
            }
            return session;
        };

        var gettoken = function(){
            return token;
        };

        var loggedin = function(){

            if(session != null){
                return true;
            }

            if($cookies.get('plexusToken')){
                return true;
            }

            return false;
        };

        var logout = function(){
            if(session != null){
                if( $cookies.get('plexusToken')){
                    $cookies.remove('plexusToken');
                }
                session = null;
                username = null;
                token = null;
                userdata = null;
                return true;
            }
            return false;
        };

        var setuserdata = function(newObj){
            userdata = newObj;
        };

        var getuserdata = function(){
            return userdata;
        };

        var setskip = function(){
            if(ALLOW.check()) {
                var expireData = new Date();
                expireData.setDate(expireData.getDate() + 30);
                $cookies.put('plexusSkip', 'true', {'expires': expireData});
            }
        };

        var removeskip = function(){
            if( $cookies.get('plexusSkip')){
                $cookies.remove('plexusSkip');
            }
        };

        var testskip = function(){
            if( $cookies.get('plexusSkip')){
                return true;
            }else{
                return false
            }
        };

        var addnotify = function(){
            if(ALLOW.check()) {
                var expireData = new Date(3000,10,10,13,30,30,30);
                $cookies.put('plexusAdded','true',{'expires':expireData});
            }
            notif = false;
        };

        var notifycheck = function(){
            if(notif){
                if( $cookies.get('plexusAdded')){
                    return true;
                }else{
                    return false
                }
            }else{
                return true;
            }

        };

        return{
            addusername: addusername,
            addsession: addsession,
            addtoken: addtoken,
            getusername: getusername,
            getsession: getsession,
            gettokem: gettoken,
            loggedin: loggedin,
            logout: logout,
            setuserdata: setuserdata,
            getuserdata: getuserdata,
            setskip: setskip,
            removeskip:removeskip,
            testskip:testskip,
            addnotify:addnotify,
            notifycheck:notifycheck
        }

    }])


    .service('locposts', ['poslocs','$cookies', 'ALLOW','$q', function(poslocs, $cookies, ALLOW, $q){
        var location = null;
        var postcode = null;
        var distance = 10;
        var region = null;
        var services = [];

        var addLocation = function(newObj){
            location = newObj;
            if(ALLOW.check()) {
                $cookies.put('plexusLoclat', newObj.lat);
                $cookies.put('plexusLoclng', newObj.lng);
            }
            poslocs.locpost(newObj).then(function (res) {
                addPostcode(res);
            });
        };

        var addRegion = function(newObj){
            region = newObj;
        };

        var addPostcode = function(newObj){
            postcode = newObj;
            if(ALLOW.check()) {
                $cookies.put('plexusPostCode', newObj);
            }
        };

        var addDist = function(newObj){
            distance = newObj;
        };

        var getLocation = function(){
            var use = {lat:null, lng:null};
            use.lat = parseFloat($cookies.get('plexusLoclat'));
            use.lng = parseFloat($cookies.get('plexusLoclng'));
            if(use.lat){
                location = use;
                return location;
            }else{
                return false;
            }
        };

        var getRegion = function(){
            var ret = $q.defer();
            if(region){
                ret.resolve(region);
            }
            if(location){
                poslocs.getRegionLat(location).then(function(res){
                    ret.resolve(res.replace(/\s+/g, '-').toLowerCase());
                })
            }else{
                ret.reject('no location');
            }
            return ret.promise;
        };

        var getPostcode = function(){
            var use = $cookies.get('plexusPostCode');
            var post;
            if(use){
                post = use;
            }else{
                post = postcode;
            }
            return post;
        };

        var getDist = function(){
            return distance;
        };

        var haslocation = function(){
            if(location != null){
                return getLocation();
            }else{
                return false;
            }
        };

        var clear = function(){
            location = null;
            postcode = null;
            distance = 10;
            region = null;
            $cookies.remove('plexusLoclat');
            $cookies.remove('plexusLoclng');
            $cookies.remove('plexusPostCode');
        };

        var addservice = function(newobj){
            services = [];
            for(var i in newobj){
                services.push(newobj[i]);
            }
        };

        var clearservice = function(){
            services = [];
        };

        var getservices = function(){
            return services;
        };

        return {
            addLocation: addLocation,
            addPostcode: addPostcode,
            addDist: addDist,
            getLocation: getLocation,
            getPostcode: getPostcode,
            getDist: getDist,
            haslocation: haslocation,
            clear: clear,
            addRegion:addRegion,
            getRegion:getRegion,
            addservice:addservice,
            clearservice:clearservice,
            getservices: getservices
        };
    }])

    .service("markercreate", ['$q', function($q){

        function mind(lst){
            var parker = $q.defer();
            var parklocation = {};
            var markers = {'mind':'img/circle-purple.svg','nhs':'img/circle-blue.svg','doit':'img/circle-grey.svg'};
            for (var i =0; i > lst.length ; i++){
                parklocation[i] = {
                    'lat':parseFloat(lst[i]._latitude_postcode),
                    'lng':parseFloat(lst[i]._longitude_postcode),
                    'message':lst[i].title,
                    'icon':{
                        iconUrl:markers['mind'],
                        iconSize:     [30, 30],
                        iconAnchor:   [15, 15],
                        popupAnchor:  [0, 0]
                    }
                }
            }
            parket.resolve(parkLocations);
            return parket.promise;
        };

        function nhs(lst){
            var parker = $q.defer();
            var parklocation = {};
            var markers = {'mind':'img/circle-purple.svg','nhs':'img/circle-blue.svg','doit':'img/circle-red.svg'};
            for (var i =0; i > lst.length ; i++){
                parklocation[i] = {
                    'lat':parseFloat(lst[i]._latitude_postcode),
                    'lng':parseFloat(lst[i]._longitude_postcode),
                    'message':lst[i].title,
                    'icon':{
                        iconUrl:markers['nhs'],
                        iconSize:     [30, 30],
                        iconAnchor:   [15, 15],
                        popupAnchor:  [0, 0]
                    }
                }
            }
            parket.resolve(parkLocations);
            return parket.promise;
        }


        return{
            mind:mind,
            nhs:nhs
        };
    }])

    .factory("locpost", ["$http", "GEO", "$q", function($http, GEO, $q){
        return {
            query: function(lng, lat){
                if(lng && lat){
                    return $http.get('https://api.postcodes.io/outcodes?lon='+lng+'&lat='+lat).then(function(response){
                        return response.result[0].outcode;
                    })
                }else{
                    return $q.reject('no lat or lng');
                }
            }
        }
    }])

    .factory("doitData", ['$http', '$q', function($http, $q){
        var doitData = function(data){
            angular.extend(this, data);
        };
        var url = "https://knowledge.plexus.support/api/v1/"; // May have it all the other bits

        function toDegree(red){
            return red * 180 / Math.PI;
        }

        function toRadians(deg){
            return deg * Math.PI / 180;
        }

        doitData.getJobsid = function(id){
            /*
             THis will get the jobs of a certain id
             */
            return $http.get(url + 'jobs/'+ id +' ?format=json').then(function(response){
                return new doitData(response.data);
            })
        };

        doitData.getOppid = function(id){
            /*
             This will return the opp of a certin id
             */
            return $http.get(url + 'opportunities/'+ id +' ?format=json').then(function(response){
                return new doitData(response.data);
            })
        };

        doitData.jobLatlng =function(loc, dist){
            /*
             This will query the mind with location and distance from it
             distance section
             */
            console.log(loc);
            if(loc.lat != undefined){
                if(dist<5){
                    dist = 5;
                }
                var R = 6371;
                var x1 = loc.lng - toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var x2 = loc.lng + toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var y1 = loc.lat - toDegree(dist/R);
                var y2 = loc.lat + toDegree(dist/R);

                if(GEO.posdiff(x1,x2)) {
                    return $http.jsonp(url + 'jobs/?latitude__gt=' + y1 + '&latitude__lt=' + y2 + '&longitude__gt=' + x1 + '&longitude__lt=' + x2 + '&callback=JSON_CALLBACK').then(function (response) {
                        return new doitData(response.data);
                    });
                }else{
                    return $http.jsonp(url + 'jobs/?latitude__gt=' + y1 + '&latitude__lt=' + y2 + '&longitude__gt=' + x1 +  '&callback=JSON_CALLBACK').then(function (response) {
                        return new doitData(response.data);
                    });
                }
            }else{
                return new doitData(null);
            }
        };

        doitData.oppLatlng =function(loc, dist){
            /*
             This will query the mind with location and distance from it
             distance section
             */

            if(loc.lat != undefined){
                if(dist<5){
                    dist = 5;
                }
                var R = 6371;
                var x1 = loc.lng - toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var x2 = loc.lng + toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var y1 = loc.lat - toDegree(dist/R);
                var y2 = loc.lat + toDegree(dist/R);

                return $http.jsonp(url + 'opportunities/?latitude__gt='+ y1 +'&latitude__lt='+ y2 +'&longitude__gt='+ x1 +'&longitude__lt='+ x2 +'&callback=JSON_CALLBACK').then(function(response) {
                    return new doitData(response.data);
                });
            }else{
                return new doitData(null);
            }
        };
        return doitData;

    }])

    .factory("doitJobs", ['$http', 'GEO', function($http, GEO){
        var doitJobs = function(data){
            angular.extend(this, data);
        };
        var url = "https://knowledge.plexus.support/api/v1/"; // May have it all the other bits

        function toDegree(red){
            return red * 180 / Math.PI;
        }

        function toRadians(deg){
            return deg * Math.PI / 180;
        }

        doitJobs.getJobsid = function(id){
            /*
             THis will get the jobs of a certain id
             */
            return $http.get(url + 'jobs/'+ id +' ?format=json').then(function(response){
                return new doitJobs(response.data);
            })
        };

        doitJobs.getOppid = function(id){
            /*
             This will return the opp of a certin id
             */
            return $http.get(url + 'opportunities/'+ id +' ?format=json').then(function(response){
                return new doitJobs(response.data);
            })
        };

        doitJobs.jobLatlng =function(loc, dist){
            /*
             This will query the mind with location and distance from it
             distance section
             */
            if(loc.lat != undefined) {
                if (dist < 10) {
                    dist = 15;
                }
                var R = 6371;
                var x1 = loc.lng - toDegree(dist / R / Math.cos(toRadians(loc.lat)));
                var x2 = loc.lng + toDegree(dist / R / Math.cos(toRadians(loc.lat)));
                var y1 = loc.lat - toDegree(dist / R);
                var y2 = loc.lat + toDegree(dist / R);
                if(GEO.posdiff(x1,x2)) {
                    return $http.jsonp(url + 'jobs/?latitude__gt=' + y1 + '&latitude__lt=' + y2 + '&longitude__gt=' + x1 + '&longitude__lt=' + x2 + '&callback=JSON_CALLBACK').then(function (response) {
                        return new doitJobs(response.data);
                    });
                }else{
                    return $http.jsonp(url + 'jobs/?latitude__gt=' + y1 + '&latitude__lt=' + y2 + '&longitude__gt=' + x1 + '&callback=JSON_CALLBACK').then(function (response) {
                        return new doitJobs(response.data);
                    });
                }
            }else{
                return new doitData(null);
            }
        };


        return doitJobs;

    }])

    .factory("hospitalData", ['$http','GEO','$q', function($http, GEO, $q){
        var nhsData = function(data){
            angular.extend(this, data);
        };
        var url = "/api/v1/"; // May have it all in one go

        function toDegree(red){
            return red * 180 / Math.PI;
        }

        function toRadians(deg){
            return deg * Math.PI / 180;
        }

        nhsData.getall = function(){
            /*
             This will get all the data for the hospital
             */
            return $http.get(url + 'hospitals/?format=json').then(function(response){
                return new nhsData(response.data);
            });
        };

        nhsData.getid = function(id){
            /*
             This will get the data from the hospital with a id
             */
            return $http.get(url + 'hospitals/'+ id +'/?format=json').then(function(response){
                return new nhsData(response.data);
            });
        };

        nhsData.getLatlng =function(loc, dist){
            /*
             This will query the mind with location and distance from it
             distance section
             */
            if(loc.lat != undefined) {
                if(dist<5){
                    dist = 5;
                }
                var R = 6371;
                var x1 = loc.lng - toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var x2 = loc.lng + toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var y1 = loc.lat - toDegree(dist/R);
                var y2 = loc.lat + toDegree(dist/R);
                if(GEO.posdiff(x1,x2)) {
                    return $http.jsonp(url + 'hospitals/?latitude__gt=' + y1 + '&latitude__lte=' + y2 + '&longitude__lte=' + x2 + '&longitude__gte=' + x1 + '&callback=JSON_CALLBACK').then(function (response) {
                        return new nhsData(response.data);
                    });

                }else{
                    //return $http.jsonp(url + 'hospitals/?latitude__gt=' + y1 + '&latitude__lte=' + y2 + '&longitude__lte=' + x2 + '&callback=JSON_CALLBACK').then(function (response) {
                    return $http.jsonp(url + 'hospitals/?latitude__gt=' + y1 + '&latitude__lte=' + y2 + '&longitude__lte=' + x2 + '&longitude__gte=' + x1 + '&callback=JSON_CALLBACK').then(function (response) {
                        return new nhsData(response.data);
                    });
                }
            }else{
                return new nhsData($q.reject('no loc'));
            }
        };

        return nhsData;

    }])

    .factory("onlineData",['$http', function($http){
        var onlineData = function(data){
            angular.extend(this,data);
        };

        var url = "/api/v1/";

        onlineData.getSections = function(){
            return $http.get(url + 'old_section?format=json').then(function(response){
                return new onlineData(response.data)
            });
        };

        onlineData.getItems = function(section){
            return $http.get(url + 'old_btwitem?section__slug__exact='+section+'&format=json').then(function(response){
                return new onlineData(response.data)
            });
        };

        onlineData.getReturn = function(){
            return $http.get(url + 'old_btwitem?section__slug__exact=returning-to-work&format=json').then(function(response){
                return new onlineData(response.data)
            });
        };

        onlineData.getGuidance = function(){
            return $http.get(url + 'old_btwitem?section__slug__exact=application-guidance&format=json').then(function(response){
                return new onlineData(response.data)
            });
        };

        onlineData.getSlider = function(){
            return $http.get(url + 'frontslide?format=json').then(function(response){
                return new onlineData(response.data)
            });
        };

        return onlineData;
    }])

    .factory("mindData",['$http', 'GEO', '$q', function($http, GEO, $q){
        var mindData = function(data) {
            angular.extend(this, data);
        };
        var url = "/data/";

        function toDegree(red){
            return red * 180 / Math.PI;
        }

        function toRadians(deg){
            return deg * Math.PI / 180;
        }

        mindData.getall = function(){
            /*
             This will produce a list of all minds
             */
            return $http.get(url + 'minds/?format=json').then(function(response) {
                return new mindData(response.data);
            });
        };

        mindData.mindid = function(id){
            /*
             This is to get a mind by id
             */
            return $http.get(url +'minds/' + id + '?format=json').then(function(response) {
                return new mindData(response.data);
            });
        };

        mindData.mindLatlng =function(loc, dists){
            /*
             This will query the mind with location and distance from it
             distance section
             */
            var dist = parseInt(dists);
            if(loc.lat != undefined) {
                if (dist < 10){
                    dist = 15;
                }
                var R = 6371;
                var x1 = loc.lng - toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var x2 = loc.lng + toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var y1 = loc.lat - toDegree(dist/R);
                var y2 = loc.lat + toDegree(dist/R);

                if(GEO.posdiff(x1,x2)){
                    return $http.jsonp(url + 'minds/?_latitude_postcode__gt='+ y1 +'&_latitude_postcode__lt='+ y2 +'&_longitude_postcode__lt='+ x1+'&_longitude_postcode__gt='+ x2 +'&callback=JSON_CALLBACK').then(function(response) {
                        return new mindData(response.data);
                    });
                }else{
                    return $http.jsonp(url + 'minds/?_latitude_postcode__gt='+ y1 +'&_latitude_postcode__lt='+ y2 +'&_longitude_postcode__lt='+ x1+'&callback=JSON_CALLBACK').then(function(response) {
                        return new mindData(response.data);
                    });
                }

            }else{
                return new mindData($q.reject('no loc'));
            }
        };

        mindData.mindServices = function(){
            /*
             This is a function to get all the services
             */
            return $http.get(url + 'services/?format=json').then(function(response){
                return new mindData(response.data);
            })
        };

        mindData.service = function(id){
            /*
             Get the service from id
             */
            return $http.get(url + 'services/' + id + '?format=json').then(function(response){
                return new mindData(response.data);
            })
        };

        mindData.serviceLM = function(id){
            /*
             Get the services for a certain local mind
             */
            return $http.get(url + 'services/?minds__id'+ id + '&format=json').then(function(response){
                return new mindData(response.data);
            })
        };

        mindData.grouping = function(){
            return $http.jsonp(url + 'service_grouping/?callback=JSON_CALLBACK').then(function(response){
                return new mindData(response.data);
            })
        };

        mindData.servicegroup = function(idlist){
            var sect = '';
            for(var i in idlist){
                sect += 'services__category='+ idlist[i] + '&';
            }
            return $http.jsonp(url +'minds/?'+ sect + 'callback=JSON_CALLBACK').then(function(response){
                return new mindData(response.data);
            })
        };

        mindData.serviceLat = function(idlist, loc, dists){
            var sect = '';
            for(var i in idlist){
                sect += 'services__category='+ idlist[i] + '&';
            }
            var dist = parseInt(dists);
            if(loc.lat != undefined) {
                if (dist < 10){
                    dist = 15;
                }
                var R = 6371;
                var x1 = loc.lng - toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var x2 = loc.lng + toDegree(dist/R/Math.cos(toRadians(loc.lat)));
                var y1 = loc.lat - toDegree(dist/R);
                var y2 = loc.lat + toDegree(dist/R);

                if(GEO.posdiff(x1,x2)){
                    return $http.jsonp(url + 'minds/?'+ sect + '_latitude_postcode__gt='+ y1 +'&_latitude_postcode__lt='+ y2 +'&_longitude_postcode__lt='+ x1+'&_longitude_postcode__gt='+ x2 +'&callback=JSON_CALLBACK').then(function(response) {
                        return new mindData(response.data);
                    });
                }else{
                    return $http.jsonp(url + 'minds/?'+ sect + '_latitude_postcode__gt='+ y1 +'&_latitude_postcode__lt='+ y2 +'&_longitude_postcode__lt='+ x1+'&callback=JSON_CALLBACK').then(function(response) {
                        return new mindData(response.data);
                    });
                }
            }else{
                return new mindData(null);
            }
        };

        return mindData;
    }])

    .service('flatblocks',[function(){
        var flatblock = {};

        var addFlat = function(obj){
            flatblock[obj.slug] = obj;
        };

        var getFlat = function(obj){
            if(flatblock[obj]){
                return flatblock[obj].content
            }else{
                return ' '
            }
        };

        return{
            addFlat: addFlat,
            getFlat: getFlat
        }
    }])

    .factory('siteData', ['$http','flatblocks', function($http,flatblocks){
        var siteData = function(data) {
            angular.extend(this, data);
        };

        var url = "/api/v1/";

        siteData.getLegal = function(){
            return $http.get(url + 'legal').then(function(response){
                return new siteData(response.data);
            });
        };

        siteData.getOur = function(){
            return $http.get(url +'data/').then(function(response){
                return new siteData(response.data);
            });
        };

        siteData.addmail = function(paramer){
            return $http({
                method: 'POST',
                url: url + 'maillist/',
                headers: { 'Content-Type': 'application/json; charset=UTF-8'},
                data: paramer
            }).then(function(response){
                return new siteData(response.data)
            });
        };

        siteData.flatblock = function(){
            return $http.get(url + 'flatblock/').then(function(response){

                return new siteData(response.data);
            })
        };

        return siteData;

    }])


    .service('aroundLocalAuth',[function(){
        var loclauth = {};

        var addRegion = function(obj){
            loclauth[obj.slug] = obj;
        };

        var getRegion = function(obj){
            if(loclauth[obj]){
                return loclauth[obj];
            }else{
                return none;
            }
        };

        var allRegions = function(){
            return loclauth;
        };

        return{
            addRegion:addRegion,
            getRegion:getRegion,
            allRegion:allRegions
        }
    }])

    .factory('localAuthData', ['aroundLocalAuth', '$http', function(aroundRegion, $http){
        var regionData = function(data) {
            angular.extend(this, data);
        };

        var url = "/api/v1/";

        regionData.getData = function(){
            return $http.get(url + 'localathority_name/?format=json').then(function(response) {
            return new regionData(response.data)
            });
        };

        regionData.getLocal = function(param){
            return $http.get(url + 'localathority?slug='+ param +'&format=json').then(function(response) {
                return new regionData(response.data.objects)
            });
        };

        regionData.getRegion = function(param){
            return $http.get(url + 'region?format=json').then(function(response) {
                return new regionData(response.data.objects)
            });
        };

        return regionData;
    }])


;


