$(document).ready(function() {     
    
    /* ======= jQuery Placeholder ======= */
    /* Ref: https://github.com/mathiasbynens/jquery-placeholder */
    
    $('input, textarea').placeholder();      
	
	/* ======= Fixed Header animation ======= */ 
	       
    $(window).on('scroll', function() {
         
         if ($(window).scrollTop() > 0 ) {
             $('#header').addClass('header-change');
         }
         else {
             $('#header').removeClass('header-change');             
         }
        if($(window).scrollTop() > $('#home').height() +100){
            $('#plexuslogo').attr('src','http://plexus.support/static/new_assets/images/devices/logo2.png');
            $('.colortext').css('color','#78BF4A');
        }else{
            $('#plexuslogo').attr('src','http://plexus.support/static/new_assets/images/devices/logo.png');
            $('.colortext').css('color','white');
        }

    }); 
    
    /* ======= Toggle between Signup & Login Modals ======= */ 
    $('#signup-link').on('click', function(e) {
        $('#signup-modal').modal();
        $('#login-modal').modal('toggle');
        e.preventDefault();
    });
    
    
    
    $('#login-link').on('click', function(e) {
        $('#login-modal').modal();
        $('#signup-modal').modal('toggle');
        e.preventDefault();
    });
    
    

});