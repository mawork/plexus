$(document).ready(function() {        
	
	/* ======= Fullpage.js ======= */ 
	/* Ref: https://github.com/alvarotrigo/fullPage.js */
        
    $('#fullpage').fullpage({
		anchors: ['home_sec', 'grace_sec', 'learning_sec'],
		navigation: true,
		navigationPosition: 'right',
		navigationTooltips: ['Home', 'Grace SMS', 'Learning & Support'],
		resize : false,
		scrollBar: true,
		autoScrolling: false,
		paddingTop: '120px'
	});
    

});